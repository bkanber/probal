close all

t=0:1/60:3;

phase_offsets=[0 pi/180*5 pi/180*10 pi/180*50 pi/180*90 pi/180*170 pi/180*180 ...
    pi/180*270 pi/180*300 pi/180*360];

for i=1:numel(phase_offsets)
    L=7+sin(t*2*pi+phase_offsets(i));

    figure;subplot(1,2,1);plot(t,L);xlabel('time(s)');ylabel('L(mm)');

    strain=L/mean(L)-1;

    subplot(1,2,2);plot(t,strain);xlabel('time(s)');ylabel('strain');

    title(['phase(degrees) = ' num2str(phase_offsets(i)/pi*180)]);
end
