function varargout = motion_analysis_gui(varargin)
% MOTION_ANALYSIS_GUI MATLAB code for motion_analysis_gui.fig
%      MOTION_ANALYSIS_GUI, by itself, creates a new MOTION_ANALYSIS_GUI or raises the existing
%      singleton*.
%
%      H = MOTION_ANALYSIS_GUI returns the handle to a new MOTION_ANALYSIS_GUI or the handle to
%      the existing singleton*.
%
%      MOTION_ANALYSIS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MOTION_ANALYSIS_GUI.M with the given input arguments.
%
%      MOTION_ANALYSIS_GUI('Property','Value',...) creates a new MOTION_ANALYSIS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before motion_analysis_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to motion_analysis_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help motion_analysis_gui

% Last Modified by GUIDE v2.5 09-Nov-2013 19:40:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @motion_analysis_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @motion_analysis_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before motion_analysis_gui is made visible.
function motion_analysis_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to motion_analysis_gui (see VARARGIN)

% Choose default command line output for motion_analysis_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes motion_analysis_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = motion_analysis_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit2,'String','');


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit2,'String','wall.mat');


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = get(handles.edit1,'String');
if ~strendswith(filename,'.mat')
    waitfor(msgbox('Select waveform to continue'));
    h=gco;
    y=get(h,'YData');
    x=y.*0;
    plaque=[];
    t=get(h,'XData');
    plaque.frameRate=1/(t(2)-t(1));
else
    plaque=load(filename);

    assert(length(plaque.mean_displacements_x)==length(plaque.mean_displacements_y));

    x=plaque.mean_displacements_x;
    y=plaque.mean_displacements_y;

    filename2 = get(handles.edit2,'String');
    if ~isempty(filename2)
        wall=load(filename2);
        assert(length(wall.mean_displacements_x)==length(wall.mean_displacements_y));
        assert(length(wall.mean_displacements_x)==length(plaque.mean_displacements_x));
        assert(wall.frameRate==plaque.frameRate);
        x=x-wall.mean_displacements_x;
        y=y-wall.mean_displacements_y;
    end

    % analytical simulation
    if 0
        plaque.frameRate=30;
        t=0:150;
        x=17*sin(t/40*2*pi);
        y=x.*0;
    end
end

figure;

t=0:length(x)-1;
t=t/plaque.frameRate;
subplot(2,3,1);plot(t,x);
title('x');
axis tight;
xlabel('time(s)');
ylabel('x (mm)');

t=0:length(y)-1;
t=t/plaque.frameRate;
subplot(2,3,2);plot(t,y);
title('y');
axis tight;
xlabel('time(s)');
ylabel('y (mm)');

subplot(2,3,3);
plot(x,y,'.');xlabel('centeroid_x(mm)');ylabel('centeroid_y(mm)');

% hold on;
% for i=1:length(x)
%     text(x(i),y(i),num2str(i),'FontSize',9);
% end

try
    dt = DelaunayTri(x',y');
    k = convexHull(dt);
    hold on;
    plot(dt.X(k,1),dt.X(k,2), 'r');

    displacement_area = polyarea(dt.X(k,1),dt.X(k,2));
catch exception
    displacement_area = NaN;
end

dmat = distmat([x' y']); % distance matrix

max_displacement = max(max(dmat));

%title(['disp_area=' num2str(displacement_area) '; max_disp=' ...
title(['max_disp=' ...
    num2str(max_displacement)],'Interpreter', 'none');

vel_x = gradient(x,1/plaque.frameRate);
vel_y = gradient(y,1/plaque.frameRate);

vel = sqrt(vel_x.^2+vel_y.^2);

subplot(2,3,4);
plot(t,vel);
axis tight;
xlabel('time(s)');
ylabel('velocity amp (mm/s)');
title(['vel amp, max=' num2str(max(vel)) ', mean=' num2str(mean(vel))]);

acc_x = gradient(vel_x,1/plaque.frameRate);
acc_y = gradient(vel_y,1/plaque.frameRate);

acc = sqrt(acc_x.^2+acc_y.^2);

subplot(2,3,5);
%plot(t,acc./9.81E3);
plot(t,acc);
axis tight;
xlabel('time(s)');
%ylabel('acc amp (g)');
ylabel('acc amp (mm/s^2)');
title(['acc amp, max=' num2str(max(acc)) ', mean=' num2str(mean(acc))]);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String','wall.mat');

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String','plaque.mat');


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit1,'String','(manual select)');
