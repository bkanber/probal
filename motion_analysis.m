load plaque

assert(length(mean_displacements_x)==length(mean_displacements_y));

x0=0;
y0=0;

x=[];
y=[];

for i=1:length(mean_displacements_x)
    x=[x x0+mean_displacements_x(i)];
    y=[y y0+mean_displacements_y(i)];
end

figure;

t=0:length(mean_displacements_x)-1;
t=t/frameRate;
subplot(2,3,1);plot(t,mean_displacements_x);
title('mean disp_x');
axis tight;
xlabel('time(s)');
ylabel('disp (mm)');

t=0:length(mean_displacements_y)-1;
t=t/frameRate;
subplot(2,3,2);plot(t,mean_displacements_y);
title('mean disp_y');
axis tight;
xlabel('time(s)');
ylabel('disp (mm)');

subplot(2,3,3);
plot(x,y,'.');xlabel('centeroid_x(mm)');ylabel('centeroid_y(mm)');

% hold on;
% for i=1:length(x)
%     text(x(i),y(i),num2str(i),'FontSize',9);
% end

dt = DelaunayTri(x',y');
k = convexHull(dt);
hold on;
plot(dt.X(k,1),dt.X(k,2), 'r');

displacement_area = polyarea(dt.X(k,1),dt.X(k,2));

dmat = distmat([x' y']); % distance matrix

max_displacement = max(max(dmat));

title(['disp_area=' num2str(displacement_area) '; max_disp=' ...
    num2str(max_displacement)],'Interpreter', 'none');

vel_x = x(2:end)-x(1:end-1);
vel_y = y(2:end)-y(1:end-1);

vel_x = vel_x.*frameRate;
vel_y = vel_y.*frameRate;

vel = sqrt(vel_x.^2+vel_y.^2);

t=1:length(mean_displacements_x)-1;
t=t/frameRate;

subplot(2,3,4);
plot(t,vel);
axis tight;
xlabel('time(s)');
ylabel('velocity amp (mm/s)');
title('velocity amplitude');

acc_x = vel_x(2:end)-vel_x(1:end-1);
acc_y = vel_y(2:end)-vel_y(1:end-1);

acc_x = acc_x.*frameRate;
acc_y = acc_y.*frameRate;

acc = sqrt(acc_x.^2+acc_y.^2);

t=2:length(mean_displacements_x)-1;
t=t/frameRate;

subplot(2,3,5);
plot(t,acc./9.81E3);
axis tight;
xlabel('time(s)');
ylabel('acc amp (g)');
title('acceleration amplitude');
