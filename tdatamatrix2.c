 /* =============================================================
 * tdatamatrix2.c - used by vesselmo.m
 *
 * Computational function
 * Calculates training data matrix
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <stdlib.h>

/*
Usage format is [datamatrix,pathlogic,targets] = tdatamatrix2(trainpath,pathsize,tblock_size,whole_block,limx,limy,targets,trunc)
*/

void tdatamatrix2(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *trainpath = mxGetPr(prhs[0]);
  double *pathsize = mxGetPr(prhs[1]);

  long tblock_size = (long)(*mxGetPr(prhs[2]));
  
  double *whole_block = mxGetPr(prhs[3]);

  long limx = (long)(*mxGetPr(prhs[4]));
  long limy = (long)(*mxGetPr(prhs[5]));

  double *targets = mxGetPr(prhs[6]);
  int targetsnsup = mxGetN(prhs[6]);

  long trunc = (long)(*mxGetPr(prhs[7]));

  long larm_len = ceil(((double)tblock_size-1)/2);
  long rarm_len = floor(((double)tblock_size-1)/2);

  long npoints = (long)pathsize[0];

  long xl, yu, xr, yd, i, x, y, optI;
  long nadd = 0;
  
  long this_x, this_y;
  double Pb;

  long tblock_size2 = tblock_size*tblock_size;
  
  double *datamatrix;
  double *newtargets;
  
  mxLogical *pathlogics;
  
  /* Determine the points in region of uncertainty */
  int nuncertain = 0;

  plhs[1] = mxCreateLogicalMatrix(npoints, 1);
  
  pathlogics = mxGetLogicals(plhs[1]);
  
  for (i = 0; i < npoints; i++)
  {
    this_x = trainpath[i];
    this_y = trainpath[i+npoints];
    
    Pb = whole_block[this_y-1+(this_x-1)*limy];
    
    /* Determine if Pb is the region of uncertainty */
    if (Pb<0.45 || Pb>0.55) pathlogics[i] = 0;
    else
    {
        pathlogics[i] = 1;
        nuncertain++;
    }

  }
  
  if (nuncertain == 0)
  {
    plhs[0] = mxCreateDoubleScalar(-1);
    plhs[2] = mxCreateDoubleScalar(-1);
    return;
  }
  
  /* Now build the tdata matrix */
  printf("targetsnsup=%d\n",targetsnsup);
  
  plhs[0] = mxCreateDoubleMatrix(tblock_size2, nuncertain, mxREAL);
  
  datamatrix = mxGetPr(plhs[0]);

  if (!targetsnsup) plhs[2] = mxCreateDoubleScalar(-1);
  else
  {
      plhs[2] = mxCreateDoubleMatrix(1, nuncertain, mxREAL);
  
      newtargets = mxGetPr(plhs[2]);
  }

  for (i = 1; i <= npoints; i++)
  {
    if (pathlogics[i-1]==1)
    {
        long Q = 0;

        this_x = trainpath[i-1];
        this_y = trainpath[i-1+npoints];

        xl = this_x-larm_len;
        yu = this_y-larm_len;
        xr = this_x+rarm_len;
        yd = this_y+rarm_len;
        
        if (trunc && 0)
        {
            optI = this_y-1+(this_x-1)*limy;
            
            if (targets[i-1]==1 && whole_block[optI]<0.5)
            {
                printf("Out but left in\n");
                continue;
            }
                
            if (targets[i-1]<1 && whole_block[optI]>=0.2)
            {
                printf("In but left out\n");
                continue;
            }
            
        }
        
        if (targetsnsup) newtargets[nadd] = targets[i-1];
            
        for (x = xl; x <= xr; x++)
        {
            for (y = yu; y <= yd; y++)
            {
                optI = Q + nadd * tblock_size2;
                
                if (x<1 || x>limx || y<1 || y>limy)
                    datamatrix[optI] = 0;
                else
                    datamatrix[optI] = whole_block[y-1+(x-1)*limy];
                
                Q = Q+1;
            }
        }
	
        nadd++;
        
    } /* if pathlogics */
    
  } /* for (i) */
  
  mxSetN(plhs[0],nadd);
  
  if (targetsnsup) mxSetN(plhs[2],nadd);
  
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=8 || nlhs!=3)
  {
    mexErrMsgTxt("Usage format is [datamatrix,pathlogic,targets] = tdatamatrix2(trainpath,pathsize,tblock_size,whole_block,limx,limy,targets,trunc)");
    return;
  }

  tdatamatrix2(nlhs, plhs, nrhs, prhs);

}
