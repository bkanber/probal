plot_t=get(gco,'XData');
plot_y=get(gco,'YData');

plot_fs = length(plot_t)/(max(plot_t));

% msgbox(strcat('check sampling frequency is ',num2str(plot_fs)),'modal');

str = ['check sampling frequency is ' num2str(plot_fs)];

figure;
subplot(2,2,1);plot(plot_t,plot_y);

plot_y = plot_y-mean(plot_y); % remove DC component

subplot(2,2,2);plot(plot_t,plot_y);

N = length(plot_y);
Y = abs(fft(plot_y))/(N/2);
f = plot_fs/2*linspace(0,1,N/2);
power_spectrum = Y.^2;
total_power = sum(power_spectrum);

subplot(2,2,[3 4]);
thisx = f*60;
thisy = power_spectrum(1:N/2)*2/total_power;
plot(thisx,thisy); 
set(gca, 'FontName', 'Delicious');
set(gca, 'FontSize', 12);
title('power spectrum(1:N/2)*2/total_power');
xlabel('frequency (/min)');
ylabel('Y.^2(1:N/2)*2/total_power');

% Calc mean beats per minute 
% old1 (>=50bpm <=160bpm fft power>5% avg weighting by fft power)
% current (>=40bpm <=160bpm fft power>5% avg weighting by fft power)
bpm = 0;
i = find(thisy>=0.05);
thisx = thisx(i);
thisy = thisy(i);
i = find(thisx>=40 & thisx<=160);
if length(i)>0
    thisx = thisx(i);
    thisy = thisy(i);
    bpm = dot(thisx,thisy)/sum(thisy);
end

str = [str ' bpm=' num2str(bpm)];

MyBox = uicontrol('style','text');
set(MyBox,'String',str,'FontName','Delicious','FontSize',10);
set(MyBox,'Position',[10,10,500,20])
