close all;
n=1000;

rng('shuffle');

asymp_dos=0.10+rand(n,1)*0.70;
symp_dos=0.40+rand(n,1)*0.55;

rng('shuffle');

asymp_sii=1.10+rand(n,1)*0.70;
symp_sii=1.40+rand(n,1)*0.55;

% asymp_dos=0.5+rand(n,1)*3;
% symp_dos=0.5+rand(n,1)*3;
% symp_dos=symp_dos.*2.5;

% asymp_dos=asymp_dos*100;
% symp_dos=symp_dos*100;

sii_vals=cat(1,asymp_sii,symp_sii);
dos_vals=cat(1,asymp_dos,symp_dos);
groups=cat(1,zeros(n,1),ones(n,1));

model1=sii_vals.*dos_vals;
model2=sii_vals+dos_vals;

figure;
x=cat(1,(10:10+n-1)',(n*2:n*2+n-1)');

subplot(2,4,1);plot(x,sii_vals,'*');subplot(2,4,2);boxplot(sii_vals,groups);
subplot(2,4,3);plot(x,dos_vals,'*');subplot(2,4,4);boxplot(dos_vals,groups);

x=cat(1,(10:10+n-1)',(n*2:n*2+n-1)');

subplot(2,4,5);plot(x,model1,'*');
title(num2str(numel(find(model1(n+1:end)>max(model1(1:n))))+numel(find(model1(1:n)<min(model1(n+1:end))))));
subplot(2,4,6);boxplot(model1,groups);
asymp_avg = mean(model1(1:n));
symp_avg = mean(model1(n+1:end));
title(num2str(symp_avg/asymp_avg));

subplot(2,4,7);plot(x,model2,'*');
title(num2str(numel(find(model2(n+1:end)>max(model2(1:n))))+numel(find(model2(1:n)<min(model2(n+1:end))))));
subplot(2,4,8);boxplot(model2,groups);
asymp_avg = mean(model2(1:n));
symp_avg = mean(model2(n+1:end));
title(num2str(symp_avg/asymp_avg));

x=1;

%B=mnrfit(gsm_vals,dos_vals);

% a=50;
% b=50;
% C0=0;
% deltaGSM=1:0.1:5;
% deltaSII=2;
% asymp = C0+a+b;
% symp = C0+deltaGSM.*a+deltaSII.*b;
% ratio = symp./asymp;
% figure;plot(deltaGSM,ratio,'*');

