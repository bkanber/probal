    %audio_data = (audio_data-min(audio_data))/(max(audio_data)-min(audio_data))*255;
    % audio_data = kron(audio_data,ones(1,f_step));
%     for i=1:log2(f_step)
%         audio_data2 = zeros(1,length(audio_data)*2);
%         audio_data2(2:2:length(audio_data2)) = audio_data;
%         audio_data2(3:2:length(audio_data2)-1) = (audio_data(1:length(audio_data)-1)+audio_data(2:length(audio_data)))/2;
%         audio_data = audio_data2;
%     end
    % audio_data = audio_data(1,:);
    %audio_data = audio_data/255*2-1;
    %stretched_distmm = min(distmm)+((audio_data+1)/2)*(max(distmm)-min(distmm));
    %theta = (stretched_distmm*1).*t_samp*2*pi;
    %theta = rem(theta,2*pi);
    %audio_data = ((stretched_distmm-min(distmm))/(max(distmm)-min(distmm))).*sin(theta);
    %figure;plot(stretched_distmm);title('strecthed distmm');
    %audio_data = sin(1000*t_samp*2*pi);
    %audio_data = [audio_data zeros(1,length(audio_data)/f_step)];
    %audio_data = repmat(audio_data,1,20);
    for i=1:log2(f_step)
        i
        audio_data2 = zeros(1,length(audio_data)*2);
        audio_data2(1:2:length(audio_data2)) = audio_data;
        for j=2:2:length(audio_data2)
            ind = j/2;
            num_add = 0;
            audio_data2(j) = 0;
            for k=ind-1:ind+2
                if (k>=1 && k<=length(audio_data)) 
                    if (k==ind-1) weight = 1; end
                    if (k==ind) weight = 2; end
                    if (k==ind+1) weight = 2; end
                    if (k==ind+2) weight = 1; end
                    audio_data2(j) = audio_data2(j)+audio_data(k)*weight;
                    num_add = num_add + weight;
                end
            end
            audio_data2(j) = audio_data2(j)/num_add;
        end
        audio_data = audio_data2;
    end

        xdata = linedata{frm}{1};
    ydata = -linedata{frm}{2};
    xa = xdata(1:length(xdata)-2);
    ya = ydata(1:length(ydata)-2);
    xc = xdata(2:length(xdata)-1);
    yc = ydata(2:length(ydata)-1);
    xb = xdata(3:length(xdata));
    yb = ydata(3:length(ydata));
    a = sqrt((yb-yc).^2+(xb-xc).^2);
    b = sqrt((ya-yc).^2+(xa-xc).^2);
    c = sqrt((ya-yb).^2+(xa-xb).^2);
    theta = acosd((a.^2+b.^2-c.^2)./(2*a.*b));
    %theta = Datan2((ydata(2:length(ydata))-ydata(1:length(ydata)-1)),(xdata(2:length(xdata))-xdata(1:length(xdata)-1)));
%     for i=1:length(xdata)
%         text(xdata(i),ydata(i),'x','Color',[0 1 0]);
%     end
    while (1==1)
        [min_theta, Ci] = min(theta);
        if (min_theta>-179) 
            xdata = xdata(1:Ci);
            ydata = ydata(1:Ci);
            break; 
        end;
        xdata = xdata(1:Ci-1);
        ydata = ydata(1:Ci-1);
    end
    %hold on
    %plot([xdata(1) xdata(Ci)], [-ydata(1) -ydata(Ci)], 'Color', [0 1 0]);
    %figure;plot(xdata(2:length(xdata)-1),ydata(2:length(ydata)-1),xdata(2:length(xdata)-1),theta);
    %figure;plot(,-linedata{frm}{2});title(num2str(frm));
