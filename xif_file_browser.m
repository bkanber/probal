function xif_file_browser()
global n
figure;
n=0;
do_dir('Q:\');

function do_dir(folder)
global n
list = dir([folder '*']);

for i=1:length(list)
    if strncmp(list(i).name,'.',1)==1
        continue
    end
    if list(i).isdir==1
        do_dir([folder list(i).name '\']);
        continue
    end
    if ~strendswith(list(i).name,'.xif')
        continue
    end
    fullname = [folder list(i).name];
    [xifinfo,whole_a] = readxif(fullname,1,[]);
    n=n+1;
    if n==10
        clf
        n=1;
    end        
    
    subplot(3,3,n);
    imagesc(whole_a);colormap(gray);title(fullname);
end
