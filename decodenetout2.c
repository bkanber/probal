 /* =============================================================
 * decodenetout2.c - used by vesselmo.m
 *
 * Computational function
 * Decodes network output
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <stdlib.h>

/*
Usage format is decodenetout2(trainpath,pathsize,tblock_size,whole_block,limx,limy,netout,pathlogics)
*/

void decodenetout2(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *trainpath = mxGetPr(prhs[0]);
  double *pathsize = mxGetPr(prhs[1]);

  long tblock_size = (long)(*mxGetPr(prhs[2]));
  
  double *whole_block = mxGetPr(prhs[3]);

  long limx = (long)(*mxGetPr(prhs[4]));
  long limy = (long)(*mxGetPr(prhs[5]));

  double *netout = mxGetPr(prhs[6]);
  
  mxLogical *pathlogics = mxGetLogicals(prhs[7]);

  long npoints = (long)pathsize[0];
  long i, X, Y, k;
  
  k = 0;
  
  for (i = 0; i < npoints; i++)
  {
    X = trainpath[i];
    Y = trainpath[i+npoints];
    
    if (pathlogics[i]==true) 
    {
        whole_block[Y-1+(X-1)*limy] = netout[k++];
    }
   /* else whole_block[Y-1+(X-1)*limy] = 0;*/
  }

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=8 || nlhs!=0)
  {
    mexErrMsgTxt("Usage format is decodenetout2(trainpath,pathsize,tblock_size,whole_block,limx,limy,netout,pathlogics)");
    return;
  }

  decodenetout2(nlhs, plhs, nrhs, prhs);

}
