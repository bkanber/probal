function consolidate_files()
global num_files

num_files = 0;

do_dir('G:\Aixplorer');

disp([num2str(num_files) ' files copied']);

end

function do_dir(dir_loc)
global num_files

    dir_list = dir(strcat(dir_loc,'\*'));

    for i=1:numel(dir_list)
        
        if strncmp(dir_list(i).name,'.',1)==1
            continue
        end
        
        fullname = strcat(dir_loc,'\',dir_list(i).name);
        
        if dir_list(i).isdir==1
            do_dir(fullname);
        else
            if strcmpi(dir_list(i).name,'DICOMDIR') 
                continue;
            end
            if strcmpi(dir_list(i).name,'dcmdir.lst') 
                continue;
            end
            if strendswith(dir_list(i).name,'.inf')
                continue;
            end
            
            disp(fullname);
            
            destname = 'Y:\PVA Stenosis Phantoms\DATASET_1\';
            
            destname = strcat(destname,num2str(num_files+1),'.dcm');
            
            if false
                [status, msg] = copyfile(fullname,destname);
            end
            
            if ~isempty(msg)
                msgbox(msg,'modal');
            else
                num_files=num_files+1;
            end
        end
    end
end
