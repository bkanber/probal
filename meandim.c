 /* =============================================================
 * meandim.c - used by vesselmo.m
 *
 * Computational Function
 * Determines object dimension over chosen area
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

/* **************************************************************** */
/* intline - returns the intersection point of the lines            */
/* ax + by + c = 0 and px + qy + r = 0                              */
/*                                                                  */
/* Return value:                                                    */
/* The return value is 0 if the lines do not intersect or -1 if     */
/* they intersect at infinite number of points.                     */
/* Otherwise a value of 1 is returned.                              */   
/*                                                                  */
/* History:                                                         */
/* 30 Jul 03 - Initial version written (BKanber)                    */
/* **************************************************************** */

int intline(double a, double b, double c, double p, double q, double r, double *int_x, double *int_y)
{
    double det;
    
	if (!b)
	{
        if (!q)
        {
            if (c/a==r/p) return -1;
            else
                return 0;    
        }
        
        *int_x = -c/a;
        *int_y = (-r-p*(*int_x))/q;
        return 1;
	}
	
	if (!q)
	{
        *int_x = -r/p;
        *int_y = (-c-a*(*int_x))/b;
        return 1;
	}
	
	det = (a*q-p*b);
	
	if (!det)
	{
        /* parallel lines (or even maybe overlapping) */
        double det2 = (r*b-c*q);
        
        if (!det2) return -1;
        else
            return 0;
    }
	
	*int_x = (r*b-c*q)/det;
	*int_y = (-c-a*(*int_x))/b;
	
	return 1;
}	

/*
Function: meandim in meandim.c
Usage format is [mdim,errors] = meandim(linedata,chosenarea,nivec,njvec,ppmmx,ppmmy)
*/

void meandim(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
mxArray *linedata = prhs[0];

double *chosenarea = mxGetPr(prhs[1]);

double nivec = *mxGetPr(prhs[2]);
double njvec = *mxGetPr(prhs[3]);

double ppmmx = *mxGetPr(prhs[4]);
double ppmmy = *mxGetPr(prhs[5]);

int numlines = mxGetM(prhs[0]);

int this_pos, i, j;

double the_x1 = chosenarea[0];
double the_x2 = chosenarea[1];
double the_y1 = chosenarea[2];
double the_y2 = chosenarea[3];

int armlen = (int)chosenarea[4];

double satx[10];
double saty[10];
int satn;

mxArray *lcldistarray = mxCreateDoubleMatrix(1,1+armlen*2,mxREAL);

double *lcldist = mxGetPr(lcldistarray);

int errors = 0;

for (this_pos = -armlen; this_pos <= +armlen; this_pos++)
{        
    double x1 = the_x1 + this_pos * nivec;
    double y1 = the_y1 + this_pos * njvec;
    double x2 = the_x2 + this_pos * nivec;
    double y2 = the_y2 + this_pos * njvec;
    
    double dely = y2 - y1;
	double delx = x2 - x1;
	
	double a = -dely;
	double b = delx;
	double c = dely*x1-delx*y1;
	
	satn = 0;
	
	if (numlines > 2)
	    printf("Condition (numlines<=2) is not satisfied...\n");
    
    for (i = 1; i <= numlines; i++) /* each line */
    {
        mxArray *xdataA = mxGetCell(linedata,i-1);
        mxArray *ydataA = mxGetCell(linedata,i-1+numlines);

        double *xdata = mxGetPr(xdataA);
        double *ydata = mxGetPr(ydataA);

        int L = mxGetNumberOfElements(xdataA);
        
        for (j = 1; j <= L-1; j++)
        {
            double midy = (ydata[j+1-1]+ydata[j-1])/2;
            double midx = (xdata[j+1-1]+xdata[j-1])/2;

            double dely = ydata[j+1-1]-ydata[j-1];
            double delx = xdata[j+1-1]-xdata[j-1];
            
			double p = -dely;
			double q = delx;
			double r = dely*xdata[j-1]-delx*ydata[j-1];
            
            double side_len = sqrt(dely*dely+delx*delx)/2;
            
            double int_x;
            double int_y;

            if (intline(a,b,c,p,q,r,&int_x,&int_y)==+1)
            {
                double delx = int_x-midx;
                double dely = int_y-midy;
                
                double len = sqrt(delx*delx+dely*dely);
                
                if (len-side_len<=1e-8)
                {
                    int k;
                    int f = 0;
                    
                    for (k = 0; k < satn; k++)
                    {
                        if (fabs(satx[k]-int_x)<1e-2 &&
                            fabs(saty[k]-int_y)<1e-2
                        )
                        {
                            f = 1;
                            break;
                        }
                    }

                    if (!f)
                    {
                        if (satn<10)
                        {
                            satx[satn] = int_x;
                            saty[satn] = int_y;
                        }
                        
                        satn++;
                    }
                }
            }
            
        } /* each point */
           
    } /* each line */
    
    if (satn>2) 
    {
        int i;
        printf("too many this_pos = %d/%d (%d)\n",this_pos,armlen,satn);
        for (i=0;i<satn;i++)
            printf("(%.4f,%.4f)\n",satx[i],saty[i]);
    }

    if (satn<2)
        printf("too few this_pos = %d/%d (%d)\n",this_pos,armlen,satn);

    if (satn==2)
    {        
        dely = (saty[1] - saty[0]) / ppmmy;
        delx = (satx[1] - satx[0]) / ppmmx;
        
        lcldist[this_pos+armlen+1-1] = sqrt(dely*dely+delx*delx);
    }
    else
    {
        int index = this_pos+armlen+1-1;
        
        if (!index) lcldist[index] = 0;
        else
            lcldist[index] = lcldist[index-1];
            
        errors++;
    }
    
} /* for this_pos */

plhs[0] = lcldistarray;
plhs[1] = mxCreateScalarDouble(errors);

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=6 || nlhs!=2)
  {
    mexErrMsgTxt("Usage format is [mdim,errors] = meandim(linedata,chosenarea,nivec,njvec,ppmmx,ppmmy)");
    return;
  }

  meandim(nlhs, plhs, nrhs, prhs);

}
