 /* =============================================================
 * marginal.c - used by vesselmo.m
 *
 * Computational function
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

/*
Usage format is [xfs,yfs] = marginal(netout0, selx0, sely0, ldimx, ldimy)
*/

void marginal(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *netout0 = mxGetPr(prhs[0]);
  double *selx0 = mxGetPr(prhs[1]);
  double *sely0 = mxGetPr(prhs[2]);

  long ldimx = (long)(*mxGetPr(prhs[3]));
  long ldimy = (long)(*mxGetPr(prhs[4]));

  int i, dim = mxGetN(prhs[0]);
  
  double *xfs;
  double *yfs;
  
  plhs[0] = mxCreateDoubleMatrix(1,ldimx,mxREAL);

  xfs = mxGetPr(plhs[0]);

  plhs[1] = mxCreateDoubleMatrix(1,ldimy,mxREAL);

  yfs = mxGetPr(plhs[1]);
  
  for (i = 0; i < ldimx; i++)
    xfs[i] = 0;

  for (i = 0; i < ldimy; i++)
    yfs[i] = 0;

  for (i = 0; i < dim; i++)
  {
      if (netout0[i]>0.5) 
      {
          xfs[(int)(selx0[i]-1)]++;
          yfs[(int)(sely0[i]-1)]++;
      }
  }
  
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=5 || nlhs!=2)
  {
    mexErrMsgTxt("Usage format is [xfs,yfs] = marginal(netout0, selx0, sely0, ldimx, ldimy)");
    return;
  }

  marginal(nlhs, plhs, nrhs, prhs);

}
