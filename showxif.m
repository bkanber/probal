function showxif(filename)

if (isempty(filename))
    [nm,nm_path] = uigetfile('*.xif','Please choose an .xif file');
 
    filename = strcat(nm_path,nm);
end

this_frame = [];
width = [];
height = [];
num_frames = [];
result = 0;

fhd = fopen(filename,'rt');

if (fhd==-1)
    disp('Error opening the .xif file');
    return;
end
    
str0 = fscanf(fhd, '%s\n');

width = GetToken(str0, 'echoNumLines')
height = GetToken(str0, 'echoNumDisplaySamples')
num_frames = GetToken(str0, 'numFrames')
echoFrameSize = GetToken(str0, 'echoFrameSize')
colorFrameSize = GetToken(str0, 'colorFrameSize')
echoDepth_displayStartDepth = GetToken(str0, 'echoDepth_displayStartDepth')
echoDepth_displayStopDepth = GetToken(str0, 'echoDepth_displayStopDepth')
echoScan_linearWidth = GetToken(str0, 'echoScan_linearWidth')

heightmm = echoDepth_displayStopDepth-echoDepth_displayStartDepth;
widthmm = echoScan_linearWidth;

if (heightmm)
    mmaspectratio = widthmm/heightmm;
else
    mmaspectratio = NaN;
end

if (not(isempty(colorFrameSize)))
    disp('This .xif file has colour information');
end

fclose(fhd);

b = isempty(width);
b = b | isempty(height);
b = b | isempty(num_frames);
b = b | isempty(echoFrameSize);

if (b)
    disp('The format of this .xif file has not been recognised');
    return;
end

b = width<80;
b = b | (width>2000);
b = b | (height<100);
b = b | (height>2000); 
b = b | (num_frames<1);

if (b)
    disp('This .xif file is not supported');
    return;
end

fhd = fopen(filename,'rb');

if (fhd==-1)
    disp('Error opening the .xif file');
    return;
end

while(1)
    str0 = fgets(fhd);
    
    if (str0==-1)
        fclose(fhd);
        disp('The format of this .xif file has not been recognised');
        return;
    end
    
    if not(isempty(strfind(str0,'EOH')))
        break;
    end
end

fpos = ftell(fhd);

fclose(fhd);

fhd = fopen(filename,'rb');

if (fhd==-1)
    disp('Error opening the .xif file');
    return;
end

% fseek(fhd,fpos-5,'bof');
% 
% [A,c]=fread(fhd,30,'uchar');
% char(A')

% struct
% {
%      unsigned char echoFrame[echoFrameSize];
%      unsigned char colorFrame[colorFrameSize];
%     } twodData[numFrames];
%     unsigned short echo2dTimeTag[numFrames];
%     unsigned short color2dTimeTag[numFrames];
%     unsigned char echo2dScanPosition[numFrames];
%     unsigned short echo2dFlags[numFrames];
%     unsigned short color2dFlags[numFrames];
% } TwodImageDataFormat;

fullFrameSize = echoFrameSize;

if (not(isempty(colorFrameSize)))
    fullFrameSize = fullFrameSize + colorFrameSize;
end

for frame_no=1:num_frames
    disp(sprintf('%d/%d',frame_no,num_frames));
    
	if (fseek(fhd,fpos+(frame_no-1)*fullFrameSize,'bof')==-1)
        fclose(fhd);
        disp('Error reading the .xif file');
        return;
	end
    
    [this_frame,count] = fread(fhd,[height,width],'uint8');
    
	if (not(count==echoFrameSize))
        fclose(fhd);
        disp('Error reading the .xif file');
        return;
	end
    
	consimwidth = 256;    
	inconsimheight = round(consimwidth/mmaspectratio);
	
	this_frame = imresize(this_frame,[inconsimheight,consimwidth],'bilinear');
      
%     figure;
%     imagesc(this_frame,[0 255]), colormap(gray);
    
    [c,map] = gray2ind(uint8(this_frame),256);
    
    M(frame_no) = im2frame(c,map);
    
    drawnow;
end

result = 1;

fclose(fhd);

avinm = 'showxif.avi';

if (exist(avinm))
    delete(avinm);
end

movie2avi(M,avinm,'compression','None','quality',100);
winopen(avinm);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function val = GetToken(str,token)

k = strfind(str, token);

if (isempty(k))
    val = [];
    return;
end

str = str(k:end);

k = strfind(str, 'value=');

if (isempty(k))
    val = [];
    return;
end

str = str(k:end);

val = sscanf(str,'value="%d"');

