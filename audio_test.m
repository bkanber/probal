t=get(gco,'XData');
distmm=get(gco,'YData');

f_samp_modulated_signal = 44E3;
t_samp_modulated_signal = 1/f_samp_modulated_signal;
f_carrier = 100; % 30E3
t_samp = (0:t_samp_modulated_signal:max(t));
audio_data = interp1(t,distmm,t_samp,'cubic');
%a = (audio_data-min(audio_data))/(max(audio_data)-min(audio_data));
%fDoppler = (audio_data-min(audio_data));
%fDoppler = exp((audio_data-min(audio_data))*30);
%fDoppler = (fDoppler-min(fDoppler))/(max(fDoppler)-min(fDoppler))*300;
%fDoppler = (audio_data-min(audio_data))/(mean(audio_data)+2*std(audio_data)-min(audio_data))*250;
fDoppler = (audio_data-mean(audio_data))/(2*std(audio_data))*250;
clear vz;
phase_corr = zeros(1,length(fDoppler));
for i=2:length(phase_corr)
    phase_corr(i) = phase_corr(i-1)+2*pi*(fDoppler(i-1)+f_carrier)*t_samp(i-1)-2*pi*(fDoppler(i)+f_carrier)*t_samp(i-1);
end
audio_data = sin(2*pi*(f_carrier+fDoppler).*t_samp+phase_corr);
clear fDoppler;
clear a;
%audio_data = (audio_data-min(audio_data))/(max(audio_data)-min(audio_data))*2-1;

%audio_data = [audio_data ones(1,length(audio_data)/5*0.25)*(-1)];
%audio_data = repmat(audio_data,1,10);

wavwrite(audio_data,f_samp_modulated_signal,8,'audio_test.wav'); % [audio_data' audio_data']
winopen('audio_test.wav');
