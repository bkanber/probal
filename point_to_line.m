% from http://www.mathworks.co.uk/support/solutions/en/data/1-1BYSR/
% Is there a function in MATLAB that calculates the shortest distance from a point to a line?
% 
% Solution:
% The ability to automatically calculate the shortest distance from a point to a line is not available in MATLAB. 
% To work around this, see the following function:
 
function d = point_to_line(pt, v1, v2)
a = v1 - v2;
b = pt - v2;
d = norm(cross(a,b)) / norm(a);

% In this function, pt, v1, and v2 are the three-dimensional coordinates of the point, one vertex on the line, 
% and a second vertex on the line, respectively. The following example illustrates how this function would 
% be called:
% 
% v1 = [0,0,0];
% v2 = [3,0,0];
% pt = [0,5,0];
% distance = point_to_line(pt,v1,v2)
