#include <math.h>

#define Min(A,B) (((A) < (B)) ? (A) : (B))
#define Max(A,B) (((A) > (B)) ? (A) : (B))

const double Pi = 3.14159265358979;
const double Pif180 = 57.29577951308232;
const double PiB180 = 0.01745329251994;

/*
Pi = acos(-1);
Pif180 = ((double)180)/Pi;
PiB180 = Pi/180;
*/

/* round to nearest integer */
double Dround(double A)
{
 double cA = ceil(A);
 double fA = floor(A);

 double cAd = cA-A;
 double fAd = A-fA;

 return (cAd < fAd) ? cA : fA;
}

/* add B degrees (+ve or -ve) to angle A maintain in range 0 to 360 */
double degrees_add(double A, double B)
{
    A += B;
    
    if (fabs(A)>=360) A = A-360*floor(A/360);
    if (A<0) A = 360-fabs(A);
    
    return A;
}

/* return the smaller of the two differences between two angles (both angles need to be in the range 0 to 360) */
double degrees_diff(double A, double B)
{
    double C = fabs(A-B);
    
    if (C<=180) return C;

    return (double)360-C;
}

double Datan2(double Y, double X)
{
    double A = atan2(Y,X)*Pif180;

    if (A<0) return (360+A);
    else return A;
}
