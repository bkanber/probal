% =============================================================
% readxif.m - reads an HDI lab .xif (cineloop) file
%
% Note
% This is a preliminary version
%
% Usage
% Information is returned in structure 'info'
% The specified frame is returned in 'this_frame'
% 'info.result' indicates whether function was successfull
%
% Example
% [info,this_frame] = readxif('myfile.xif',1,[]);
% if (not(info.result))
%   disp('error opening myfile.xif');
% else
%   for i=1:info.num_frames
%     [info,this_frame] = readxif('myfile.xif',i,info);
%     display(this_frame);
%   end
% end
%
% Copyright (c) 2003 Department of Medical Physics
%
% July 03 (Bar?? Kanber) - Preliminary version
% =============================================================

function [info,this_frame] = readxif(filename,frame_no,info)

this_frame = [];

if (isempty(info))
    % retrieve the header and fill the 'info' structure
    info.result = 0;
	
	[fhd,info.syserrmsg] = fopen(filename,'rt');
	
	if (fhd==-1)
        info.errmsg = 'Error opening the .xif file';
        return;
	end
    
    [nm,permission,info.machineformat] = fopen(fhd);
        
	[str0,count] = fscanf(fhd,'%c');
    
    if (count<1)
        info.syserrmsg = ferror(fhd);
        fclose(fhd);
        info.errmsg = 'Error reading the .xif file';
        return;
    end
	
	info.echoNumLines = GetToken(str0, 'echoNumLines', 'int');
	info.echoNumDisplaySamples = GetToken(str0, 'echoNumDisplaySamples', 'int');
	info.num_frames = GetToken(str0, 'numFrames', 'int');
	info.echoFrameSize = GetToken(str0, 'echoFrameSize', 'int');
	info.colorFrameSize = GetToken(str0, 'colorFrameSize', 'int');
	info.frameRate = GetToken(str0, 'frameRate', 'int');
	
	info.echoBounds_depthFirstSample = GetToken(str0, 'echoBounds_depthFirstSample', 'double');
	info.echoDepth_startDepth = GetToken(str0, 'echoDepth_startDepth', 'double');
	info.echoDepth_stopDepth = GetToken(str0, 'echoDepth_stopDepth', 'double');
	info.echoDepth_displayStartDepth = GetToken(str0, 'echoDepth_displayStartDepth', 'double');
	info.echoDepth_displayStopDepth = GetToken(str0, 'echoDepth_displayStopDepth', 'double');
	info.echoScan_linearWidth = GetToken(str0, 'echoScan_linearWidth', 'double')
	
	info.institution = GetToken(str0, 'institution', 'string');
	info.date = GetToken(str0, 'date', 'string');
	info.time = GetToken(str0, 'time', 'string');
	
	info.lastname = GetBracketToken(str0, 'last', 'string');
	info.firstname = GetBracketToken(str0, 'first', 'string');
	
	info.patient_id = GetBracketToken(str0, 'codeMeaning="Id"', 'string');
	
	if (not(isempty(info.colorFrameSize)))
        disp('Note: this .xif file has colour information');
	end
	
	info.fullFrameSize = info.echoFrameSize;
	
	if (not(isempty(info.colorFrameSize)))
        info.fullFrameSize = info.fullFrameSize + info.colorFrameSize;
	end

	info.heightmm = info.echoDepth_displayStopDepth-info.echoDepth_displayStartDepth;
    info.widthmm = info.echoScan_linearWidth;
    
    if (info.heightmm)
        info.aspectratio = info.widthmm/info.heightmm;
    else
        info.aspectratio = NaN;
    end
    
    info.fpos = NaN;

    info.width = 256;
    info.height = info.width/info.aspectratio;
    
	if (info.heightmm)
        info.scale_ppmmy = info.height/info.heightmm;
	else
        info.scale_ppmmy = NaN;
	end
    
    if (info.widthmm)
        info.scale_ppmmx = info.width/info.widthmm;
    else
        info.scale_ppmmx = NaN;
    end

	fclose(fhd);
end

info.result = 0;

b = isempty(info.echoNumLines);
b = b | isempty(info.echoNumDisplaySamples);
b = b | isempty(info.num_frames);
b = b | isempty(info.echoFrameSize);

if (b)
    info.errmsg = 'The format of this .xif file has not been recognised';
    return;
end

b = info.echoNumLines < 80;
b = b | (info.echoNumLines > 2000);
b = b | (info.echoNumDisplaySamples < 100);
b = b | (info.echoNumDisplaySamples > 2000); 
b = b | (info.num_frames < 1);

if (b)
    info.errmsg = 'The format of this .xif file is not supported';
    return;
end

if (isnan(info.fpos))
    % determine where the header finishes and data starts
	[fhd,info.syserrmsg] = fopen(filename,'rb');
	
	if (fhd==-1)
        info.errmsg = 'Error opening the .xif file';
        return;
	end
	
	while(1)
        str0 = fgets(fhd);
        
        if (str0==-1)
            info.syserrmsg = ferror(fhd);
            fclose(fhd);
            info.errmsg = 'The format of this .xif file has not been recognised';
            return;
        end
        
        if not(isempty(strfind(str0,'EOH')))
            break;
        end
	end
	
	info.fpos = ftell(fhd);
	
	fclose(fhd);
end

[fhd,info.syserrmsg] = fopen(filename,'rb');

if (fhd==-1)
    info.errmsg = 'Error opening the .xif file';
    return;
end

if (fseek(fhd,info.fpos+(frame_no-1)*info.fullFrameSize,'bof')==-1)
    info.syserrmsg = ferror(fhd);
    fclose(fhd);
    info.errmsg = 'Error reading the .xif file';
    return;
end

[this_frame,count] = fread(fhd,[info.echoNumDisplaySamples,info.echoNumLines],'uint8');

if (not(count==info.echoFrameSize))
    info.syserrmsg = ferror(fhd);
    fclose(fhd);
    info.errmsg = 'Error reading the .xif file';
    return;
end

this_frame = imresize(this_frame,[info.height info.width],'bicubic');

info.result = 1;

fclose(fhd);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function val = GetToken(str,token,type)

token = sprintf('"%s"', token);

k = strfind(str, token);

if (isempty(k))
    val = [];
    return;
end

str = str(k:end);

k = strfind(str, 'value=');

if (isempty(k))
    val = [];
    return;
end

switch (type)
    case 'int'
        val = sscanf(str(k:end),'value="%d"');
    case 'double'
        val = sscanf(str(k:end),'value="%f"');
    case 'string'
        str = str(k+length('value=')+1:end);
        
        k = strfind(str, '"');
        
		if (isempty(k))
            val = [];
            return;
		end
        
        val = str(1:k-1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieves values for tokens of the type <token>value</token>
function val = GetBracketToken(str,token,type)

token = strcat(token, '>');

k = strfind(str, token);

if (isempty(k))
    val = [];
    return;
end

str = str(k+length(token):end);

switch (type)
    case 'int'
        val = sscanf(str,'%d<');
    case 'double'
        val = sscanf(str,'%f<');
    case 'string'
        k = strfind(str, '<');
        
		if (isempty(k))
            val = [];
            return;
		end
        
        val = str(1:k-1);
end
