function L = vector_length( V )
% Return vector length given Vx and Vy
    L=sqrt(V(1)^2+V(2)^2);
end
