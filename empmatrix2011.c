 /* =============================================================
 * empmatrix.c
 *
 * Calculates theoretical probability matrix
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

void empmatrix(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  long centre_x = (long)(*mxGetPr(prhs[0]));
  long centre_y = (long)(*mxGetPr(prhs[1]));

  double *la = mxGetPr(prhs[2]);
  double *trainpath = mxGetPr(prhs[3]);
  
  long npoints = mxGetM(prhs[3]);

  long ldimx = (long)(*mxGetPr(prhs[4]));
  long ldimy = (long)(*mxGetPr(prhs[5]));
  long block_size = (long)(*mxGetPr(prhs[6]));
  
  double g_threshold = *mxGetPr(prhs[8]);
  double pfilter = *mxGetPr(prhs[9]);
  
  long i;

  long larm_len = ceil(((double)block_size-1)/2);
  long rarm_len = floor(((double)block_size-1)/2);

  double *whole_block = mxGetPr(prhs[7]);

  long bx, by;
  
  double Gth = g_threshold*256;

  double chromConst = (Gth*Gth)/log(2); /* Chromatic constant */

  for (i = 1; i <= npoints; i++)
  {
		long this_x = trainpath[i-1];
		long this_y = trainpath[i-1+npoints];
		long G = (this_y-1)+(this_x-1)*ldimy;
		double thisGS = la[G];
		/*double WhatP = whole_block[G];*/

        long xl = this_x-larm_len;
        long yu = this_y-larm_len;
        long xr = this_x+rarm_len;
        long yd = this_y+rarm_len;
	
        double P1 = 0;
        double num = 0;
        
        for (bx = xl; bx <= xr; bx++)
        {
            for (by = yu; by <= yd; by++)
            {
                double P;
                unsigned long optI;
				double cd;
                  
				if (by==this_y && bx==this_x) continue;
                  
                if (bx<1 || bx>ldimx || by<1 || by>ldimy) continue;
                            
                optI = (by-1)+(bx-1)*ldimy;
                  
                P = whole_block[optI];
                  
                if (P<=pfilter) continue;

				cd = la[optI]-thisGS;
                
				/*
				if (this_y==288 && this_x==36)
				{
					mexPrintf("by=%ld,bx=%ld,P=%lf,cd=%lf\n",by,bx,P,cd2);
				}
                */    
	
				if (1)
				{
					P1 += P*exp(-(cd*cd)/chromConst);
					num += 1;
				}
				else
				if (1)
				{
					if (P*exp(-(cd*cd)/chromConst)>P1)
					{
						P1 = P*exp(-(cd*cd)/chromConst);
						num=1;
					}
				}
				else
				{
					double wf=255-abs(cd);
					P1 += P*exp(-(cd*cd)/chromConst)*wf;
					num += wf;
				}
                      
            } /* for (by) */
              
        } /* for (bx) */

		if (num==0) 
		{
			static int __once = 0;
			if (__once==0)
			{
				mexPrintf("Variable num remains zero for (%d,%d); supressing further messages.\n", (int)this_x, (int)this_y);
				__once = 1;
			}
		}
        else
        {
            whole_block[G] = P1/num;
	    }
   } /* for npoints */
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs==0 || 1==1)
  {
    mexPrintf("P matrix computation module - Build(8) 19 Nov 2011 - (C) 2003-2011\n\n");
    /*return;*/
  }
  
  if (nrhs!=10 || nlhs!=0)
  {
    mexErrMsgTxt("Usage format is empmatrix(x, y, la, trainpath, ldimx, ldimy, block_size, whole_block, g_threshold, pfilter)");
    return;
  }
  
  empmatrix(nlhs, plhs, nrhs, prhs);

}
