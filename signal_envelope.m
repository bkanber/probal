close all;

f=5E6;
tau=1/f;
t=0:tau/180:tau*30.5;

A = 0.5+abs(sin(2*pi*f/15*t));

figure;
plot(t,A.*cos(2*pi*f*t));
hold on;
plot(t,A,'r');
axis tight;
xlabel('time(s)');
ylabel('signal (arbitrary units)');
plot(t,zeros(1,numel(t)),'k');