plot_x=get(gco,'XData');

plot_y=get(gco,'YData');

text_x = min(plot_x)+(max(plot_x)-min(plot_x))*0.10;

text_y = max(plot_y);

gtext(...
    ['mean = ',num2str(mean(plot_y)),' std = ',num2str(std(plot_y)),' cov = ',num2str(std(plot_y)/mean(plot_y))],...
    'FontName','Delicious','FontSize',14)
