close all;
n=1000;

rng('shuffle');

groups=cat(1,zeros(n,1),ones(n,1));

sii_vals=1.5+rand(n*2,1)-0.5;
dos_vals=1.5+rand(n*2,1)-0.5;
%dos_vals=0.5+(rand(n*2,1)-0.5)*2*0.4;

sii_vals(groups==0)=sii_vals(groups==0)-0.5*round(rand(numel(find(groups==0)),1)*0.75);
sii_vals(groups==1)=sii_vals(groups==1)+0.5*round(rand(numel(find(groups==1)),1)*0.75);

dos_vals(groups==0)=dos_vals(groups==0)-0.5*round(rand(numel(find(groups==0)),1)*0.75);
dos_vals(groups==1)=dos_vals(groups==1)+0.5*round(rand(numel(find(groups==1)),1)*0.75);

model1=sii_vals.*dos_vals;
model2=sii_vals+dos_vals;

figure;
x=cat(1,(10:10+n-1)',(n*2:n*2+n-1)');

subplot(2,4,1);plot(x,sii_vals,'*');subplot(2,4,2);boxplot(sii_vals,groups);
subplot(2,4,3);plot(x,dos_vals,'*');subplot(2,4,4);boxplot(dos_vals,groups);

x=cat(1,(10:10+n-1)',(n*2:n*2+n-1)');

subplot(2,4,5);plot(x,model1,'*');
title(num2str(numel(find(model1(n+1:end)>max(model1(1:n))))));
subplot(2,4,6);boxplot(model1,groups);
asymp_avg = mean(model1(1:n));
symp_avg = mean(model1(n+1:end));
title(num2str(symp_avg/asymp_avg));

subplot(2,4,7);plot(x,model2,'*');
title(num2str(numel(find(model2(n+1:end)>max(model2(1:n))))));
subplot(2,4,8);boxplot(model2,groups);
asymp_avg = mean(model2(1:n));
symp_avg = mean(model2(n+1:end));
title(num2str(symp_avg/asymp_avg));

x=1;

%B=mnrfit(gsm_vals,dos_vals);

% a=50;
% b=50;
% C0=0;
% deltaGSM=1:0.1:5;
% deltaSII=2;
% asymp = C0+a+b;
% symp = C0+deltaGSM.*a+deltaSII.*b;
% ratio = symp./asymp;
% figure;plot(deltaGSM,ratio,'*');

