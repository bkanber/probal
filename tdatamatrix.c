 /* =============================================================
 * tdatamatrix.c - used by vesselmo.m
 *
 * Computational function
 * Calculates training data matrix
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>

/*
Usage format is [datamatrix,newtargets] = tdatamatrix(trainpath,pathsize,tblock_size,whole_block,limx,limy,targets,trunc)
*/

void tdatamatrix(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *trainpath = mxGetPr(prhs[0]);
  double *pathsize = mxGetPr(prhs[1]);

  long tblock_size = (long)(*mxGetPr(prhs[2]));
  
  double *whole_block = mxGetPr(prhs[3]);

  long limx = (long)(*mxGetPr(prhs[4]));
  long limy = (long)(*mxGetPr(prhs[5]));

  double *targets = mxGetPr(prhs[6]);

  long trunc = (long)(*mxGetPr(prhs[7]));

  long larm_len = ceil(((double)tblock_size-1)/2);
  long rarm_len = floor(((double)tblock_size-1)/2);

  long npoints = (long)pathsize[0];
  long i;
  
  long nok = 0;

  long tblock_size2 = tblock_size*tblock_size;
  
  double *datamatrix;
  double *newtargets;
  
  plhs[0] = mxCreateDoubleMatrix(tblock_size2,npoints,mxREAL);
  
  datamatrix = mxGetPr(plhs[0]);

  plhs[1] = mxCreateDoubleMatrix(1,npoints,mxREAL);
  
  newtargets = mxGetPr(plhs[1]);

  for (i = 1; i <= npoints; i++)
  {
    long this_x = trainpath[i-1];
    long this_y = trainpath[i-1+npoints];
    
    long xl = this_x-larm_len;
    long yu = this_y-larm_len;
    long xr = this_x+rarm_len;
    long yd = this_y+rarm_len;
    
    long Q = 1;
    long x , y;

    if (trunc)
    {
        long optI = this_y-1+(this_x-1)*limy;
        
        if (targets[i-1]==1 && whole_block[optI]<0.5)
        {
            printf("Out but left in\n");
            continue;
        }
            
        if (targets[i-1]<1 && whole_block[optI]>=0.2)
        {
            printf("In but left out\n");
            continue;
        }
        
/*        if (rand()<0.8*RAND_MAX) continue;    */
    }
    
    newtargets[nok] = targets[i-1];
        
    for (x = xl; x <= xr; x++)
    {
        for (y = yu; y <= yd; y++)
        {
            long optI = Q-1+(nok-0)*tblock_size2;
            
            if (x<1 || x>limx || y<1 || y>limy)
                datamatrix[optI] = 0;
            else
                datamatrix[optI] = whole_block[y-1+(x-1)*limy];
            
            Q = Q+1;
        }
    }
/*    
    printf("Q=%ld, larm_len=%ld, rarm_len=%ld\n",Q-1,larm_len,rarm_len);
*/ 
    nok++;    
  }
  
  mxSetN(plhs[0],nok);
  mxSetN(plhs[1],nok);
  
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=8 || nlhs!=2)
  {
    mexErrMsgTxt("Usage format is [datamatrix,newtargets] = tdatamatrix(trainpath,pathsize,tblock_size,whole_block,limx,limy,targets,trunc)");
    return;
  }

  tdatamatrix(nlhs, plhs, nrhs, prhs);

}
