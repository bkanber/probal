[Maxima,MaxIdx] = findpeaks(y);
DataInv = 1.01*max(y) - y;
[Minima,MinIdx] = findpeaks(DataInv);
Minima = y(MinIdx);
figure;plot(Minima);