function Y = OversampleMatlab(X,OverSamplingFactor,Method)

Ni = size(X,1);
Nj = size(X,2);
Nt = size(X,3);

OverSamplingFactor = round(OverSamplingFactor);

NewNi = OverSamplingFactor*(Ni-1)+1;
Y = zeros(NewNi,Nj,Nt);


for t=1:Nt
    for j=1:Nj
        if (strcmp(Method,'interp1linear'))
            Y(:,j,t) = interp1((0:(Ni-1)),X(:,j,t),(0:OverSamplingFactor*(Ni-1))/OverSamplingFactor,'linear');
        end; %method interp1linear
        if (strcmp(Method,'Lanczos3'))
            M = 3;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = sinc(x).*sinc(x/M);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Lanczos3
        if (strcmp(Method,'Lanczos4'))
            M = 4;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = sinc(x).*sinc(x/M);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Lanczos4
        if (strcmp(Method,'Linear'))
            M = 1;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = (1-abs(x)).*(abs(x)<1);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Linear
        if (strcmp(Method,'Nearest'))
            M = 1;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = (abs(x)<0.5);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Nearest

        if (strcmp(Method,'Bicubic0.5'))
            M = 2;
            a = -0.5;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = (abs(x)<=1).*((a+2)*x.^3-(a+3)*x.^2+1)+(1<abs(x)).*(abs(x)<=2).*(a*abs(x).^3-5*a*x.^2+8*a*abs(x)-4*a);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Cubic0.5
        if (strcmp(Method,'Bicubic0.75'))
            M = 2;
            a = -0.75;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = (abs(x)<=1).*((a+2)*x.^3-(a+3)*x.^2+1)+(1<abs(x)).*(abs(x)<=2).*(a*abs(x).^3-5*a*x.^2+8*a*abs(x)-4*a);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method Cubic0.75
        if (strcmp(Method,'BSplines'))
            M = 2;
            x = (0:(M*OverSamplingFactor-1))/OverSamplingFactor;
            P = (abs(x)<=1).*(3*x.^3-6*x.^2+4)/6+(1<abs(x)).*(abs(x)<=2)/6.*(-x.^3+6*x.^2-12*x+8);
            for newi = 0:(NewNi-1)
                dy = 0;
                i0 = max(0,ceil (-M + (newi + 1)/OverSamplingFactor));
                i1 = min(floor(+M + (newi - 1)/OverSamplingFactor),Ni-1);
                for i=i0:i1
                    xi = X(1+i,j,t);
                    PIndex = abs(newi - i * OverSamplingFactor);
                    dy = dy + P(PIndex+1)*xi;
                end; %i
                Y(1+newi,j,t) = dy;
            end; %newi
        end;%method BSplines
        
    end;%j
end;%t
