Probal - For research use only - NOT FOR USE IN DIAGNOSIS OR TREATMENT OF PATIENTS

Instructions:
Clone repository
RUN vm.m in MATLAB
Several file formats are supported; however, you will most likely need ultrasound cineloops in DICOM format.

Copyright 2016, Baris Kanber, Centre for Medical Image Computing, University College London
b.kanber@ucl.ac.uk

Please cite:
* Dynamic variations in the ultrasound greyscale median of carotid artery plaques. Cardiovascular Ultrasound 2013; 11: 21.       
* A probabilistic approach to computerized tracking of arterial walls in ultrasound image sequences. ISRN Signal Processing 2012.
