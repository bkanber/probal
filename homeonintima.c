 /* =============================================================
 * homeonintima.c - used by vesselmo.m
 *
 * Tracking function
 * Homes on lumen-intima barrier
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

int CheckNorm(int x1, int y1, double norman, int this_dir, int maxlen, double *la, long ldimx, long ldimy, double *sum)
{
    double thisGS, otherGS, prevGS;
    double this_x, this_y;
    int ithis_x, ithis_y, i;
    double normivec, normjvec;
    
    *sum = 0;
    
    if (x1<1 || x1>ldimx || y1<1 || y1>ldimy)
        return 0;

    thisGS = la[y1-1+(x1-1)*ldimy];

    this_x = x1;
    this_y = y1;

    normivec = cos(norman*PiB180)*this_dir;
    normjvec = -sin(norman*PiB180)*this_dir;

    prevGS = thisGS;
    
    for (i=0;i<maxlen;i++)
    {
		this_x += normivec;
		this_y += normjvec;
		
		ithis_x = Dround(this_x);
		ithis_y = Dround(this_y);

        if (ithis_x<1 || ithis_x>ldimx || ithis_y<1 || ithis_y>ldimy)
            return 0;

        otherGS = la[ithis_y-1+(ithis_x-1)*ldimy];    
        
        *sum += otherGS-thisGS;
    }
    
    return 1;
}    

int SumNormPs(int x1, int y1, double norman, int this_dir, int maxlen, double *whole_block, long ldimx, long ldimy, double *sum)
{
    double P;
    double this_x, this_y;
    int ithis_x, ithis_y, i;
    double normivec, normjvec;
    int num = 0;
    
    *sum = 0;
    
    if (x1<1 || x1>ldimx || y1<1 || y1>ldimy)
        return 0;

    this_x = x1;
    this_y = y1;

    normivec = cos(norman*PiB180)*this_dir;
    normjvec = -sin(norman*PiB180)*this_dir;

    for (i=0;i<maxlen;i++)
    {
		this_x += normivec;
		this_y += normjvec;
		
		ithis_x = Dround(this_x);
		ithis_y = Dround(this_y);

        if (ithis_x<1 || ithis_x>ldimx || ithis_y<1 || ithis_y>ldimy)
            break;

        P = whole_block[ithis_y-1+(ithis_x-1)*ldimy];    
        
        *sum += P;
        num++;
    }
    
    if (num) *sum /= num;
    
    return 1;
}    
/*
int FindLclMax(int x1, int y1, double norman, int this_dir, int maxlen, double *whole_block, long ldimx, long ldimy, double *area, int *dist)
{
    double P;
    double this_x, this_y;
    int ithis_x, ithis_y, i;
    double normivec, normjvec;
    int num = 0;
    
    *area = 0;
    *dist = 0;
    
    if (x1<1 || x1>ldimx || y1<1 || y1>ldimy)
        return 0;

    this_x = x1;
    this_y = y1;

    normivec = cos(norman*PiB180)*this_dir;
    normjvec = -sin(norman*PiB180)*this_dir;

    for (i=0;i<maxlen;i++)
    {
		this_x += normivec;
		this_y += normjvec;
		
		ithis_x = Dround(this_x);
		ithis_y = Dround(this_y);

        if (ithis_x<1 || ithis_x>ldimx || ithis_y<1 || ithis_y>ldimy)
            break;

        P = whole_block[ithis_y-1+(ithis_x-1)*ldimy];    
        
        *sum += P;
        num++;
    }
    
    if (num) *sum /= num;
    
    return 1;
}    
*/

/*
Function: homeonintima in homeonintima.c
Usage format is [axdata,aydata,ixdata,iydata] = homeonintima(xdata,ydata,la,whole_block,limx,limy,centre_x,centre_y,gdata,coords_xy)
*/

void homeonintima(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *xdata = mxGetPr(prhs[0]);
  double *ydata = mxGetPr(prhs[1]);
  double *la = mxGetPr(prhs[2]);
  double *whole_block = mxGetPr(prhs[3]);

  long ldimx = (long)(*mxGetPr(prhs[4]));
  long ldimy = (long)(*mxGetPr(prhs[5]));
  
  int centre_x = (int)(*mxGetPr(prhs[6]));
  int centre_y = (int)(*mxGetPr(prhs[7]));

  double *gdata = mxGetPr(prhs[8]);

  int coords_xy = (int)(*mxGetPr(prhs[9]));
  
  int npoints = mxGetN(prhs[0]);
  int i;
  
  int x1, y1;
  double an, norman;
  double delx, dely;
  int prevanP = 0;
  
  int FlipDir = 0;
  int nowInt;
  
  double this_x;
  double this_y;
  
  int ithis_x;
  int ithis_y;

  int Justed, Taken, Det, Backtrack;

  double normivec;
  double normjvec;

  double otherGS;
  double thisGS;
  
  double *axdata;
  double *aydata;
  double *ixdata;
  double *iydata;

  int nadata = 0;
  int nidata = 0;

  plhs[0] = mxCreateDoubleMatrix(1,npoints,mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1,npoints,mxREAL);
  plhs[2] = mxCreateDoubleMatrix(1,npoints,mxREAL);
  plhs[3] = mxCreateDoubleMatrix(1,npoints,mxREAL);
      
  axdata = mxGetPr(plhs[0]);
  aydata = mxGetPr(plhs[1]);
  ixdata = mxGetPr(plhs[2]);
  iydata = mxGetPr(plhs[3]);
  
  for (i=0; i<npoints; i++)
  {
    x1 = xdata[i];
    y1 = ydata[i];
    
	if (x1<1 || x1>ldimx || y1<1 || y1>ldimy)
        continue;
    /*
    if (x1<50 || x1>ldimx-50 || y1<50 || y1>ldimy-50)
        continue;
    */    
    
    an = Datan2(-gdata[i],1);
    
    if (!coords_xy)
        an = degrees_add(90,-an);
    
    norman = degrees_add(an,90);

    if (i<50)
    {
		double suma, sumb;
		
		SumNormPs(x1, y1, norman, +1, 20, whole_block, ldimx, ldimy, &suma);
		SumNormPs(x1, y1, norman, -1, 20, whole_block, ldimx, ldimy, &sumb);
		
		if (sumb<suma)
		{
            printf("Flip direction\n");
            norman = degrees_add(an,-90);
            FlipDir++;
		}
		else
		    FlipDir--;
    }
    else
    {
        if (FlipDir>0)
        {
            printf("Flip direction [%d]\n", FlipDir);
            norman = degrees_add(an,-90);
        }
    }
  
    normivec = cos(norman*PiB180);
    normjvec = -sin(norman*PiB180);
        
    this_x = x1;
    this_y = y1;
    
    thisGS = la[y1-1+(x1-1)*ldimy];
        
    for (nowInt = 0; nowInt<=1; nowInt++)
    {
        double LoT, HiT;

        Justed = 0;
        Backtrack = 0;
        Det = 0;
        Taken = 0;
        
        if (nowInt)
        {
            normivec = -normivec;
            normjvec = -normjvec;
            HiT = 4+1.0*20;
            LoT = 4+0.5*20;
        }
        else
        {
            HiT = 4+2*20;
            LoT = 4+1*20;
        }

        if (!nowInt && thisGS>=LoT)
        {
            Backtrack = 1;
            normivec = -normivec;
            normjvec = -normjvec;
            printf("(%d,%d) needs backtracking\n",x1,y1);
        }
       
        while (1)
        {
            ithis_x = Dround(this_x);
            ithis_y = Dround(this_y);
            /*
            printf("(%d,%d)\n",ithis_x,ithis_y);
            */
			if (ithis_x<1 || ithis_x>ldimx || ithis_y<1 || ithis_y>ldimy)
                break;
                
            otherGS = la[ithis_y-1+(ithis_x-1)*ldimy];    

            if (!Justed)
            {
                if (Backtrack)
                {
                    if (otherGS<LoT)
                    {
                        Justed = 1;
                        normivec = -normivec;
                        normjvec = -normjvec;
                        printf("backtracked to (%d,%d)\n", ithis_x,ithis_y);
                        Backtrack = 0;
                        Justed = 0;
                    }
                }
                else
                {
                    if (otherGS>=HiT)
                    {
                        Justed = 1;
                        printf("justified to (%d,%d)\n", ithis_x,ithis_y);
                    }
                }
            }
            else
            {
                if (otherGS<HiT)
                {
                    
                    double delx = ithis_x-x1;
                    double dely = ithis_y-y1;
                    double len = sqrt(delx*delx+dely*dely);
                    if (len>40)
                    {
                        printf("\n*** len is >40 ");
                        break;
                    }
                    
	
                    if (!Taken)
                    {
                        if (nowInt)
                        {
                            ixdata[nidata] = ithis_x;
                            iydata[nidata] = ithis_y;
                        }
                        else
                        {
                            axdata[nadata] = ithis_x;
                            aydata[nadata] = ithis_y;
                        }
                        
                        printf("nowInt=%d added(%d,%d)\n",nowInt,ithis_x,ithis_y);
                        
                        Taken = 1;
                        
                        if (otherGS<LoT)
                        {
                            Taken = 0;

                            if (nowInt) nidata++;
                            else nadata++;
                            
                            Det = 1;
                            break;
                        }
                    }
                    else
                    {
                        Taken++;
	
                        if (Taken>4)
                        {
                            if (nowInt)
                            {
                                this_x = ixdata[nidata];
                                this_y = iydata[nidata];
                            }
                            else
                            {
                                this_x = axdata[nadata];
                                this_y = aydata[nadata];
                            }

                            if (nowInt) nidata++;
                            else nadata++;

                            Det = 1;
                            break;
                        }
	
                    }
                    
                }
                else Taken = 0;
                
               
            } /* else */

            cont:
            this_x += normivec;
            this_y += normjvec;
            
        } /* while (1) */
        
        if (!Det) 
        {
            printf("Not determined\n");
            break;
        }
        
    } /* for (nowInt) */
    
  } /* for npoints */

  mxSetN(plhs[0], nadata);
  mxSetN(plhs[1], nadata);
  mxSetN(plhs[2], nidata);
  mxSetN(plhs[3], nidata);
  
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=10 || nlhs!=4)
  {
    mexErrMsgTxt("Usage format is [axdata,aydata,ixdata,iydata] = homeonintima(xdata,ydata,la,whole_block,limx,limy,centre_x,centre_y,gdata,coords_xy)");
    return;
  }

  homeonintima(nlhs, plhs, nrhs, prhs);

}
