x = min(inside_points_x):max(inside_points_x);
y = min(inside_points_y):max(inside_points_y);
clf
[xx,yy] = meshgrid(x,y);
zz = zeros(length(y),length(x));
for i=1:length(inside_points_x)
    ygrid = inside_points_y(i)-min(inside_points_y)+1;
    xgrid = inside_points_x(i)-min(inside_points_x)+1;
    zz(length(y)-ygrid+1,xgrid)=255-whole_a(inside_points_y(i),inside_points_x(i));
end
%figure
hold on
pcolor(x,y,zz);
axis equal;
colormap(jet);
shading interp
[px,py] = gradient(zz,.2,.2);
%quiver(x,y,px,py,2,'k');
axis off
hold off

% x = min(inside_points_x):max(inside_points_x);
% y = min(inside_points_y):max(inside_points_y);
% clf
% [xx,yy] = meshgrid(x,y);
% zz = zeros(length(y),length(x));
% for yy=1:length(y)
%     for xx=1:length(x)
%         yactual = min(inside_points_y)+yy-1;
%         xactual = min(inside_points_x)+xx-1
%         zz(length(y)-yy+1,xx)=whole_a(yactual,xactual);
%     end
% end
% %figure
% hold on
% pcolor(x,y,zz);
% axis equal;
% colormap(jet);
% shading interp
% [px,py] = gradient(zz,.2,.2);
% quiver(x,y,px,py,2,'k');
% axis off
% hold off
