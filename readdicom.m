function frame_data = readdicom(filename, frame_no)
global handles
global DICOMfilename
global fileinfo
global frame_limiter_num
global frame_limiter_offset

PRELOAD=1; % load all frames at first read (memory intensive)
global PRELOAD_data
global PRELOAD_filename

if isempty(frame_limiter_num)
    frame_limiter_num = 0;
end

if isempty(frame_limiter_offset)
    frame_limiter_offset = 0;
end

if frame_no==1 || isempty(DICOMfilename) || ~strcmp(filename,DICOMfilename)
    
    fileinfo = dicominfo(filename);
    
    if isfield(fileinfo,'NumberOfFrames')
        
        if frame_limiter_num~=0
            fileinfo.NumberOfFrames = min(fileinfo.NumberOfFrames,frame_limiter_num);
        end

        if frame_limiter_num==0 && frame_limiter_offset~=0
            fileinfo.NumberOfFrames = fileinfo.NumberOfFrames-frame_limiter_offset;
        end
        
    end
    
    if isfield(fileinfo,'ManufacturerModelName') && strcmpi(fileinfo.ManufacturerModelName,'Aixplorer') && isfield(fileinfo,'CineRate')
        fileinfo.FrameRate = fileinfo.CineRate;
    else
        if isfield(fileinfo,'FrameTime')
            assert(fileinfo.FrameTime>0);

            fileinfo.FrameRate = 1/(fileinfo.FrameTime*1E-3);
        end
    end
    
    if (isfield(fileinfo,'SequenceOfUltrasoundRegions'))
        for r_no=1:99
            field_name=sprintf('Item_%d',r_no);

            if (~isfield(fileinfo.SequenceOfUltrasoundRegions,field_name)) 
                break;
            end
            
            fld = fileinfo.SequenceOfUltrasoundRegions.(field_name);
            
            if (fld.RegionSpatialFormat~=1) % Make sure spatial format is '2D'
                continue;
            end
            
            if (fld.RegionDataType~=1) % Make sure region data type is 'tissue'
                continue;
            end
            
            assert(~isfield(fld,'DopplerCorrectionAngle'));
            
            if (isfield(fld,'DopplerCorrectionAngle'))
                continue;
            end
            
            if (fld.PhysicalUnitsXDirection~=3)
                continue;
            end
            
            assert(fld.PhysicalUnitsXDirection==3); % Make sure units are 'cm'
            
            assert(fld.PhysicalUnitsYDirection==3); % Make sure units are 'cm'
            
            ppmmx = 1/(fld.PhysicalDeltaX*10);
            
            ppmmy = 1/(fld.PhysicalDeltaY*10);

            if isfield(fileinfo,'ppmmx') % Make sure no previous US region defined the ppmmx (if so not different)
                
                assert(fileinfo.ppmmx==ppmmx);
                
            end

            if isfield(fileinfo,'ppmmy') % Make sure no previous US region defined the ppmmy (if so not different)
                
                assert(fileinfo.ppmmy==ppmmy);
                
            end
           
            fileinfo.ppmmx = ppmmx;
            
            fileinfo.ppmmy = ppmmy;
        end
    end
    
    fileinfo.MyFileName = filename;

    DICOMfilename = filename;
end

if PRELOAD==1 && (isempty(PRELOAD_filename) || ~strcmp(filename,PRELOAD_filename))
    PRELOAD_data = [];
    PRELOAD_filename = [];
end

if PRELOAD==1 && isempty(PRELOAD_data)
    PRELOAD_data = dicomread(filename);
    PRELOAD_filename = filename;
end
    
if isfield(fileinfo,'TrueNumberOfFrames') % Syntetically enlengthened image sequence
    frame_no = min(frame_no, fileinfo.TrueNumberOfFrames);
end

if PRELOAD==1
    frame_data = PRELOAD_data(:,:,:,frame_no+frame_limiter_offset);
else
    frame_data = dicomread(filename,'frames',frame_no+frame_limiter_offset);
end

if numel(size(frame_data))==3
    frame_data = rgb2hsv(frame_data);
    
    if false % blacken colour flow
        sat = frame_data(:,:,2);
        frame_data = frame_data(:,:,3)*255;
        ind = find(sat>0);
        if numel(ind)>0
            frame_data(ind) = 0;
        end
    else
        frame_data = frame_data(:,:,3)*255;
    end
end

if ~strcmpi(class(frame_data),'double')
    frame_data = double(frame_data);
end

frame_data = frame_data-min(min(frame_data));

frame_data = frame_data/max(max(frame_data));

frame_data = frame_data*255;

if false % invert
    frame_data = 255-frame_data;
end

% custom scaling
if 0
    scale_f = 4;

    frame_data = imresize(frame_data,scale_f,'nearest');

    fileinfo.ppmmx = fileinfo.ppmmx*scale_f;
    fileinfo.ppmmy = fileinfo.ppmmy*scale_f;
end

x1 = str2double(get(handles.edit11,'String'));
y1 = str2double(get(handles.edit12,'String'));
x2 = str2double(get(handles.edit13,'String'));
y2 = str2double(get(handles.edit14,'String'));

if x1>=1 && x1<=size(frame_data,2) && x2>=1 && x2<=size(frame_data,2) && x2>=x1 && ...
   y1>=1 && y1<=size(frame_data,1) && y2>=1 && y2<=size(frame_data,1) && y2>=y1
    
    frame_data = frame_data(y1:y2,x1:x2);

end

% add noise
if 0
    frame_data = double(imnoise(uint8(frame_data),'gaussian',0,0.12));
end

% motion phantom
if 0
    frame_data = frame_data*0;
    %plaque_x = 400+200*sin((frame_no-1)/60*2*pi);
    plaque_x = (100+(frame_no-1)*1.1);
    plaque_y = 100;
    plaque_phantom=load('plaque_phantom');
    frame_data(plaque_y-20:plaque_y+20,plaque_x-20:plaque_x+20) = plaque_phantom.plaque_phantom;
end

if get(handles.checkbox25,'Value')==1
    if ~isfield(fileinfo,'NumberOfFrames') % Synthetically enlengthen into a image sequence
        fileinfo.NumberOfFrames = 2;
        fileinfo.TrueNumberOfFrames = 1;
        fileinfo.FrameRate = 1;
    end
end

FLAG_DO_NOT_ASK_FOR_MISSING_DICOM_HEADER_INFO = true;

if ~isfield(fileinfo,'FrameRate')
    fileinfo.FrameRate=1;
end

if ~FLAG_DO_NOT_ASK_FOR_MISSING_DICOM_HEADER_INFO
    if ~isfield(fileinfo,'FrameRate')
        prompt = {'Enter frame rate in Hz'};
        name = 'Enter frame rate information';
        numlines=1;

        global def_framerate;

        if isempty(def_framerate) 
            def_framerate = '';
        end

        defaultanswer={def_framerate};

        options.Resize='on';
        options.WindowStyle='modal';
        options.Interpreter='tex';

        answer=inputdlg(prompt,name,numlines,defaultanswer,options);

        if ~isempty(answer)
            fileinfo.FrameRate = str2num(answer{1});
            def_framerate = answer{1};
        end
    end

    if ~isfield(fileinfo,'ppmmx') || ~isfield(fileinfo,'ppmmy')
        prompt = {'Enter x resolution in pixels per mm','Enter y resolution in pixels per mm'};
        name = 'Enter resolution information';
        numlines=1;

        global def_ppmmx;
        global def_ppmmy;

        if isempty(def_ppmmx) 
            def_ppmmx = '';
        end

        if isempty(def_ppmmy) 
            def_ppmmy = '';
        end

        if isfield(fileinfo,'ppmmx')
            def_ppmmx = num2str(fileinfo.ppmmx);
        end

        if isfield(fileinfo,'ppmmy')
            def_ppmmy = num2str(fileinfo.ppmmy);
        end

        defaultanswer={def_ppmmx,def_ppmmy};

        options.Resize='on';
        options.WindowStyle='modal';
        options.Interpreter='tex';

        answer=inputdlg(prompt,name,numlines,defaultanswer,options);

        if ~isempty(answer)
            fileinfo.ppmmx = str2num(answer{1});
            fileinfo.ppmmy = str2num(answer{2});
            def_ppmmx = answer{1};
            def_ppmmy = answer{2};
        end
    end
end
