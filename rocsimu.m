function AUC = rocsimu(vals,class)
param = 'param';
units = 'units';
formatSpec = '%.2f';

figure('Color',[1 1 1]);

subplot(2,2,1);

ind=find(class==1);
plot(15:14+numel(vals(ind)),vals(ind),'*b','MarkerSize',10);
xtickASYMP=(15+14+numel(vals(ind)))/2;
mean_asymp = mean(vals(ind));

ind=find(class==2);
hold on;
plot(45:44+numel(vals(ind)),vals(ind),'*r','MarkerSize',10);
xtickSYMP=(45+44+numel(vals(ind)))/2;
mean_symp = mean(vals(ind));

rangeVALS = max(vals)-min(vals);

title([param ' distribution']);
ylabel({param [' (' units ')']});
axis([10 44+numel(vals(ind))+5 min(vals)-rangeVALS*0.10 max(vals)+rangeVALS*0.10]);
set(gca,'XTick',[xtickASYMP;xtickSYMP]);
set(gca,'XTickLabel',{'ASYMP';'SYMP'});

% class_str = [];
% for i=1:numel(class)
%     if class(i)==1 
%         class_str = [class_str;'ASYMP'];
%     else
%         if class(i)==2
%             class_str = [class_str;'SYMP '];
%         else
%             class_str = [class_str;'ERROR'];
%         end
%     end
% end
% boxplot(vals,class_str,'notch','on');
% title('GSM distribution');
% ylabel('GSM (normalized)');

N = numel(find(class==1));
P = numel(find(class==2));

assert(P+N==numel(class),'Check classes are 1(negative) and 2(positive)');

%cutoff_values=0:0.01:4;

L=min(vals);
H=max(vals);
step=(H-L)*0.0005;
cutoff_values=max(0,L-step):step:H+step;

specificity = [];
sensitivity = [];

accuracy = [];

PPV = []; % positive predictive value
NPV = []; % negative predictive value
FDR = []; % false discovery rate

for this_cutoff=1:numel(cutoff_values)
    cutoff = cutoff_values(this_cutoff);
    if (abs(cutoff-1.6)<1E-6)
        disp('yes');
    end
    
    TN = 0;
    TP = 0;
    FN = 0;
    FP = 0;

    for i=1:numel(vals)
        calc_class = 1;
        
        if (mean_symp>mean_asymp)
            if (vals(i)-cutoff>1E-6)
                calc_class = 2;
            end
        else
            if (vals(i)-cutoff<1E-6)
                calc_class = 2;
            end
        end
        
        if calc_class==class(i)
            if calc_class==1
                TN = TN+1;
            end
            if calc_class==2
                TP = TP+1;
            end
        end
        if calc_class~=class(i)
            if calc_class==1
                FN = FN+1;
            end
            if calc_class==2
                FP = FP+1;
            end
        end
    end

    specificity = [specificity TN/N];
    sensitivity = [sensitivity TP/P];
    
    accuracy = [accuracy (TP+TN)/(P+N)];

    PPV = [PPV TP/(TP+FP)];
    NPV = [NPV TN/(TN+FN)];
    FDR = [FDR FP/(FP+TP)];
end

% subplot(2,2,3);
% predictive_power = [];
% for i=1:numel(cutoff_values)
%     pt = [1-specificity(i),sensitivity(i),0];
%     v1 = [0,0,0];
%     v2 = [1,1,0];
%     this_dist = point_to_line(pt,v1,v2);
%     pt = [0 1 0];
%     max_dist = point_to_line(pt,v1,v2);
%     predictive_power = [predictive_power this_dist/max_dist*100];
% end
% plot(cutoff_values,predictive_power,'-*','MarkerSize',10);                         
% axis tight;
% xlabel(['cut-off value (' units ')']);ylabel('predictive power (%)');
% ind = find(predictive_power==max(predictive_power));
% ind = ind(1);
% title(['Predictive power (' num2str(max(predictive_power),'%.0f') '% at ' num2str(cutoff_values(ind),formatSpec) ' ' units ')']);

subplot(2,2,3);
ROC_dist = [];
for i=1:numel(cutoff_values)
    pt = [1-specificity(i),sensitivity(i),0];
    pt = pt*100;
    v1 = [0,1,0];
    v1 = v1*100;
    this_dist = norm(v1-pt);
    ROC_dist = [ROC_dist this_dist];
end
plot(cutoff_values,ROC_dist,'-*','MarkerSize',6);                         
axis tight;
xlabel(['cut-off value (' units ')']);ylabel('ROC dist (%)');
ind = find(ROC_dist==min(ROC_dist));
ind = ind(1);
title(['ROC dist (' num2str(ROC_dist(ind),'%.1f') '% at ' num2str(cutoff_values(ind),formatSpec) ' ' units ')']);

subplot(2,2,4);
plot(cutoff_values,accuracy*100,'-*b','MarkerSize',6);
axis tight;
xlabel(['cut-off value (' units ')']);ylabel('accuracy (%)');
% ind = find(accuracy==max(accuracy));
% ind = ind(1);
title(['Diagnostic accuracy (' num2str(accuracy(ind)*100,'%.0f') '% at ' num2str(cutoff_values(ind),formatSpec) ' ' units ')']);

subplot(2,2,2);
plot(0:1:100,0:1:100,':r');
hold on;
plot((1-specificity)*100,sensitivity*100,'-.*b','DisplayName','','MarkerSize',10);
xlabel('false positive rate (%)');ylabel('sensitivity (%)');

polyarea_X = 1-specificity;
polyarea_Y = sensitivity;
polyarea_X = [polyarea_X 1];
polyarea_Y = [polyarea_Y 0];
AUC = polyarea(polyarea_X,polyarea_Y);

% x = (1-specificity)*100;
% y = sensitivity*100;
% labels = cutoff_values;
% ind = find(x<60 & y>40);
% text(x(ind),y(ind),cellstr(num2str(labels(ind)')),'VerticalAlignment','bottom', ...
%                              'HorizontalAlignment','right','FontSize',9);

title({'Receiver Operating Characteristic (ROC)';['(sensitivity=' num2str(sensitivity(ind)*100,'%.0f') ...
    '%,specificity=' num2str(specificity(ind)*100,'%.0f') ...
    '% at ' num2str(cutoff_values(ind),formatSpec) ' ' units ')']});

axis ([0 100.1 0 100.1]);

% subplot(2,3,5);
% plot(cutoff_values,sensitivity*100,'-*b','MarkerSize',10);
% axis tight;
% xlabel('cut-off value');ylabel('sensitivity (%)');title('Sensitivity');
% 
% subplot(2,3,6);
% plot(cutoff_values,specificity*100,'-*b','MarkerSize',10);
% axis tight;
% xlabel('cut-off value');ylabel('specificity (%)');title('Specificity');

% subplot(2,3,4);
% plot(cutoff_values,ppv,'-*b');xlabel('cutoff value');ylabel('PPV');title('Positive Predictive Value (PPV)');
% 
% subplot(2,3,5);
% plot(cutoff_values,npv,'-*b');xlabel('cutoff value');ylabel('NPV');title('Negative Predictive Value (NPV)');
% 
% subplot(2,3,6);
% plot(cutoff_values,fdr,'-*b');xlabel('cutoff value');ylabel('FDR');title('False Discovery Rate (FDR)');
end

