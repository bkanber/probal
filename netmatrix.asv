 /* =============================================================
 * netmatrix.c - used by vesselmo.m
 *
 * Computational function
 * Builds neural network data matrix
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

/*
Usage format is [netdata,selx,sely] = netmatrix(la,tblock_size,limx,limy,numpoints,trunc)
*/

void netmatrix(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *la = mxGetPr(prhs[0]);

  long tblock_size = (long)(*mxGetPr(prhs[1]));
  
  long limx = (long)(*mxGetPr(prhs[2]));
  long limy = (long)(*mxGetPr(prhs[3]));

  long numpoints = (long)(*mxGetPr(prhs[4]));

  int trunc = (int)(*mxGetPr(prhs[5]));

  long larm_len = ceil(((double)tblock_size-1)/2);
  long rarm_len = floor(((double)tblock_size-1)/2);

  long tblock_size2 = tblock_size*tblock_size;
  
  long elemcnt = tblock_size*2-1;
  
  double *datamatrix;
  double *selx;
  double *sely;
  
  int i;
  int dim = numpoints;
  
  int count = 0;
  
  if (trunc) elemcnt = ceil((double)elemcnt/2);
  
  plhs[0] = mxCreateDoubleMatrix(elemcnt,dim,mxREAL);
  datamatrix = mxGetPr(plhs[0]);

  plhs[1] = mxCreateDoubleMatrix(1,dim,mxREAL);
  selx = mxGetPr(plhs[1]);
  
  plhs[2] = mxCreateDoubleMatrix(1,dim,mxREAL);
  sely = mxGetPr(plhs[2]);
  
  for (i = 0; i < dim; i++)
  {
    double A = ((double)rand()/RAND_MAX) * limx;
    double B = ((double)rand()/RAND_MAX) * limy;
    
    long this_x = Max(1, A);
    long this_y = Max(1, B);
    
    long xl = this_x-larm_len;
    long yu = this_y-larm_len;
    long xr = this_x+rarm_len;
    long yd = this_y+rarm_len;
    
    long x, y, j, k;
    
    double thisGS;

    if (this_x<=0 || this_y<=0)
    printf("err (%ld,%ld)\n",this_x,this_y);
    
    j = 0;
    k = -1;

    for (x = xl; x <= xr; x++)
    {
        for (y = yu; y <= yd; y++)
        {
            if (x!=this_x && y!=this_y)
                continue;
                
            k++;
                /*
            if (trunc && (k%2)!=0)
                continue;
        */
            if (trunc && x!=this_x) 
                continue;
        
            if (x<1 || x>limx || y<1 || y>limy)
                thisGS = 0;
            else
                thisGS = la[y-1+(x-1)*limy];

            datamatrix[j+count*elemcnt] = (thisGS-127.5)/127.5;
                
            j++;
        
        } /* for (y) */
        
    } /* for (x) */
    
    selx[count] = this_x;
    sely[count++] = this_y;
    
    lbl:
    i = i;

  } /* for (i) */
  
  mxSetN(plhs[0], count);  
  mxSetN(plhs[1], count);  
  mxSetN(plhs[2], count);  
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=6 || nlhs!=3)
  {
    mexErrMsgTxt("Usage format is [netdata,selx,sely] = netmatrix(la,tblock_size,limx,limy,numpoints,trunc)");
    return;
  }

  netmatrix(nlhs, plhs, nrhs, prhs);

}
