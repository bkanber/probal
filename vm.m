%
% vm.m - Copyright 2016, Baris Kanber, Centre for Medical Image Computing,
% University College London
%

function varargout = vm(varargin)
global handles
global block_size
global g_threshold
global preload_cineloops median_filter maximize_contrast create_movie
global PiB180
global main_fig
global PThreshold
global pfilter
global prefilter
global driveLetters

% vm Application M-file for vm.fig
%    FIG = vm launch vm GUI.
%    vm('callback_name', ...) invoke the named callback.

% Last Modified by GUIDE v2.5 13-Jul-2015 20:57:05

if nargin == 0  % LAUNCH GUI
    % Change default axes fonts. 
    set(0,'DefaultAxesFontName', 'Segoe UI Light') 
    set(0,'DefaultAxesFontSize', 8) 

    % Change default text fonts. 
    set(0,'DefaultTextFontname', 'Segoe UI Light') 
    set(0,'DefaultTextFontSize', 8) 

    PiB180 = pi/180;
    
    block_size = 3; % i.e. a mxm block (m>2 required, odd m preferable)
    preload_cineloops = 0;
    maximize_contrast = 0;
    median_filter = 1;
    create_movie = 1;
    PThreshold = 0.10; % probability matrix contour threshold
    pfilter = 0;
    prefilter = 5;
    g_threshold = 0.01;
    
	main_fig = openfig(mfilename,'reuse');

	% Generate a structure of handles to pass to callbacks, and store it. 
	handles = guihandles(main_fig);
	guidata(main_fig, handles);
    
    % Initialise the Slider Text
    set(handles.ThresholdSlider,'Value',g_threshold);
    set(handles.RangeSlider,'Value',(block_size-1)/2);
    set(handles.PThresholdslider,'Value',PThreshold);
    set(handles.pfilterslider,'Value',pfilter);
    set(handles.prefilterslider,'Value',prefilter);

    ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);
    RangeSlider_Callback(handles.RangeSlider, [], handles);
    PThresholdslider_Callback(handles.PThresholdslider, [], handles);
    pfilterslider_Callback(handles.pfilterslider, [], handles);
    prefilterslider_Callback(handles.prefilterslider, [], handles);
    
    driveLetters = getdrives('-nofloppy');
    
    for i=1:numel(driveLetters)
        if strendswith(driveLetters{i},filesep)
            driveLetters{i}=strrep(driveLetters{i},filesep,'');
        end
        driveLetters{i}=upper(driveLetters{i});
    end
    
    loadCurrentDirectoryContents(handles);
    
	if nargout > 0
		varargout{1} = fig;
	end

elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK

	try
		[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
    catch err
        [cdata,map] = imread('trees.tif'); 
        msgbox(err.message,'Error','custom',cdata,map);
	end

end

% --------------------------------------------------
function [ynewdata,gdata] = Dpolyfit(xdata,ydata,n)

p = polyfit(xdata,ydata,n);

ynewdata = 0;

i = 1;

for c=n:-1:0
	ynewdata = ynewdata + p(i)*xdata.^c;
	i = i + 1;
end

gdata = 0;

i = 1;

for c=n:-1:1
	gdata = gdata + p(i)*c*xdata.^(c-1);
    i = i + 1;
end



% --------------------------------------------------------------------
% Function intline - returns the intersection point of the lines
% ax + by + c = 0 and px + qy + r = 0
% The return values are empty vectors if the lines do not
% intersect or [Inf] if they intersect at infinite number of points
function [int_x,int_y] = intline(a,b,c,p,q,r)

if (not(b))
    if (not(q))
        if (c/a==r/p)
            int_x = [Inf];
            int_y = [Inf];
            return;
        end
        
        int_x = [];
        int_y = [];
        return;
    end
    
    int_x = -c/a;
    int_y = (-r-p*int_x)/q;
    return;
end

if (not(q))
    int_x = -r/p;
    int_y = (-c-a*int_x)/b;
    return;
end

det = (a*q-p*b);

if (not(det))
    % parallel lines (or even maybe overlapping)
    det2 = (r*b-c*q);
    
    if (not(det2))
        int_x = [Inf];
        int_y = [Inf];
        return;
    end    
    
    int_x = [];
    int_y = [];
    return;
end    

int_x = (r*b-c*q)/det;

int_y = (-c-a*int_x)/b;


% --------------------------------------------------
% add B degrees (+ve or -ve) to angle A and maintain in range 0 to 360
function C = degrees_add(A,B)

C = A + B;

if (abs(C)>=360)
    C = C-360*floor(C/360);
end

if (C<0) 
    C = 360-abs(C);
end

% --------------------------------------------------
function theta = Datan2(Y,X)
global PiB180

theta = atan2(Y,X)/PiB180;

if (theta<0) 
    theta = (360+theta);
end


% --------------------------------------------------
% return the smaller of the two differences between two angles
% both angles need to be in the range 0 to 360
function ddiff = degrees_diff(A,B)

C = abs(A-B);

if (C<=180) 
    ddiff = C;
else
    ddiff = 360-C;
end




% --------------------------------------------------
function mmsClearROIData()
% Clear all region of interest data (eg. When loading a new file, etc..)
global a matrix_dimensions;
global pos_interest dim_interest;

matrix_dimensions = [];
a = [];

pos_interest = [];
dim_interest = [];

clear global vesselx;
clear global vessely;
clear global vesselp;


% --------------------------------------------------
function [pos_picture,dim_picture] = mmsPictureChoosePos(ldimx,ldimy)

% ratio1 = ldimy/ldimx;
% ratio2 = ldimx/ldimy;
% 
% if (ratio1>1.5)
% 	pos_picture = [50 50];
% 	dim_picture = [ldimx*ratio1*0.6 ldimy*1.0];
%     return;
% end
% 
% if (ratio2>1.5)
% 	pos_picture = [50 50];
% 	dim_picture = [ldimx*1.0 ldimy*ratio2*0.6];
%     return;
% end
% 
lcldimx = ldimx+80;
lcldimy = ldimy+80;

pos_picture = [50 50];
dim_picture = [lcldimx lcldimy];

if (lcldimx>640) | (lcldimy>640)
    dim_picture = dim_picture./1.8;
end

if (lcldimx<150) | (lcldimy<150)
    dim_picture = dim_picture.*1.8;
end



function selectFile(handles,frm)
global fullname
global whole_a whole_matrix_dimensions % The whole matrix
global a matrix_dimensions % Only the region of interest
global xifinfo % info structure for .xif files
global cmlinfo % info structure for .cml files
global fileinfo % info structure for DICOM files
global preload_cineloops % preload cineloops?

if frm==1
    close all;
    mmsClearROIData;

    clear global xifinfo % info structure for .xif files
    global xifinfo
    clear global cmlinfo % info structure for .cml files
    global cmlinfo
    clear global fileinfo % info structure for DICOM files
    global fileinfo
end

[pathstr,name,ext] = fileparts(fullname);

str = sprintf('Loading file %s',fullname);
if frm>1
    str = strcat(str,' (f',num2str(frm),')');
end
set(handles.infoedit,'String',str);
drawnow;

if (strcmp(ext,'.avi')||strcmp(ext,'.mpg')||strcmp(ext,'.wmv'))
    whole_a = readavi(fullname,frm);

    whole_matrix_dimensions = size(whole_a)
else
if (strcmp(ext,'.xif'))
    [xifinfo,whole_a] = readxif(fullname,frm,[]);

    if (not(xifinfo.result))
        whole_a = [];
        disp(xifinfo.errmsg);
        disp(xifinfo.syserrmsg);
        str = sprintf('Unable to open the file %s',fullname);
        set(handles.infoedit,'String',str);
        drawnow;
        return;
    end

    whole_matrix_dimensions = size(whole_a)

    if (preload_cineloops)
        showxif(fullname);
    end

else
    if (strcmp(ext,'.mtf'))
        fid = fopen(fullname, 'rt');
        [str cnt] = fscanf(fid, '%s\n', 1);
        whole_a = imread(str);
        whole_matrix_dimensions = size(whole_a)
        fclose(fid);
    else
    if (strcmp(ext,'.cml'))
        [cmlinfo, frame_data] = readcml(handles,fullname,frm);
        if ~(cmlinfo.FileOpened)
            str = sprintf('Unable to open the file %s',fullname);
            set(handles.infoedit,'String',str);
            drawnow;
            return;
        end
        whole_matrix_dimensions = [cmlinfo.Ni, cmlinfo.Nj];
        whole_a = frame_data;
    else
    if (strcmp(ext,'.dcm'))
        frame_data = readdicom(fullname,frm);
        whole_matrix_dimensions = size(frame_data);
        whole_a = frame_data;
    else
        whole_a = imread(fullname);

        if (length(size(whole_a))==3 && size(whole_a,3)==3) % RGB format?
            whole_a = rgb2hsv(whole_a);
            whole_a = whole_a(:,:,3)*255;
        else
            if (length(size(whole_a))==4 && size(whole_a,3)==1) % GIF from preclinical?
                whole_a = whole_a(:,:,:,1);
            end
        end
        whole_matrix_dimensions = size(whole_a);
    end
    end
    end
end
end

a = whole_a;
matrix_dimensions = whole_matrix_dimensions;

if (exist('xifinfo','var') && isstruct(xifinfo))
    str1 = sprintf('The file %s has been opened',fullname);
    str2 = sprintf('Institution = %s',xifinfo.institution);
    str3 = sprintf('Patient = %s %s',xifinfo.lastname,xifinfo.firstname);
    str4 = sprintf('Date = %s %s',xifinfo.date,xifinfo.time);
    str5 = sprintf('Study contains %d frames at %dfps',xifinfo.num_frames,xifinfo.frameRate);
    %str6 = sprintf('Machine format = %s',upper(xifinfo.machineformat));
    str7 = sprintf('Dimensions = %d x %d pixels',xifinfo.width,xifinfo.height);
    str8 = sprintf('           = %.2f x %.2f mm',xifinfo.echoScan_linearWidth,xifinfo.echoDepth_displayStopDepth-xifinfo.echoDepth_displayStartDepth);

    set(handles.infoedit,'String',{str1,'',str2,str3,str4,'',str5,'',str7,str8});
    drawnow;
else
    str = {sprintf('The file %s has been opened',fullname),''};
    if exist('fileinfo','var') && isstruct(fileinfo)
        if isfield(fileinfo,'PatientID')
            str = [str,{sprintf('PatientID: %s',fileinfo.PatientID)}];
        end
        if isfield(fileinfo,'NumberOfFrames')
            str = [str,{sprintf('NumberOfFrames: %d',fileinfo.NumberOfFrames)}];
        end
        if isfield(fileinfo,'FrameRate')
            str = [str,{sprintf('FrameTime: %f ms',1/fileinfo.FrameRate*1E3)}];
            str = [str,{sprintf('FrameRate: %f',fileinfo.FrameRate)}];
        end
        if isfield(fileinfo,'ppmmx')
            str = [str,{sprintf('ppmmx: %f',fileinfo.ppmmx)}];
        end
        if isfield(fileinfo,'ppmmy')
            str = [str,{sprintf('ppmmy: %f',fileinfo.ppmmy)}];
        end
    end
    set(handles.infoedit,'String',str);
    drawnow;
end

set(handles.slider11,'Enable','inactive');

if exist('fileinfo','var') && isstruct(fileinfo)
    if isfield(fileinfo,'NumberOfFrames')
        set(handles.slider11,'Max',fileinfo.NumberOfFrames);
        set(handles.slider11,'Min',1);
        set(handles.slider11,'Value',frm);
        smallstep = 1/(fileinfo.NumberOfFrames-1);
        largestep = 10/(fileinfo.NumberOfFrames-1);
        set(handles.slider11,'SliderStep',[smallstep largestep]);
        set(handles.slider11,'Enable','on');
        set(handles.text40,'String',strcat('Animator (',num2str(frm),'/',num2str(fileinfo.NumberOfFrames),')'));
    end
end

% Display the image in its original form (assumed to be Grayscale)
VD_DisplayOriginal;

% --------------------------------------------------------------------
function varargout = VD_DisplayOriginal()
% Displays the image in its original form (assumed to be Grayscale)
global nm_file nm_path fullname Ufname
global whole_a whole_matrix_dimensions % The whole matrix
global a matrix_dimensions % Only the region of interest
%global xifinfo % info structure for .xif files
global pos_interest dim_interest

h = findobj('UserData',101);

if (isempty(h))
	[pos_picture,dim_picture] = mmsPictureChoosePos(whole_matrix_dimensions(2),whole_matrix_dimensions(1));
	
	lcl_positionvector = Picture_Display_Fit(pos_picture,dim_picture,0);
	
	str = sprintf('The File %s', Ufname);
	h = figure('Position',lcl_positionvector,'Name',str,'NumberTitle','off','UserData',101);
else
    set(0, 'CurrentFigure', h);
    clf;
end

lcl_grayscalevector = [0,255]; % The Min & Max values of an 8-bit grayscale image
imagesc(whole_a, lcl_grayscalevector); axis image; colormap(gray);

set(gca,'FontAngle','italic','TickLength',[0 0]);

DisplayROI;
DisplayXifInfo; 


% % --------------------------------------------------------------------
% function varargout = VD_DisplayOriginalROI(cmap)
% % Displays the image (only the Region of Interest) in its original form (assumed to be Grayscale)
% global nm_file nm_path fullname Ufname
% global whole_a whole_matrix_dimensions % The whole matrix
% global a matrix_dimensions % Only the region of interest
% global vesselx vessely vesselp % Polygon describing the vessel on ROI
% 
% lcl_grayscalevector = [0 255]; % The Min & Max values of an 8-bit grayscale image
% 
% [pos_picture,dim_picture] = mmsPictureChoosePos(matrix_dimensions(2),matrix_dimensions(1));
% 
% lcl_positionvector = Picture_Display_Fit(pos_picture,dim_picture,0);
% 
% nm_title = sprintf('Region of Interest on %s', Ufname);
% 
% figure('UserData', 80, 'NumberTitle', 'off', 'Position', lcl_positionvector, 'Name', nm_title);
% imagesc(a, lcl_grayscalevector); colormap(cmap); pixval on;
% set(gca, 'FontAngle', 'italic');
% 
% lcl_titleobject = text('String',nm_title,'FontName','Lucida Console');
% set(gca,'Title',lcl_titleobject);
% 
% if (length(vesselx) & length(vessely))
%     hold on, plot(vesselx,vessely,':r','LineWidth',1);
% end
% 
% if (length(vesselp))
%     hold on, plot(vesselp(1),vesselp(2),':rv','LineWidth',1);
% end


% --------------------------------------------------------------------
function varargout = Exit_Callback()
% Called when the application is exiting
close all
clear all





% --------------------------------------------------------------------
function varargout = menu_DrawInterestRegion_Callback(h, eventdata, handles, varargin, channel_int)
% Stub for Callback of the selection of the menu items Draw a Region of Interest (??)
global nm_file nm_path fullname Ufname
global whole_a whole_matrix_dimensions % The whole matrix
global a matrix_dimensions % Only the region of interest
global pos_interest dim_interest

TAG_MIN_INTERESTREGION_DIMENSION = 30;

if whole_matrix_dimensions

    str1 = sprintf('Selecting a Region of Interest:\n\n');
    str2 = sprintf('Choose your region of interest by drawing a rectangular area over the original ultrasound image\n\n');
    
    set(handles.infoedit,'String',[str1 str2]);
    
    % close all previous windows as we are selecting a new Region of Interest
    % close all;
    
    % initialise data
	clear global vesselx;
	clear global vessely;
	clear global vesselp;
    
    pos_interest = [];
    dim_interest = [];

    % display the original so that a Region of Interest can be selected
    VD_DisplayOriginal;

    % get the new Region of Interest
    if channel_int==1
        k = waitforbuttonpress;
        pt_first = double(int16(get(gca,'CurrentPoint')));
        rbbox;
        pt_other = double(int16(get(gca,'CurrentPoint')));
        
        % convert the points to their 2D representations
        pt_first = pt_first(1,1:2);
        pt_other = pt_other(1,1:2);
    else
        dimensions_drag_l = [120,80];
        dimensions_drag_l = dimensions_drag_l - [1,1];

        k = waitforbuttonpress;
        
        % the trouble here is that we need to convert between logical and physical units
        set(gca, 'Units', 'pixels');
        axes_position_p = get(gca, 'Position');

        picture_dimensions_p = double(int16(axes_position_p(3:4)));
        picture_dimensions_l = [whole_matrix_dimensions(2) whole_matrix_dimensions(1)];
        
        dimensions_drag_p = dimensions_drag_l .* picture_dimensions_p ./ picture_dimensions_l;
                
        pt_drag_start = get(gcf,'CurrentPoint');
        
        rect_drag = [pt_drag_start(1,1) pt_drag_start(1,2) dimensions_drag_p];
        rect_drag = dragrect(rect_drag);
        
        pt_first = double(int16(get(gca,'CurrentPoint')));
        pt_first = pt_first(1,1:2);
        
        pt_other = pt_first + [dimensions_drag_l(1),-dimensions_drag_l(2)];
    end

    pos_interest = min(pt_first,pt_other);
    dim_interest = abs(pt_first-pt_other);

    % eliminate selections outside the range
    unity_vector_a = [1 1];
    limit_vector_a = [whole_matrix_dimensions(2) whole_matrix_dimensions(1)];
    
    pos_interest = max(pos_interest, unity_vector_a);
    pos_interest = min(pos_interest, limit_vector_a);
    
    range_vector_a = limit_vector_a - pos_interest;
    dim_interest = min(dim_interest, range_vector_a);
    dim_interest = max(dim_interest, unity_vector_a);

    if (min(dim_interest(1),dim_interest(2)) < TAG_MIN_INTERESTREGION_DIMENSION)
        str1 = sprintf('The selected region of interest is too small.\n\n');
        str2 = sprintf('The smallest dimension of the region of interest is required to be at least %.0f units.\n', TAG_MIN_INTERESTREGION_DIMENSION);
        
        pos_interest = [];
        dim_interest = [];
        
        set(handles.infoedit,'String',[str1 str2]);
        return
    end
    
    % create the region of interest matrix
    a = whole_a(pos_interest(2):pos_interest(2)+dim_interest(2), pos_interest(1):pos_interest(1)+dim_interest(1));
    matrix_dimensions = size(a)
 
    % VD_DisplayOriginalROI(gray);
    VD_DisplayOriginal;
    
else
    str1 = sprintf('Please choose an ultrasound image file first by selecting the option Choose File from the Ultrasound menu\n\n');
    str2 = sprintf('A variety of file formats are supported however note that the image needs to be 8-bits Grayscale\n');

    set(handles.infoedit,'String',[str1 str2]);
end





% --------------------------------------------------------------------
function varargout = menu_DrawInterestRegion1_Callback(h, eventdata, handles, varargin)
% Stub for Callback of the selection of the menu item Draw a Region of Interest (Custom)
menu_DrawInterestRegion_Callback(h, eventdata, handles, varargin, 1);




% --------------------------------------------------------------------
function Coords_picture = Picture_Display_Fit(pos_picture,lcl_dimensions,L)
% Caller is trying to display a picture at position pos_picture with dimensions lcl_dimensions
% Caller calls this function to ensure that the picture fits on the display
% The function scales down the picture until it does fit
% returns the result in a 1x4 vector in form (x,y,width,height)
lcl_dimensions = abs(lcl_dimensions);

% In the event that one of the dimensions is 0 scaling can not be done
if (min(lcl_dimensions(1),lcl_dimensions(2)) < 1)
    lcl_dimensions = [100 100];
end

% Grow until it is a reasonable dimension
while (L) & (min(lcl_dimensions(1),lcl_dimensions(2)) < 150)
    lcl_dimensions = (lcl_dimensions * 1.02);
end

% Shrink until it fits on the screen
set(0, 'Units', 'pixels');

mms_dimensions = get(0, 'ScreenSize');
mms_dimensions = mms_dimensions(3:4);
    
limit_dimensions = (mms_dimensions - pos_picture) - [50 100];
    
while (lcl_dimensions(1)>limit_dimensions(1)) | (lcl_dimensions(2)>limit_dimensions(2))
    lcl_dimensions = (lcl_dimensions * 0.98);
end

Coords_picture = [pos_picture lcl_dimensions];




% --------------------------------------------------------------------
function mmsSetupAxes(Hf,lcl_axeslimits_H,lcl_axeslimits_V,lcl_Title)

Ha = get(Hf,'CurrentAxes');

if (Ha)
	set(Ha,'xlim',lcl_axeslimits_H);
	set(Ha,'ylim',lcl_axeslimits_V);
	
	lcl_titleobject = text('String',lcl_Title,'FontName','Lucida Console');
	
	set(Ha,'Title',lcl_titleobject);
	% set(Ha,'FontAngle','italic');
end





% --------------------------------------------------------------------
function outputStr = mmsOutputPct(lcl_Pvariable)

TAG_CLOSENESS_FACTOR = 1;
TAG_COMPLETENESS_VALUE = 100;

if (abs(lcl_Pvariable-TAG_COMPLETENESS_VALUE) < TAG_CLOSENESS_FACTOR)
    outputStr = sprintf('Completing...\n');
else
    outputStr = sprintf('%.0f%% Completed...\n', lcl_Pvariable);
end




% --------------------------------------------------------------------
function h = TrainFunctionDisplay(handles,id)
global tdata fullname
global pos_interest dim_interest

h = findobj('UserData',id);

if (isempty(h))
	[pos_picture,dim_picture] = mmsPictureChoosePos(tdata.ldimx,tdata.ldimy);

    lcl_positionvector = Picture_Display_Fit(pos_picture,dim_picture,0);
	
	h = figure('UserData',id,'NumberTitle','off','Name',fullname,'Position',lcl_positionvector);
else
    set(0, 'CurrentFigure', h);
    clf;
end
    
%imagesc(tdata.la, [0 255]), colormap(gray), axis image;

viz_from = str2num(get(handles.edit7,'String'));
viz_to = str2num(get(handles.edit8,'String'));

imagesc(tdata.la, [viz_from viz_to]), colormap(gray), axis image;

set(gca, 'FontAngle', 'italic', 'TickLength', [0 0]);

DisplayROI;
DisplayXifInfo;


% --------------------------------------------------------------------
function DisplayROI
global pos_interest dim_interest
return
if (not(isempty(pos_interest)))

    polyvec = [...
    pos_interest(1)-1,pos_interest(2)-1; ...
    pos_interest(1)+dim_interest(1),pos_interest(2)-1; ...
    pos_interest(1)+dim_interest(1),pos_interest(2)+dim_interest(2); ...
    pos_interest(1)-1,pos_interest(2)+dim_interest(2); ...
    pos_interest(1)-1,pos_interest(2)-1; ...
    ];

    hold on;
    plot(polyvec(:,1),polyvec(:,2),':ys','MarkerSize',4);
    
    text(pos_interest(1),pos_interest(2)-4,...
        'ROI','FontName','Lucida Console','FontSize',9,'VerticalAlignment','bottom','Color','y');
    
end



% --------------------------------------------------------------------
function DisplayXifInfo()
%global xifinfo % info structure for .xif files
global whole_matrix_dimensions

if (exist('xifinfo','var') & isstruct(xifinfo))
    sy = whole_matrix_dimensions(1)/xifinfo.heightmm;
    sx = whole_matrix_dimensions(2)/xifinfo.widthmm;
    
	d = sy*5;
	
	for y=1+d:d:whole_matrix_dimensions(1)
        hold on;
        plot([whole_matrix_dimensions(2)-4,whole_matrix_dimensions(2)+1],[y y],'-w');
	end

%   d = sx*5;
% 	
% 	for x=1+d:d:whole_matrix_dimensions(2)
%         hold on;
%         plot([x x],[whole_matrix_dimensions(1)-4,whole_matrix_dimensions(1)+1],'-w');
% 	end
    
    trian = [...
    whole_matrix_dimensions(2),whole_matrix_dimensions(1)/2-8;...
    whole_matrix_dimensions(2),whole_matrix_dimensions(1)/2+8;...
    whole_matrix_dimensions(2)-8,whole_matrix_dimensions(1)/2;...
    whole_matrix_dimensions(2),whole_matrix_dimensions(1)/2-8;
    ];
    
    patch(trian(:,1),trian(:,2),'g');    
	
	text(whole_matrix_dimensions(2)-4,4,'5mm','Color','w','FontAngle','normal','HorizontalAlignment','right','VerticalAlignment','top','FontSize',8);
	
	% text(4,4,{xifinfo.institution,sprintf('%s %s',xifinfo.date,xifinfo.time),' ',sprintf('%s %s',xifinfo.lastname,xifinfo.firstname),xifinfo.patient_id},'Color','w','FontAngle','italic','FontSize',8,'HorizontalAlignment','left','VerticalAlignment','top');
	
	drawnow;
end


% --------------------------------------------------------------------
function myButtonDownFun(obj,eventdata)
global main_fig tdata

curpoint = get(gca,'CurrentPoint'); % Returns 3d values

x = double(int16(curpoint(1,1)));
y = double(int16(curpoint(1,2)));

if (x<1 | x>tdata.ldimx | y<1 | y>tdata.ldimy)
    return;
end

alldata = get(obj,'UserData');

userdata = alldata{1};
linedata = alldata{2};

if ~isa(userdata{5},'matlab.graphics.chart.primitive.Line') || (userdata{6}==2) % begin direction selection
    axs = findobj(get(obj,'Children'),'type','axes');
    delete(findobj(get(axs,'Children'),'UserData',117));
    
    hold on;
    h0 = plot(x,y,'-sb','EraseMode','xor','MarkerSize',6,'MarkerEdgeColor','r','UserData',117);
    
    userdata = {...
            x ...  % x1
            0 ...  % x2
            y ...  % y1
            0 ...  % y2
            h0 ... % handle of normal line
            0 ...  % set when the selection is complete
            max(min(tdata.ldimx,tdata.ldimy)/20,15) ... % arm length
            0 ...  % handle of green box
            0 ...  % handle of midline section
            0 ...  % handle of text object
        };
    
    set(obj,'UserData',{userdata,linedata});
else
    userdata{6} = userdata{6}+1;
    set(obj,'UserData',{userdata,linedata});
    
    if (userdata{6}==2)
        handles = guidata(main_fig);
        set(handles.infoedit,'String','');
        drawnow;
        MeasurementFun(handles,userdata,linedata);
    end
end


% --------------------------------------------------------------------
function myButtonMotionFunDefault(obj,eventdata)
curpoint = get(gca,'CurrentPoint'); % Returns 3d values

x = round(curpoint(1,1));
y = round(curpoint(1,2));


% --------------------------------------------------------------------
function myButtonMotionFun4ArtSel(obj,eventdata)
global PiB180 tdata

curpoint = get(gca,'CurrentPoint'); % Returns 3d values

x = round(curpoint(1,1));
y = round(curpoint(1,2));

delete(findobj(get(gca,'Children'),'UserData',117));

if not(x<1 | x>tdata.ldimx | y<1 | y>tdata.ldimy)
    text(x,y,{'Click on/inside','the OOI'},'Color','y','FontName','Segoe UI Light','FontSize',9.5,'EraseMode','xor','UserData',117);
end



% --------------------------------------------------------------------
function myButtonMotionFun(obj,eventdata)
global PiB180 tdata

alldata = get(obj,'UserData');

userdata = alldata{1};
linedata = alldata{2};

curpoint = get(gca,'CurrentPoint'); % Returns 3d values

x = round(curpoint(1,1));
y = round(curpoint(1,2));

if (x<1 | x>tdata.ldimx | y<1 | y>tdata.ldimy)
    if (userdata{10})
        delete(userdata{10});
        userdata{10} = 0;
        set(obj,'UserData',{userdata,linedata});
    end
    return;
end

if (userdata{5}==0)
    if (userdata{10}==1)
        set(userdata{10},'Position',[x y 0]);
    else
        delete(findobj(get(gca,'Children'),'UserData',117));
        
        userdata{10} = text(x,y,{'Click to','draw a normal'},'Color','y','FontName','Segoe UI Light','FontSize',9.5,'EraseMode','xor','UserData',117);
        set(obj,'UserData',{userdata,linedata});
    end
    return;
end

if ~isa(userdata{5},'matlab.graphics.chart.primitive.Line') || userdata{6}==2 % selection already completed
    return;
end

if (userdata{6})
    delx = x-userdata{2};
    dely = y-userdata{4};
    dist = sqrt(delx^2+dely^2);
    userdata{7} = dist;
else
	userdata{2} = x;
	userdata{4} = y;
    xdata=[userdata{1} userdata{2}];
    ydata=[userdata{3} userdata{4}];
    set(userdata{5},'XData',xdata,'YData',ydata);
end

x1 = userdata{1};
x2 = userdata{2};
y1 = userdata{3};
y2 = userdata{4};

armlen = userdata{7};

delx = x2 - x1;
dely = y2 - y1;

dist = sqrt(delx^2+dely^2);

theta = Datan2(-dely,delx);

alpha = degrees_add(theta,90);

aivec = cos(theta*PiB180);
ajvec = -sin(theta*PiB180);
nivec = cos(alpha*PiB180);
njvec = -sin(alpha*PiB180);

pdata = [...
x1-nivec*armlen,y1-njvec*armlen; ...
x1+nivec*armlen,y1+njvec*armlen; ...
x2+nivec*armlen,y2+njvec*armlen; ...
x2-nivec*armlen,y2-njvec*armlen; ...
x1-nivec*armlen,y1-njvec*armlen; ...
];

if isa(userdata{8},'matlab.graphics.chart.primitive.Line') 
    set(userdata{8},'XData',pdata(:,1),'YData',pdata(:,2));
else
    userdata{8} = plot(pdata(:,1),pdata(:,2),'-m','EraseMode','xor','UserData',117);
end

midx = (x1 + x2) / 2;
midy = (y1 + y2) / 2;

pdata = [...
midx-nivec*armlen,midy-njvec*armlen; ...
];

if isa(userdata{9},'matlab.graphics.chart.primitive.Line')
    set(userdata{9},'XData',pdata(:,1),'YData',pdata(:,2));
else
    userdata{9} = plot(pdata(:,1),pdata(:,2),'sr','EraseMode','xor','UserData',117);
end

set(obj,'UserData',{userdata,linedata});

%--------------------
function handleFigureWindowThresholdChange(hObj,event)
global g_threshold 
g_threshold = get(hObj,'Value')

% --------------------------------------------------------------------
function linedata = TrainFunction(handles,action,datafile,frm,num_frames)
global fullname in_training;
global net net0 block_size tdata;
global whole_a a
global g_threshold
global PiB180
global maximize_contrast
global fhd
global prefilter
global fileinfo

linedata = [];

%figure;imhist(uint8(tdata.la),256);

if (prefilter>0)
    disp('histeq');
    tic
    %tdata.la = double(histeq(uint8(tdata.la),256));
    toc
end

%figure;imhist(uint8(tdata.la),256);

if (maximize_contrast)
    max_pix = max(max(tdata.la));
    min_pix = min(min(tdata.la));
    pix_range = max_pix - min_pix;
    
    maxGS = 255;
    
    tdata.la = tdata.la - min_pix;
    tdata.la = tdata.la .* (maxGS/pix_range);
end

fhd = TrainFunctionDisplay(handles,777);

if (prefilter>0)
    disp('filtering');
    tic
    
    if exist('fileinfo','var') && isstruct(fileinfo) && ...
       isfield(fileinfo,'ppmmx') && isfield(fileinfo,'ppmmy')
   
        scale = sqrt(fileinfo.ppmmx*fileinfo.ppmmy)/sqrt(11.77*11.77);
        
        actualprefiltersize = double(prefilter)*scale;
    else
        actualprefiltersize = double(prefilter);
    end
    
    hdisk = fspecial('disk',actualprefiltersize);
   
    text(50, 80, ['prefilt=' num2str(prefilter) '; actual=' num2str(actualprefiltersize)], 'Color', 'magenta','FontName','Segoe UI Light','FontSize',8);

    tdata.la = imfilter(tdata.la,hdisk);
    
    toc
else
    %%text(50, 80, ['prefilt= ' num2str(prefilter)], 'Color', 'magenta','FontName','Segoe UI Light','FontSize',8);
end

if (length(tdata.lvesselp)<2)
    str1 = 'Click on/inside the object of interest (OOI) and spacebar to move on to the next stage';
    set(fhd,'Pointer','arrow');
    set(handles.infoedit,'String',str1);
    drawnow;
    
    set(fhd,'WindowButtonMotionFcn',@myButtonMotionFun4ArtSel);
    tdata.lvesselp = [];
    
    uicontrol('Style', 'slider',...
        'Min',0.01,'Max',0.24,'Value',g_threshold,...
        'Position', [0 0 120 20],...
        'Callback',@handleFigureWindowThresholdChange);   

    uicontrol('Style','text',...
        'Position',[0 25 120 20],...
        'String','Threshold')

%     h=nan; % handle to ContourGroup object
    for i=1:40
        k = waitforbuttonpress;
        if (not(k==0))
            break;
        end
        if (get(handles.checkbox12,'Value')==0)
            curpoint = get(gca,'CurrentPoint'); % Returns 3d values
            x = double(int16(curpoint(1,1)));
            y = double(int16(curpoint(1,2)));
            if (x>size(whole_a,2) || y>size(whole_a,1))
                continue;
            end
            if (1==1)
                hold on, plot(x,y,'-.r*','MarkerSize',8,'MarkerFaceColor','g','MarkerEdgeColor','r');
                drawnow;
            end            
        else
            moveptr(handle(gca),'init');
            moveptr(handle(gca),'move',395,350-188);  % so easy
            R=java.awt.Robot;
            R.setAutoWaitForIdle(1);
            R.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);pause(0.1);R.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
            % presses 1st mouse-Button for 0.1 secs (at current position).

            curpoint = get(gca,'CurrentPoint'); % Returns 3d values
            x = double(int16(curpoint(1,1)));
            y = double(int16(curpoint(1,2)));
            %x = double(571);
            %y = double(185);
        end
        if (isempty(tdata.lvesselp))
            tdata.lvesselp = [x y];
            tdata.thresholds = g_threshold;
        else
            qq = [x y];
            tdata.lvesselp = [tdata.lvesselp;qq];
            tdata.thresholds = [tdata.thresholds;g_threshold];
        end
%         for hi=1:length(h)
%             if (~isnan(h(hi))) 
%                 delete(h(hi)); % delete previous ContourGroup object
%             end
%         end
        h = findobj(gcf, 'type', 'Contour');
        for hi=1:length(h)
            delete(h(hi));
        end
        [linedata,h2]=TrainFunction_do(handles,frm);
%         h=h2;
    end
    
    set(fhd,'Pointer','arrow');

    delete(findobj(get(gca,'Children'),'UserData',117));
    set(fhd,'WindowButtonMotionFcn',@myButtonMotionFunDefault);

    set(handles.infoedit,'String','');
    drawnow;
else
    [linedata,h2]=TrainFunction_do(handles,frm);
end

% --------------------------------------------------------------------
function [linedata,h] = TrainFunction_do(handles,frm)
global fullname in_training;
global net net0 block_size tdata;
global whole_a a
global g_threshold
global PiB180
global maximize_contrast
global PThreshold
global fhd
global pfilter

%%%%%%%%%%%%%%
%%%tdata.lvesselp = [132 127];
%%%tdata.thresholds = g_threshold;
%%%%%%%%%%%%%%

disp('making blank matrix');
tic
whole_block = zeros(tdata.ldimy,tdata.ldimx);
toc

for i=1:size(tdata.lvesselp,1)
    x = tdata.lvesselp(i,1);
    y = tdata.lvesselp(i,2);
    
    limdist = max(max(tdata.dataregion(3)-x,x-tdata.dataregion(1)),max(tdata.dataregion(4)-y,y-tdata.dataregion(2)));
    
    whole_block_this = zeros(tdata.ldimy,tdata.ldimx);
    whole_block_this(y,x) = 1;   % We know this

    disp('scanpath');
    tic
    [trainpath npoints] = scanpath(x,y,tdata.ldimx,tdata.ldimy,limdist,0,tdata.dataregion);
    toc

    trainpath = (trainpath(1:npoints,:));

    pathsize = size(trainpath);
    
    g_lcl_threshold = tdata.thresholds(i);

    if (~isfloat(tdata.la))
        disp('tdata.la not float, converting to floating point');
        tdata.la = double(tdata.la);
    end
    
    disp('calling empmatrix');

    tic
    
    Qmethod = get(handles.popupmenu3, 'Value');
    
    switch Qmethod
        case 1
            empmatrix2011(x, y, tdata.la, trainpath, tdata.ldimx, tdata.ldimy, block_size, whole_block_this, g_lcl_threshold, pfilter);
        case 2
            empmatrix_ht2011(x, y, tdata.la, trainpath, tdata.ldimx, tdata.ldimy, block_size, whole_block_this, g_lcl_threshold, pfilter);
        case 3
            [~, whole_block_this] = regionGrowing(tdata.la, [y,x], g_lcl_threshold*255, Inf, true, true, true);
    end
   
    toc

    whole_block = max(whole_block,whole_block_this);
end

if (0)
    h = findobj(0,'UserData',401);
    if (h)
        close(h);
    end

    figure('UserData', 401, 'NumberTitle', 'off', 'Name', 'Probability Map');
    imagesc(whole_block,[0 1]);axis image;colormap(gray);

     if (tdata.nvertices>0)
         hold on, plot(tdata.lvesselp(1),tdata.lvesselp(2),':yv','LineWidth',1);
     end

    set(gca, 'FontAngle', 'italic');
    IMPIXELINFO;
    drawnow;
end

if (0)
else
    set(handles.infoedit,'String','Finding probability boundaries...');
    drawnow;

    set(0, 'CurrentFigure', fhd);
    hold on;
    disp('doing contour');
    tic
    [C,h] = contour('v6', whole_block, [PThreshold PThreshold], ':y');
    toc
    flag = get(handles.checkbox11,'Value');
    if (flag==1 && frm>1)
        h.LineStyle=none;
    end
    h.LineWidth=2;
    
    numcolsC=size(C,2);
    col=1
    num_contours=0
    linedata={}
    while col<=numcolsC
        level=C(1,col);
        numpoints=C(2,col);
        if numpoints>50
            linedata{num_contours+1,1}=C(1,col+1:col+1+numpoints-1);
            linedata{num_contours+1,2}=C(2,col+1:col+1+numpoints-1);
            num_contours=num_contours+1
        end
        col=col+numpoints+1
    end
    %linedata = cell(numclines,2);
    
    set(handles.infoedit,'String','Completed.');
    drawnow;

end

function MeasurementFun(handles,userdata,linedata)
global PiB180
global xifinfo % info structure for .xif files
global cmlinfo % info structure for .cml files
global fileinfo % info structure for DICOM files
global main_fig fullname nm_file
global median_filter
global whole_matrix_dimensions
global nm_file nm_path fullname Ufname
global g_threshold
global prefilter

x1 = userdata{1};
x2 = userdata{2};
y1 = userdata{3};
y2 = userdata{4};
armlen = ceil(userdata{7});

%%%%%%%%%
% % % x1=140;
% % % y1=50;
% % % x2=140;
% % % y2=200;
% % % armlen=12;
%%%%%%%%%

delx = x2 - x1;
dely = y2 - y1;

theta = degrees_add(Datan2(-dely,delx),90);

nivec = cos(theta*PiB180);
njvec = -sin(theta*PiB180);

xx = x1+armlen*nivec;
yy = y1+armlen*njvec;
hold on;
plot(xx,yy,'.');

if (exist('cmlinfo','var') & isstruct(cmlinfo))
    ppmmy = cmlinfo.ppmmy;
    ppmmx = cmlinfo.ppmmx;
    resolution_known = 1;
else
    if (exist('xifinfo','var') & isstruct(xifinfo))
        ppmmy = whole_matrix_dimensions(1)/xifinfo.heightmm;
        ppmmx = whole_matrix_dimensions(2)/xifinfo.widthmm;
        resolution_known = 1;
    else
        if (exist('fileinfo','var') && isfield(fileinfo,'ppmmx'))
            ppmmy = fileinfo.ppmmy;
            ppmmx = fileinfo.ppmmx;
            resolution_known = 1;
        else
            ppmmy = 1;
            ppmmx = 1;
            resolution_known = 0;
            disp('resolution not known - cannot convert from pixels to mm');
        end
    end
end

if (size(linedata,2)==1)
    multi = 1; % multi-frame
    numframes = length(linedata);
else
    multi = 0;
    numframes = 1;
end

dist = zeros(1,numframes);
errs = zeros(1,numframes);
full_dists = [];

for epoch=1:numframes
    if (multi)
        this_frame = linedata{epoch};
    else
        this_frame = linedata;
    end
    
    disp(sprintf('%d/%d',epoch,numframes));
    drawnow;
    
    [dims,errors] = meandim4(this_frame,[x1 x2 y1 y2 armlen],nivec,njvec,ppmmx,ppmmy);
    
%     if min(dims)<1
%         msgbox('min(dims)<1','modal');
%     end
    
    if (isempty(full_dists))
        full_dists = dims;
    else
        full_dists = [full_dists;dims];
    end
    
    dist(epoch) = mean(dims);
    errs(epoch) = errors;
%     numlines = size(this_frame,1);
%     
%     for this_pos = -armlen : +armlen
%         
%         x1 = or_x1 + this_pos * nivec;
%         y1 = or_y1 + this_pos * njvec;
%         x2 = or_x2 + this_pos * nivec;
%         y2 = or_y2 + this_pos * njvec;
%         
%         dely = y2 - y1;
% 		delx = x2 - x1;
% 		
% 		a = -dely;
% 		b = delx;
% 		c = dely*x1-delx*y1;
% 		
% 		% str = sprintf('line (%f)x + (%f)y + (%f) = 0',a,b,c);
% 		% disp(str);
% 
% 		satx = [];
% 		saty = [];
%         
%         for i=1:numlines % each line
%             xdata = this_frame{i,1};
%             ydata = this_frame{i,2};
%             L = length(xdata);
%             
%             for j=1:L-1
%                 dely = ydata(j+1)-ydata(j);
%                 delx = xdata(j+1)-xdata(j);
% 	
%                 midy = (ydata(j+1)+ydata(j))/2;
%                 midx = (xdata(j+1)+xdata(j))/2;
%                 
% 				p = -dely;
% 				q = delx;
% 				r = dely*xdata(j)-delx*ydata(j);
%                 
%                 side_len = sqrt(dely^2+delx^2)/2;
% 	
%                 [int_x,int_y] = intline(a,b,c,p,q,r);
%                 
%                 if (not(isempty(int_x)) & isfinite(int_x))
%                     delx = int_x-midx;
%                     dely = int_y-midy;
%                     
%                     len = sqrt(delx^2+dely^2);
%                     
%                     if (len-side_len<=1e-4)
%                         if (isempty(find(abs(satx-int_x)<1e-4 & abs(saty-int_y)<1e-4)))
%                             satx = [satx int_x];
%                             saty = [saty int_y];
%                         end
%                     end
%                 end
%                 
%             end % each point
%                
%         end % each line
%         
%         satn = length(satx);
%         
%         if (epoch==numframes & this_pos==0)
%             for k=1:satn
%                 hold on;
%                 plot(satx(k),saty(k),'*m','MarkerSize',5,'UserData',117,'EraseMode','xor');
%                 % text(satx(k),saty(k),sprintf('%d',k),'Color','y');
%                 drawnow;
%             end
%         end
%         
%         if (satn>2)
%             disp(sprintf('too many (%d)',satn));
%         end
% 	
%         if (satn<2)
%             disp(sprintf('frame %d line %d has too few (%d)',epoch,i,satn));
%         end
% 	
% 	%     h=figure;imagesc(blank,[0 255]);colormap(gray);
% 	
%         if (satn==2)
%             
%             dely = saty(2) - saty(1);
%             delx = satx(2) - satx(1);
%             
%             lcldist(this_pos+armlen+1) = sqrt((dely/ppmmy)^2+(delx/ppmmx)^2);
%             
%         else
% %             if (not(epoch==1))
% %                 dist(:,epoch) = dist(:,epoch-1);
%             lcldist(this_pos+armlen+1) = 0;
%         end
%         
%     end % for this_pos
%     
%     dist(epoch) = mean(lcldist);
        
end % each frame

% dist = dist(find(not(dist==0)));

% distmm = sqrt((dist(1,:)./ppmmy).^2+(dist(2,:)./ppmmx).^2);
distmm = dist;
    
if (size(dist,2)<2)
    distmm
else
    if (median_filter)
        distmm = medfilt1(distmm);
    end
    
    if (exist('xifinfo','var') & isstruct(xifinfo))
        frameRate = xifinfo.frameRate;
    
        tD = 1/frameRate;

        t = (0:numframes-1) * tD;
    else
        if (exist('cmlinfo','var') & isstruct(cmlinfo))
            frameRate = cmlinfo.PRF;

            tD = 1/frameRate;

            t = (0:numframes-1) * tD;
        else
            if (not(isempty(strfind(nm_file, '.mtf'))))
                frameRate = 10;

                tD = 1/frameRate;

                t = (0:numframes-1) * tD;
            else
                if (exist('fileinfo','var') && isfield(fileinfo,'FrameRate'))
                    frameRate = fileinfo.FrameRate;

                    tD = 1/frameRate;

                    t = (0:numframes-1) * tD;
                else
                    disp('frame rate not known - cannot convert from frame no to time');
                    frameRate = NaN;

                    tD = NaN;

                    t = (1:numframes);
                end
            end
        end
    end
    
    maxmm = max(distmm);
    minmm = min(distmm);
    meanmm = mean(distmm);
    devmm = std(distmm);
    ranmm = maxmm-minmm;

    h = findobj('type','Figure','UserData',117);
    
    if (isempty(h))
        h = figure('Name',sprintf('VLDT for %s',fullname),'NumberTitle','off','UserData',117,'Position',[50 50 780 400]);
    else
        figure(h);
        clf;
    end
    
    for pos=1:size(full_dists, 2)
        hold on;
        if (~isempty(find(full_dists(:,pos)<3.0)))
        	% continue;
        end
        
        %plot(t,full_dists(:,pos),'k');
    end
    % plot(t,medfilt1(mean(full_dists,2)),'g','LineWidth',0.5);
    %plot(t,std(full_dists,0,2),'g','LineWidth',1);
    plot(t,mean(full_dists,2),'g','LineWidth',1);
    %plot(t,median(full_dists,2),'b','LineWidth',1);
    %legend('mean','median');
    
    %plot(t,median(full_dists,2),'r','LineWidth',4);
    %plot(t,medfilt1(mean(full_dists,2)),'b','LineWidth',4);
    %plot(t,medfilt1(median(full_dists,2)),'c','LineWidth',4);
    
%     plot(t,distmm,'-b');
% 
%     for i=1:length(errs)
%         if (errs(i))
%             hold on;
%             plot(t(i),distmm(i),'bo','MarkerSize',6);
%             plot(t(i),distmm(i),'r*','MarkerSize',4);
%         end
%     end
    
%     hold on;
%     plot(t,errs,'r');
    
    %set(gca, 'FontAngle', 'italic');
    set(gca, 'FontName', 'Segoe UI Light');
    set(gca, 'FontSize', 8);
    
    axis tight;
    
    if (not(isnan(frameRate)))
        xlabel('time (s)');
        xlim = [0,(numframes-1)*tD];
    else
        xlabel('frame number');
        xlim = [1,numframes];
    end
    
    if (resolution_known)
        units = 'mm';
        ylabel('mean lumen diameter (mm)');
    else
        units = 'pixel';
        ylabel('mean lumen diameter (pixel)');
    end
    
    title(sprintf('Variation of mean lumen diameter with time (Mean of means is %.2f %s)',meanmm,units));
    
    ylim = [minmm-ranmm/50,maxmm+ranmm/50];

    %set(gca,'XLim',xlim);
    %set(gca,'YLim',ylim);
    axis tight;
    
    saveas(gcf,strcat(fullname,'.wm.th',num2str(g_threshold(1)*100),'.prefilt',...
        num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

%     hold on;
%     plot([xlim(1),xlim(2)],[meanmm,meanmm],'-.');
    
%     text(xlim(1)+(xlim(2)-xlim(1))/50,meanmm+ranmm/50,sprintf('Mean Value = %.2f mm',meanmm),'FontName','Lucida Console','FontSize',8,'VerticalAlignment','bottom','FontAngle','italic');

    str0 = sprintf('Variation of lumen diameter with time - VLDT');
    strA = sprintf('Results for %s', fullname);
    str1 = sprintf('Mean: %.4f %s', meanmm, units);
    str2 = sprintf('Max: %.4f %s', maxmm, units);
    str3 = sprintf('Min: %.4f %s', minmm, units);
    str4 = sprintf('Max-Min: %.4f %s', maxmm-minmm, units);
    str5 = sprintf('Standard Deviation: %.4f %s', devmm, units);
    str6 = sprintf('Coefficient of variation: %.4f%%', devmm/meanmm*100);
    
    set(handles.infoedit,'String',{str0,'',strA,'',str1,str2,str3,str4,'',str5,str6});
    drawnow;
    
    %t = 0:0.01:5;
    %distmm = sin(t/0.7*2*pi)
    %plot(t,distmm)
    %figure;
    if (exist('xifinfo','var') & isstruct(xifinfo))
        fs=xifinfo.frameRate;
    else
        if (exist('cmlinfo','var') & isstruct(cmlinfo))
            fs = cmlinfo.PRF;
        else
            if (not(isempty(strfind(nm_file, '.mtf'))))
                fs = 10;
            else
                if (exist('fileinfo','var') && isfield(fileinfo,'FrameRate'))
                    fs = fileinfo.FrameRate;
                else
                    disp('frame rate not known - cannot continue');
                    return;
                end
            end
        end
    end
    
%     fs=20
%     tau = 1/fs;
%     t = 0:tau:5
%     signalFrequency = 1.5
%     signalPeriod = 1/signalFrequency
%     distmm = sin(t/signalPeriod*2*pi)/2;
%     figure;
%     plot(t,distmm);
   
    X = distmm-mean(distmm);
    N = length(X);
    Y = abs(fft(X))/(N/2);
    f = fs/2*linspace(0,1,N/2);
    power_spectrum = Y.^2;
    total_power = sum(power_spectrum);
    figure;
    plot(f,power_spectrum(1:N/2)*2); 
    set(gca, 'FontName', 'Segoe UI Light');
    set(gca, 'FontSize', 8);
    title('power spectrum(1:N/2)*2');
    xlabel('frequency (Hz)');
    ylabel('Y.^2(1:N/2)*2');
    
    power_ratio = (power_spectrum(1:N/2)*2)/total_power;
    [max_power_ratio,max_power_ratio_index] = max(power_ratio);
    f0 = f(max_power_ratio_index);
    harmonics = f/f0;
    IND = find(harmonics>=1.50);
    harmonic_high_freq_power_ratio = sum(power_ratio(IND));
    IND2 = find(harmonics<=0.50);
    harmonic_low_freq_power_ratio = sum(power_ratio(IND2));
    set(handles.infoedit,'String', {
        strcat('f0=',num2str(f0),' (',num2str(f0*60),' bpm'), 
        strcat('max_power_ratio=',num2str(max_power_ratio)),
        strcat('total power_ratio=',num2str(sum(power_ratio))),
        strcat('harmonic high freq power_ratio=',num2str(harmonic_high_freq_power_ratio))
        strcat('harmonic low freq power_ratio=',num2str(harmonic_low_freq_power_ratio))
        strcat('harmonic central power_ratio=',num2str(1-harmonic_high_freq_power_ratio-harmonic_low_freq_power_ratio))
    });
    drawnow;
    figure;
    harmonic_show = power_ratio*0;
    harmonic_show(IND) = max_power_ratio;
    harmonic_show2 = power_ratio*0;
    harmonic_show2(IND2) = max_power_ratio;
    plot(f/f0,power_ratio,f/f0,harmonic_show,f/f0,harmonic_show2);
    title('power_ratio');
    
    f_samp_modulated_signal = 44E3;
    t_samp_modulated_signal = 1/f_samp_modulated_signal;
    f_carrier = 100; % 30E3
    t_samp = (0:t_samp_modulated_signal:max(t));
    audio_data = interp1(t,distmm,t_samp,'cubic');
    a = (audio_data-min(audio_data))/(max(audio_data)-min(audio_data));
    %fDoppler = (audio_data-min(audio_data));
    %fDoppler = exp((audio_data-min(audio_data))*30);
    %fDoppler = (fDoppler-min(fDoppler))/(max(fDoppler)-min(fDoppler))*300;
    fDoppler = (audio_data-min(audio_data))/(mean(audio_data)+2*std(audio_data)-min(audio_data))*250;
    clear vz;
    phase_corr = zeros(1,length(fDoppler));
    for i=2:length(phase_corr)
        phase_corr(i) = phase_corr(i-1)+2*pi*(fDoppler(i-1)+f_carrier)*t_samp(i-1)-2*pi*(fDoppler(i)+f_carrier)*t_samp(i-1);
    end
    audio_data = a.*sin(2*pi*(f_carrier+fDoppler).*t_samp+phase_corr);
    clear fDoppler;
    clear a;
    audio_data = (audio_data-min(audio_data))/(max(audio_data)-min(audio_data))*2-1;
   
    %audio_data = [audio_data ones(1,length(audio_data)/5*0.25)*(-1)];
    %audio_data = repmat(audio_data,1,10);
    
    if false
        wavwrite(audio_data,f_samp_modulated_signal,8,'vm.wav'); % [audio_data' audio_data']
        winopen('vm.wav');
    end
     
	X = audio_data;
    N = length(X);
    Y = abs(fft(X))/(N/2);
    f = f_samp_modulated_signal/2*linspace(0,1,N/2);
    power_spectrum = Y.^2;
    figure;
    plot(f,power_spectrum(1:N/2)*2); title('power_spectrum(1:N/2)*2 audio_data');

    %figure;
    %plot(audio_data_left_chan(1:length(audio_data_left_chan)));title('audio_data left ch');
    %wavwrite([audio_data_left_chan' audio_data_right_chan'],f_samp_modulated_signal,8,'vm.wav'); % [audio_data' audio_data']
    %figure;
    %plot(f,fy/max(fy))
    
%    figure(main_fig);
%     text(xlim(1)+(xlim(2)-xlim(1))*0.04,...
%          ylim(2)-(ylim(2)-ylim(1))*0.06,...
%          {str2,str3,str4,str5,' ',str6,str7},...
%          'BackgroundColor',[.7 .9 .7],...
%          'FontName','Lucida Console','VerticalAlignment','top');
end

% avinm = 'pixmov.avi';
% 
% if (exist(avinm))
%     delete(avinm);
% end
% 
% movie2avi(M,avinm,'compression','None','quality',100);
% winopen(avinm);


function MeasurementFunSetup(handles,linedata)
fhd = findobj('UserData',777);

if (isempty(fhd))
    return;
end

set(0,'CurrentFigure',fhd);

set(fhd,'DoubleBuffer','on');
set(fhd,'Renderer','painters');

set(fhd,'Pointer','arrow');

str1 = 'Please select a normal for distance measurement';
str2 = ' Please select a normal for distance measurement ';

% title(str2,'FontName','Lucida Console','BackgroundColor','y','FontAngle','normal');

set(handles.infoedit,'String',str1);
drawnow;

auxdata = {...
            0 ...  % x1
            0 ...  % x2
            0 ...  % y1
            0 ...  % y2
            0 ...  % handle of normal line
            0 ...  % set when the selection is complete
            0 ...  % arm length
            0 ...  % handle of green box
            0 ...  % handle of midline section
            0 ...  % handle of text object
        };
        
set(fhd,'UserData',{auxdata,linedata});

set(fhd,'WindowButtonMotionFcn',@myButtonMotionFun);
set(fhd,'WindowButtonDownFcn',@myButtonDownFun);


%
% Read a file which is in the tdf format
% 
function tdata = ReadTdfFile(filename)
fid = fopen(filename, 'rb');
if (fid==-1) return;
end

lvesselp = fread(fid,2,'integer*2');

nvertices0 = fread(fid,1,'integer*2');

lvesselx0 = zeros(nvertices0,1);
lvessely0 = zeros(nvertices0,1);

for cx=1:nvertices0
    lvesselx0(cx) = fread(fid,1,'integer*2');
    lvessely0(cx) = fread(fid,1,'integer*2');
end

nvertices = fread(fid,1,'integer*2');

lvesselx = zeros(nvertices,1);
lvessely = zeros(nvertices,1);

for cx=1:nvertices
    lvesselx(cx) = fread(fid,1,'integer*2');
    lvessely(cx) = fread(fid,1,'integer*2');
end

ldimy = fread(fid,1,'integer*2');
ldimx = fread(fid,1,'integer*2');

la = fread(fid,'integer*2');

fclose(fid);

tdata.lvesselp = lvesselp;
tdata.nvertices0 = nvertices0;
tdata.lvesselx0 = lvesselx0;
tdata.lvessely0 = lvessely0;
tdata.nvertices = nvertices;
tdata.lvesselx = lvesselx;
tdata.lvessely = lvessely;
tdata.ldimx = ldimx;
tdata.ldimy = ldimy;
tdata.la = reshape(la,ldimy,ldimx);
tdata.dataregion = [];

    

% --------------------------------------------------------------------
function varargout = TrainorSimulate(h, eventdata, handles, varargin, action)
global in_training;
global tdata;

in_training = 1;
num_files = 1;
turn = 0;

while ((in_training==1) & (num_files))

    turn = turn + 1
    
    if (strcmp(action,'train'))
        set(handles.infoedit,'String','Scanning Directory...');
        drawnow;

        files = dir(sprintf('Training Data\\*.td%d',varargin{1}));
    	num_files = length(files);
        
        if (num_files)
            this_file = ceil(max(rand*num_files,1));
            thisfile = files(this_file).name;
            close all;
        end
    else
        [nm_file,nm_path] = uigetfile('Training Data\\*.td?','Please choose the data file you would like to simulate');
        if nm_file % is present
            num_files = 1;
            thisfile = strcat(nm_path, nm_file);
            close all;
        else
            num_files = 0;
        end
       
    end
    
	if num_files
        % str = get(handles.infoedit,'String');
        if (strcmp(action,'train'))
            train_mode = 1;
            set(handles.infoedit,'String',sprintf('Training %s',thisfile));
            drawnow;
            tdata = ReadTdfFile(sprintf('Training Data\\%s',thisfile));
        else
            train_mode = 0;
            set(handles.infoedit,'String',sprintf('Simulating %s',thisfile));
            drawnow;
            
            tdata = ReadTdfFile(thisfile);
        end
    
        linedata = TrainFunction(handles,action,thisfile,[],[]);

        MeasurementFunSetup(handles,linedata);
        
	end

    if (strcmp(action,'simulate'))
        in_training = 0;
    end
    
end


% --------------------------------------------------------------------
function track_GO(handles)
global nm_file nm_path fullname Ufname
global a matrix_dimensions
global pos_interest dim_interest
global tdata in_training
global xifinfo % info structure for .xif files
global cmlinfo % info structure for .cml files
global fileinfo % info structure for DICOM files
global create_movie
global g_threshold
global prefilter

if (not(length(matrix_dimensions)==2))
    set(handles.infoedit,'String','Please select an ultrasound image or cineloop file first');
    drawnow;
    return;
end

[pathstr,name,ext] = fileparts(fullname);

if (isempty(pos_interest) || isempty(dim_interest))
    pos_interest = [1 1];
    dim_interest = fliplr(matrix_dimensions);
end

tdata.lvesselp = -1;
tdata.nvertices0 = -1;
tdata.nvertices = -1;
tdata.dataregion = [pos_interest pos_interest+dim_interest-1];

if (not(strcmp(ext,'.mtf')) && not(strcmp(ext,'.xif')) && not(strcmp(ext,'.cml')) && 1==0)
    % for any single frame data format (e.g. tif)
	tdata.la = double(a);
	tdata.ldimx = matrix_dimensions(2);
	tdata.ldimy = matrix_dimensions(1);

    close all;
    in_training = 1;
	linedata = TrainFunction(handles,'simulate',[],[],[]);
    MeasurementFunSetup(handles,linedata);
    in_training = 0;
    return;
end

close all;

if (strcmp(ext,'.mtf'))
	fid = fopen(fullname, 'rt');
	[str cnt] = fscanf(fid, '%s\n', 1);
else
if (strcmp(ext,'.cml'))
    str = sprintf('Extracting frame 1 from .data file');
    set(handles.infoedit,'String',str);
    drawnow;
        
    [cmlinfo,tdata.la] = readcml(handles,fullname,1);
    cnt = 1;
else
if (strcmp(ext,'.xif'))
    str = sprintf('Extracting frame 1 from .xif file');
    set(handles.infoedit,'String',str);
    drawnow;
        
    [xifinfo,tdata.la] = readxif(fullname,1,xifinfo);
    cnt = xifinfo.result;
    
    if (not(xifinfo.result))
        disp(xifinfo.errmsg);
        disp(xifinfo.syserrmsg);
    end        
else
if (strcmp(ext,'.avi')||strcmp(ext,'.mpg')||strcmp(ext,'.wmv'))
    str = sprintf('Extracting frame 1 from .avi/mpg/wmv file');
    set(handles.infoedit,'String',str);
    drawnow;
        
    tdata.la = readavi(fullname,1);
    cnt = 1;
else
    if (strcmp(ext,'.dcm'))
        str = sprintf('Extracting frame 1 from .dcm file');
        set(handles.infoedit,'String',str);
        drawnow;

        tdata.la = readdicom(fullname,1);
        cnt = 1;
    else
        whole_a = double(imread(fullname));

        if (length(size(whole_a))==3 && size(whole_a,3)==3) % RGB format?
            whole_a = rgb2hsv(whole_a);
            whole_a = whole_a(:,:,3)*255;
        else
            if (length(size(whole_a))==4 && size(whole_a,3)==1) % GIF from preclinical?
                whole_a = whole_a(:,:,:,1);
            end
        end
        
        if 0
            whole_a = whole_a(100:600,200:800);
            whole_a = imresize(whole_a,2,'bilinear');
        end
        
        if 0
            whole_a = max(1,whole_a-60);
        end
        
        whole_a = whole_a-min(min(whole_a));
        whole_a = min(255,double(whole_a)/max(max(whole_a))*255);
        if 1 
            %whole_a = min(255,double(whole_a)/150*255);
            %whole_a = uin8(whole_a);
        end
        clear fileinfo;
        fileinfo.NumberOfFrames = 1;
        fileinfo.FrameRate = 1;
        tdata.la = whole_a;
        cnt = 1;
    end
end
end
end
end

frm = 1;

in_training = 1;

while ((cnt==1) & (in_training==1))
    %userview = memory;
    %fprintf('%f MB contg mem free\n',double(userview.MaxPossibleArrayBytes)/1024/1024);
    if (strcmp(ext,'.mtf'))
        prev_dir = pwd;
        cd(pathstr);
        tdata.la = double(imread(str));
        cd(prev_dir);
    else
        if (strcmp(ext,'.cml'))
            disp(sprintf('%d/%d',frm,cmlinfo.Nt));
            if (frm==256)
                disp('frm 256');
            end
        else
         if (strcmp(ext,'.xif'))
            disp(sprintf('%d/%d',frm,xifinfo.num_frames));
        else
         if (strcmp(ext,'.avi')||strcmp(ext,'.mpg')||strcmp(ext,'.wmv'))
            disp(sprintf('%d/%d',frm,fileinfo.NumberOfFrames));
        else
            disp(sprintf('%d/%d',frm,fileinfo.NumberOfFrames));
            if (frm==256)
                disp('frm 256');
            end
         end
        end
        end
    end
    if (frm==12)
        disp('yes');
    end
    lcl_matrix_dimensions = size(tdata.la);

	tdata.ldimx = lcl_matrix_dimensions(2);
	tdata.ldimy = lcl_matrix_dimensions(1);
    
    if (frm>1) 
        whole_a_pf=whole_a;
    end
    whole_a = tdata.la; % Before it gets noise filtered by TrainFunction
    
    if (exist('cmlinfo','var') & isstruct(cmlinfo))
        ppmmy = cmlinfo.ppmmy;
        ppmmx = cmlinfo.ppmmx;
        resolution_known = 1;
    else
        if (exist('xifinfo','var') & isstruct(xifinfo))
            ppmmy = lcl_matrix_dimensions(1)/xifinfo.heightmm;
            ppmmx = lcl_matrix_dimensions(2)/xifinfo.widthmm;
            resolution_known = 1;
        else
            if (exist('fileinfo','var') && isfield(fileinfo,'ppmmx'))
                ppmmy = fileinfo.ppmmy;
                ppmmx = fileinfo.ppmmx;
                resolution_known = 1;
            else
                ppmmy = 1;
                ppmmx = 1;
                resolution_known = 0;
                disp('resolution not known - cannot convert from pixels to mm');
            end
        end
    end

%fhd = TrainFunctionDisplay(777);

    if (strcmp(ext,'.mtf'))
    	linedata{frm,1} = TrainFunction(handles,'simulate',[],1,[]);
    else
        if (strcmp(ext,'.cml'))
            str = sprintf('Processing frame %d of %d',frm,cmlinfo.Nt);
            set(handles.infoedit,'String',str);
            drawnow;
            linedata{frm,1} = TrainFunction(handles,'simulate',[],frm,cmlinfo.Nt);
        else
        if (strcmp(ext,'.xif'))
            str = sprintf('Processing frame %d of %d',frm,xifinfo.num_frames);
            set(handles.infoedit,'String',str);
            drawnow;
            comb_ld = [];
            %for th=0.0025:0.0025:0.10
            for th=g_threshold:g_threshold
                brk=0;
                if (frm~=1) 
                    th = g_threshold; 
                    brk=1; 
                end
                g_threshold = th;
                tdata_la_prev = tdata.la;
                %tdata.lvesselp = pos_st_interest_in_frame((frm-1)*n_templates+temp_n,:);
                %tdata.lvesselp = pos_st_interest(1,:);
                ld = TrainFunction(handles,'simulate',[],frm,xifinfo.num_frames);
                tdata.la = tdata_la_prev;
                if (isempty(comb_ld)) 
                    comb_ld = ld;
                else
                    disp('A');
                    hold on;
                    plot(comb_ld{1},comb_ld{2},'Color','blue');
                    S(1).P(1).x = comb_ld{1};
                    S(1).P(1).y = comb_ld{2};
                    S(1).P(1).hole = 0;
                    S(2).P(1).x = ld{1};
                    S(2).P(1).y = ld{2};
                    S(2).P(1).hole = 0;
                    Display_result = 0;
                    Accuracy = 1e-6;
                    [G] = Polygons_intersection(S,Display_result,Accuracy);
                    comb_ld{1} = G(1,3).P.x;
                    comb_ld{2} = G(1,3).P.y;
                    hold on;
                    plot(comb_ld{1},comb_ld{2},'Color','yellow');
                    disp('B');
                end
                linedata{frm,1} = comb_ld;
                if (brk==1 || 1==1)
                    break;
                end
            end
        else
            str = sprintf('Processing frame %d of %d',frm,fileinfo.NumberOfFrames);
            set(handles.infoedit,'String',str);
            drawnow;
            linedata{frm,1} = TrainFunction(handles,'simulate',[],frm,fileinfo.NumberOfFrames);
        end
        end
    end
    
    PERFORM_NORMALISATION = true;
    
    if (PERFORM_NORMALISATION)
        bss = 2; % box side size
        SELECT_REFERENCE_POINTS_IN_ALL_FRAMES = false;
        
        if (SELECT_REFERENCE_POINTS_IN_ALL_FRAMES || frm==1)
            str1 = sprintf('Select a point on the adventitia\n\n');
            set(handles.infoedit,'String',[str1]);
            k = waitforbuttonpress;
                        
            adv_pos = double(int16(get(gca,'CurrentPoint')));

            adv_x = adv_pos(1,1);
            adv_y = adv_pos(1,2);

            rectangle('Position',[adv_x-bss adv_y-bss bss*2+1 bss*2+1],'EdgeColor','r');
            
            gs_adventitia = medianN(whole_a(adv_y-bss:adv_y+bss,adv_x-bss:adv_x+bss));
                        
            bld_x = tdata.lvesselp(1,1);
            bld_y = tdata.lvesselp(1,2);
            
            rectangle('Position',[bld_x-bss bld_y-bss bss*2+1 bss*2+1],'EdgeColor','r');

            gs_blood = medianN(whole_a(bld_y-bss:bld_y+bss,bld_x-bss:bld_x+bss));
        end

        whole_a_normalized = max(0,whole_a-gs_blood);
        whole_a_normalized = min(255,whole_a_normalized/(gs_adventitia-gs_blood)*190);
        
        gs_adv_check1 = medianN(whole_a(adv_y-bss:adv_y+bss,adv_x-bss:adv_x+bss));
        gs_bld_check1 = medianN(whole_a(bld_y-bss:bld_y+bss,bld_x-bss:bld_x+bss));

        gs_adv_check2 = medianN(whole_a_normalized(adv_y-bss:adv_y+bss,adv_x-bss:adv_x+bss));
        gs_bld_check2 = medianN(whole_a_normalized(bld_y-bss:bld_y+bss,bld_x-bss:bld_x+bss));

        if (false)
            % load mask_f1; 
            mask = roipoly;

            plq_data = whole_a(mask);
            size(plq_data)
            median(plq_data)
            std(plq_data)
            std(plq_data)/mean(plq_data)

            plq_data = whole_a_normalized(mask);
            size(plq_data)
            median(plq_data)
            std(plq_data)
            std(plq_data)/mean(plq_data)

            h=gcf;
            figure;imshow(mask);
            figure(h);
        end
        
        %figure;imagesc(whole_a_normalized);colormap(gray);axis image;
    else
        clear whole_a_normalized;
    end
    
if (1==0)    
    if (strcmp(ext,'.cml'))
        EN = 1;
        EM = 1;
        if (frm>EN)
             cfimage = zeros(size(IQ,1),size(IQ,2));
             for x=1:size(IQ,2)
                 for y=1:EM:size(IQ,1)
                     if (y+EM>size(IQ,1)) break; end;
                     sum_NUM = 0;
                     sum_DENOM = 0;
                     for m=y:y+EM-1
                         for n=frm-EN:frm-1
                             %sum_NUM = sum_NUM + angle(IQ(m,x,n+1))-angle(IQ(m,x,n));
                             sum_NUM = sum_NUM+Q(m,x,n+1).*I(m,x,n)-Q(m,x,n).*I(m,x,n+1);
                             sum_DENOM = sum_DENOM+I(m,x,n+1).*I(m,x,n)+Q(m,x,n+1).*Q(m,x,n);
                         end
                     end
                     vz = -atan2(sum_DENOM,sum_NUM)*1540*cmlinfo.PRF/(4*pi*cmlinfo.f0*1E6);
                     cfimage(y:y+EM-1,x) = vz;
                 end
             end
             II = (0:31)'/31;
             colormapVelocity = [[(1-II), 0*II, 0*II];[0*II, 0*II, II]];
             cfimage = imresize(cfimage,[size(whole_a,1),size(whole_a,2)]);
             figure;imagesc(cfimage);colormap(colormapVelocity);axis image;
        end
    end
end

if (1==1)
    if (frm==1)
        track_radius = str2num(get(handles.edit15,'String')); %80;
        pos_st_interest = [];
        pos_st_interest_internal_offset = [];
        n_templates = 0; % Number of speckle track patterns
        pos_st_interest_in_frame = [];
        whole_a_ff = whole_a;
        % fhd = TrainFunctionDisplay(776);
        for template_i=1:200
            str1 = sprintf('Select a Point of Interest (POI) to speckle track (or press spacebar to move on to the next stage):\n\n');
            str2 = sprintf('Choose your %d. point of interest on the ultrasound image\n\n', template_i);
            set(handles.infoedit,'String',[str1 str2]);
            % get the new Region of Interest
            k = waitforbuttonpress;
            if (not(k==0))
                if (length(pos_st_interest)>0) 
%                     vertices = pos_st_interest;
%                     line(vertices(:,1),vertices(:,2),'Color','blue','EraseMode','xor');
%                     if (size(vertices,1)>1)
%                         vertices = [vertices;vertices(1,:)];
%                     end
%                     line(vertices(:,1),vertices(:,2),'Color','blue','EraseMode','xor');
                end
                break;
            end
            n_templates = n_templates + 1;
            pos = double(int16(get(gca,'CurrentPoint')));
            pos = pos(1,1:2);
            if (length(pos_st_interest)>0) 
                vertices = pos_st_interest;
                if (size(vertices,1)>1)
                    %vertices = [vertices;vertices(1,:)];
                end
                line(vertices(:,1),vertices(:,2),'Color','blue','EraseMode','xor');
            end
            pos_st_interest = [pos_st_interest;pos];
            vertices = pos_st_interest;
            if (size(vertices,1)>1)
                %vertices = [vertices;vertices(1,:)];
            end
            line(vertices(:,1),vertices(:,2),'Color','blue','EraseMode','xor');
            hold on;plot(pos(1),pos(2),'Color','m','EraseMode','xor');
            text(pos(1), pos(2), num2str(template_i), 'Color', [0 1 0],'FontName','Segoe UI Light','FontSize',8,'HorizontalAlignment','center','VerticalAlignment','middle');
            template_y1 = pos(2)-track_radius;
            template_y2 = pos(2)+track_radius;
            template_x1 = pos(1)-track_radius;
            template_x2 = pos(1)+track_radius;
            template_x1 = min(max(1,template_x1),size(whole_a,2));
            template_x2 = min(max(1,template_x2),size(whole_a,2));
            template_y1 = min(max(1,template_y1),size(whole_a,1));
            template_y2 = min(max(1,template_y2),size(whole_a,1));
            pos = [pos(1) - template_x1, pos(2) - template_y1];
            pos_st_interest_internal_offset = [pos_st_interest_internal_offset;pos];
            if (~strcmp(ext,'.cml'))    
                temp = whole_a(template_y1:template_y2,template_x1:template_x2);
                if (template_i==1) 
                    templates = temp;
                else
                    templates = [templates {temp}];
                end
            else
                temp = cmlinfo.frame_data_I(template_y1:template_y2,template_x1:template_x2);
                if (template_i==1) 
                    templates_I = temp;
                else
                    templates_I = [templates_I {temp}];
                end
                temp = cmlinfo.frame_data_Q(template_y1:template_y2,template_x1:template_x2);
                if (template_i==1) 
                    templates_Q = temp;
                else
                    templates_Q = [templates_Q {temp}];
                end
            end        
        end
%         poly_X = pos_st_interest(:,1);%/ppmmx;
%         poly_Y = pos_st_interest(:,2);%/ppmmy;
%         poly_areas = polyarea(poly_X,poly_Y);
        poly_areas = [];
        poly_areas_low_gs = [];
        mean_displacements_x = [];
        mean_displacements_y = [];
    else
    end
end

    if (1==1)

        % pos_st_interest_in_frame = [];
        
        for template_i=1:n_templates
            if (~strcmp(ext,'.cml'))
                if (n_templates>1)
                    template = templates{template_i};
                else
                    template = templates;
                end
                NCC = normxcorr2(template, whole_a);
            else
                if (n_templates>1)
                    template = templates_I{template_i};
                else
                    template = templates_I;
                end
                NCC = normxcorr2(template, cmlinfo.frame_data_I);
            end
            [max_cc, imax] = max(NCC(:));
            ind = find(NCC==max_cc);
            if (length(ind)>1)
                msgbox('length(ind)>1','modal');
            end
            template_internal_offset = pos_st_interest_internal_offset(template_i,:);
            final_tr = nan;
            auto_threshold = get(handles.checkbox7,'Value');
            min_tr = str2num(get(handles.edit16,'String'))
            if (max_cc<1 && 1==auto_threshold)
                for tr=track_radius-5:-5:min_tr
                    pos = pos_st_interest(template_i,:);
                    template_y1 = pos(2)-tr;
                    template_y2 = pos(2)+tr;
                    template_x1 = pos(1)-tr;
                    template_x2 = pos(1)+tr;
                    template_x1 = min(max(1,template_x1),size(whole_a_ff,2));
                    template_x2 = min(max(1,template_x2),size(whole_a_ff,2));
                    template_y1 = min(max(1,template_y1),size(whole_a_ff,1));
                    template_y2 = min(max(1,template_y2),size(whole_a_ff,1));
                    if (get(handles.checkbox20,'Value')~=1 || frm==1)
                        temp = whole_a_ff(template_y1:template_y2,template_x1:template_x2);
                    else
                        temp = whole_a_pf(template_y1:template_y2,template_x1:template_x2);
                    end
                    rho=std2(temp);
                    if (rho<1) continue; end
                    pos = [pos(1) - template_x1, pos(2) - template_y1];
                    template_internal_offset_this_tr = pos;
                    NCC_this_tr = normxcorr2(temp, whole_a);
                    [max_cc_this_tr, imax_this_tr] = max(NCC_this_tr(:));
                    if (max_cc_this_tr>max_cc)
                        ind = find(NCC_this_tr==max_cc_this_tr);
                        if (length(ind)>1)
                            disp('^^^^^^^^^^^^^^^^^^^length(ind)>1');
                        end
                        NCC = NCC_this_tr;
                        imax = imax_this_tr;
                        template = temp;
                        template_internal_offset = template_internal_offset_this_tr;
                        max_cc = max_cc_this_tr;
                        final_tr = tr
                    else
                        if (max_cc_this_tr<max_cc)
                            %break;
                        end
                    end
                end
            end
            
            if (isnan(final_tr))
                final_tr = track_radius;
            end
            
            flag=true;
            while (flag) % best point search
                low_cc = 0;
                if (max_cc<0.60) 
                    low_cc = 1; 
                end;
                [ypeak, xpeak] = ind2sub(size(NCC),imax);
                corr_offset = [ (xpeak-size(template,2)+1) (ypeak-size(template,1)+1) ];
                pos = corr_offset + template_internal_offset;

                displacement_vector = pos-pos_st_interest(template_i,:);
                %displacement_vector = pos-pos_st_interest_in_frame((frm-2)*n_templates+i,:);

                if (low_cc==1)% || sqrt(displacement_vector(1)^2+displacement_vector(2)^2)>2)
                    pos = pos_st_interest(template_i,:);%[nan nan];
                    displacement_vector = [0 0];
                    max_cc=0;
                    flag=false;
                    msgbox('low_cc','modal');
                else
                    disp_limit = track_radius/2;
                    if true || (abs(displacement_vector(1))<disp_limit&&abs(displacement_vector(2))<disp_limit)
                        flag=false;
                    else
                        text(10,10,'large displacements discarded','Color','r','FontName','Throw My Hands Up in the Air','FontSize',9);
                        NCC(imax)=0;
                        [max_cc, imax] = max(NCC(:));
                    end
                end
            end
            
            clr = hsv2rgb([min(1.0,60.0/360+max_cc*300.0/360) 1.0 1.0]);
            hold on
            
            flag_illustrate_NCCs = get(handles.checkbox23,'Value');
            if flag_illustrate_NCCs
                plot(pos(1), pos(2), '.', 'Color', clr);
                text(pos(1), pos(2)+75, num2str(max_cc,'%0.2f'), 'Color', clr,'FontName','Throw My Hands Up in the Air','FontSize',9,'HorizontalAlignment','center','VerticalAlignment','middle');
            end
            
            if true % record and display speckle tracking quality
                if template_i==1
                    STQs_thisframe = max_cc;
                else
                    STQs_thisframe = [STQs_thisframe max_cc];
                end
                
                if template_i==n_templates
                    STQ_thisframe = mean(STQs_thisframe);
                    
                    if frm==1
                        STQs = STQ_thisframe;
                    else
                        STQs = [STQs STQ_thisframe];
                    end
                    
                    if frm==fileinfo.NumberOfFrames
                        h=gcf;
                        t=(0:frm-1)/fileinfo.FrameRate;

                        h2=figure;plot(t,STQs);
                        set(gca,'FontName','Segoe UI Light');
                        set(gca,'FontSize',8);
                        title('Speckle tracking quality');
                        xlabel('time(s)');
                        ylabel('mean correlation coefficient');                    

                        saveas(h2,strcat(fullname,'.STQs.th',num2str(g_threshold(1)*100),'.prefilt',...
                        num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
                        
                        figure(h);
                    end
                end
            end
            
            flag_showdisplacements = get(handles.checkbox19,'Value');
            if (flag_showdisplacements && frm>1)
                incremental = get(handles.checkbox26,'Value');
                if ~incremental
                    arrow(pos,pos+displacement_vector*10,'EdgeColor',clr, 'FaceColor',clr,'Length',4);
                else
                    prev_pos = pos_st_interest_in_frame(end+1-n_templates,:);
                    incremental_displacement = pos-prev_pos;
                    arrow(prev_pos,prev_pos+incremental_displacement*10,'EdgeColor',clr, 'FaceColor',clr,'Length',4);
                    if template_i==1
                        ST_velocities=[];
                    end
                    inc_disp_mm = incremental_displacement;
                    inc_disp_mm(1) = inc_disp_mm(1)/fileinfo.ppmmx;
                    inc_disp_mm(2) = inc_disp_mm(2)/fileinfo.ppmmy;
                    this_vel = inc_disp_mm*fileinfo.FrameRate;
                    this_vel = sqrt(this_vel(1).^2+this_vel(2).^2);
                    ST_velocities = [ST_velocities this_vel];
                    if template_i==n_templates
                        max_vel = max(ST_velocities);
                        min_vel = min(ST_velocities);
                        max_disc_vel = max_vel-min_vel;
                        mean_vel = mean(ST_velocities);
                        if frm==2
                            max_vels = [];
                            max_disc_vels = [];
                            mean_vels = [];
                        end
                        max_vels = [max_vels max_vel];
                        max_disc_vels = [max_disc_vels max_disc_vel];
                        mean_vels = [mean_vels mean_vel];
                        if frm==fileinfo.NumberOfFrames
                            h=gcf;
                            figure;
                            subplot(1,3,1);plot(max_vels);title('max_vels');
                            subplot(1,3,2);plot(max_disc_vels);title('max_disc_vels');
                            subplot(1,3,3);plot(mean_vels);title('mean_vels');
                            figure(h);
                        end
                    end
                end
            end
            
            pos_st_interest_in_frame = [pos_st_interest_in_frame;pos];
            
            if (get(handles.checkbox20,'Value')==1 ...
                    ) %&& ~isequal(pos_st_interest(i,:),pos)) % update template
                % pos_st_interest(i,:) = pos;
                template_y1 = pos(2)-track_radius;
                template_y2 = pos(2)+track_radius;
                template_x1 = pos(1)-track_radius;
                template_x2 = pos(1)+track_radius;
                template_x1 = min(max(1,template_x1),size(whole_a,2));
                template_x2 = min(max(1,template_x2),size(whole_a,2));
                template_y1 = min(max(1,template_y1),size(whole_a,1));
                template_y2 = min(max(1,template_y2),size(whole_a,1));
                pos = [pos(1) - template_x1, pos(2) - template_y1];
                pos_st_interest_internal_offset(template_i,:) = pos;
                temp = whole_a(template_y1:template_y2,template_x1:template_x2);
                if (n_templates==1) 
                    templates = temp;
                else
                    templates{template_i} = temp;
                end
            end
        end
        
        % calculate strains
        if (frm==1)
            mean_strains = [];
            mean_strains_x = [];
            mean_strains_y = [];
            mean_dists = [];
            mean_dists_x = [];
            mean_dists_y = [];
        end
        
        pos_st_interest_in_this_frame = pos_st_interest_in_frame(end-n_templates+1:end,:);
        strains = [];
        strains_x = [];
        strains_y = [];
        dists = [];
        dists_x = [];
        dists_y = [];
        
        for template_i=1:n_templates
            for j=template_i+1:n_templates
                d0 = sqrt(sum((pos_st_interest(template_i,:)-pos_st_interest(j,:)).^2));
                d1 = sqrt(sum((pos_st_interest_in_this_frame(template_i,:)-pos_st_interest_in_this_frame(j,:)).^2));
                strains = [strains;(d1-d0)/d0];
                
                dx0 = abs(pos_st_interest(template_i,1)-pos_st_interest(j,1));
                dx1 = abs(pos_st_interest_in_this_frame(template_i,1)-pos_st_interest_in_this_frame(j,1));
                if dx0~=0
                    strains_x = [strains_x;(dx1-dx0)/dx0];
                end

                dy0 = abs(pos_st_interest(template_i,2)-pos_st_interest(j,2));
                dy1 = abs(pos_st_interest_in_this_frame(template_i,2)-pos_st_interest_in_this_frame(j,2));
                if dy0~=0
                    strains_y = [strains_y;(dy1-dy0)/dy0];
                end
                
                dists_x = [dists_x;dx1/ppmmx];
                dists_y = [dists_y;dy1/ppmmy];
                dists = [dists;sqrt((dx1/ppmmx)^2+(dy1/ppmmy)^2)];
            end
        end
        
        mean_strains = [mean_strains;mean(strains)];
        mean_strains_x = [mean_strains_x;mean(strains_x)];
        mean_strains_y = [mean_strains_y;mean(strains_y)];
        mean_dists = [mean_dists;mean(dists)];
        mean_dists_x = [mean_dists_x;mean(dists_x)];
        mean_dists_y = [mean_dists_y;mean(dists_y)];
        
        if (strcmp(ext,'.xif') && frm==xifinfo.num_frames) || ...
            (~strcmp(ext,'.xif') && frm==fileinfo.NumberOfFrames)
                h=gcf;
                x=1:frm;
                if (strcmp(ext,'.xif')) 
                    x=x/xifinfo.frameRate;
                else
                    x=x/fileinfo.FrameRate;
                end

                h2=figure;subplot(2,2,[1 2]);plot(x,mean_strains);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean strains');
                xlabel('time(s)');
                ylabel('arbitrary units');                    
                
                subplot(2,2,3);plot(x,mean_strains_x);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean strains(x)');
                xlabel('time(s)');
                ylabel('arbitrary units');                    
                
                subplot(2,2,4);plot(x,mean_strains_y);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean strains(y)');
                xlabel('time(s)');
                ylabel('arbitrary units');
                
                saveas(h2,strcat(fullname,'.mean_strains.th',num2str(g_threshold(1)*100),'.prefilt',...
                num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
                
                h2=figure;subplot(2,2,[1 2]);plot(x,mean_dists);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean dists');
                xlabel('time(s)');
                ylabel('mm');                    
                                
                subplot(2,2,3);plot(x,mean_dists_x);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean dists(x)');
                xlabel('time(s)');
                ylabel('mm');                    
                                
                subplot(2,2,4);plot(x,mean_dists_y);
                set(gca,'FontName','Segoe UI Light');
                set(gca,'FontSize',8);
                title('Mean dists(y)');
                xlabel('time(s)');
                ylabel('mm');           
                
                saveas(h2,strcat(fullname,'.mean_distances.th',num2str(g_threshold(1)*100),'.prefilt',...
                num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

                figure(h);
        end
        
       %%% join speckle track with probal
       cond_bad = false;
       % smart_join = false;
       
       if (n_templates>1)
            st_vertices = pos_st_interest_in_frame(end-n_templates+1:end,:);
       
            ld = linedata{frm};
            poly_X_just_spectrack = st_vertices(:,1)/ppmmx;
            poly_Y_just_spectrack = st_vertices(:,2)/ppmmy;
            flag = get(handles.checkbox8,'Value');
            curve_number = str2num(get(handles.edit6,'String'));
            if (curve_number>size(ld,1))
                % cond_bad = true;
                disp('curve_number out of bounds');
            end
            curve_number=min(curve_number,size(ld,1));
            if (false)
                if (size(ld,1)==1)
                    ld_x = ld{1,1};
                    ld_y = ld{1,2}
                    cuff = round(length(ld_x)/2);
                    ld = [{ld_x(1:cuff) ld_y(1:cuff)}; {ld_x(cuff+1:end) ld_y(cuff+1:end)}];
                end
            end
            
            QQ = get(handles.radiobutton1,'Value');
            if (QQ==1)
                % auto-select bottom-most line
                num_curves = size(ld,1);
                if (num_curves>1)
                    grand_max_y = 0;
                    for curve_i=1:num_curves
                        ld_x = ld{curve_i,1};
                        ld_y = ld{curve_i,2};
                        max_y = max(ld_y);
                        if (max_y>=grand_max_y)
                            grand_max_y = max_y;
                            curve_number = curve_i;
                        end
                    end
                end
            end
            
            QQ = get(handles.radiobutton2,'Value');
            if (QQ==1)
                % auto-select upper-most line
                num_curves = size(ld,1);
                if (num_curves>1)
                    grand_min_y = Inf;
                    for curve_i=1:num_curves
                        ld_x = ld{curve_i,1};
                        ld_y = ld{curve_i,2};
                        min_y = min(ld_y);
                        if (min_y<=grand_min_y)
                            grand_min_y = min_y;
                            curve_number = curve_i;
                        end
                    end
                end
            end
            
        for dummy=1:1
            if (flag==1 && ~cond_bad)
                if (n_templates<3)
                    p=curve_number;
                    ld_x = ld{p,1};
                    ld_y = ld{p,2};
                    ld_vertices = [ld_x' ld_y'];
                    st_vertices = ld_vertices;
                else
                    p=curve_number;
                    ld_x = ld{p,1};
                    ld_y = ld{p,2};
                    ld_vertices = [ld_x' ld_y'];
                    ld_vertices_original = ld_vertices;

                    distx1=st_vertices(1,1)-ld_vertices(1,1);
                    disty1=st_vertices(1,2)-ld_vertices(1,2);
                    distx2=st_vertices(1,1)-ld_vertices(end,1);
                    disty2=st_vertices(1,2)-ld_vertices(end,2);
                    dist1=sqrt(distx1^2+disty1^2);
                    dist2=sqrt(distx2^2+disty2^2);
                    
                    flip_ud = get(handles.checkbox24,'Value');
                    if flip_ud==1
                        if dist1>dist2 || 1==1
                            ld_vertices = flipud(ld_vertices);
                        end
                    end

                    if (frm==15)
                        disp('yes');
                    end;

                    p1=st_vertices(1,:);
                    p2=st_vertices(2,:);
                    p3=p1;
                    unit_v=(p2-p1)/vector_length(p2-p1);
                    num_points_added=0;
                    if vector_length(p2-p1)>1
                        for template_i=2:vector_length(p2-p1)
                            p3=p3+unit_v;
                            st_vertices=[st_vertices(1:1+num_points_added,:);p3;st_vertices(2+num_points_added:end,:)];
                            num_points_added=num_points_added+1;
                        end
                    end
                    [qq,dd] = dsearchn(ld_vertices,st_vertices(1,:));
                    if (dd>5000) 
                        cond_bad = true;
                        continue;
                    end
                    j=0;
                    for template_i=1:1+num_points_added
                        [qq2,dd2] = dsearchn(ld_vertices,st_vertices(2+j,:));
                        if (dd2-dd<=0.5)
                            st_vertices=st_vertices(2+j:end,:);
                            qq=qq2;
                            dd=dd2;
                            j=0;
                        else
                            j=j+1;
                        end
                    end
                    
                    if (dd>5000) 
                        cond_bad = true;
                        continue;
                    end
                    %text(ld_vertices(qq,1),ld_vertices(qq,2),'b','Color','white');
                    qq_one=qq;
                    if (true)
                        ld_vertices = ld_vertices(qq:end,:);
                    end
                    %text(st_vertices(end,1),st_vertices(end,2),'c','Color','white');
                    p1=st_vertices(end-1,:);
                    p2=st_vertices(end,:);
                    p3=p1;
                    unit_v=(p2-p1)/vector_length(p2-p1);
                    num_points_added=0;
                    if vector_length(p2-p1)>1
                        for template_i=2:vector_length(p2-p1)
                            p3=p3+unit_v;
                            st_vertices=[st_vertices(1:end-1,:);p3;st_vertices(end,:)];
                            num_points_added=num_points_added+1;
                        end
                    end
                    [qq,dd] = dsearchn(ld_vertices,st_vertices(end,:));
                    if (dd>5000) 
                        cond_bad = true;
                        continue;
                    end
                    for template_i=1:1+num_points_added
                        [qq2,dd2] = dsearchn(ld_vertices,st_vertices(end-1,:));
                        if (dd2-dd<=0.5)
                            st_vertices=st_vertices(1:end-1,:);
                            qq=qq2;
                            dd=dd2;
                        else
                            break
                        end
                    end
                    if (dd>5000) 
                        cond_bad = true;
                        continue;
                    end
                    %text(ld_vertices(qq,1),ld_vertices(qq,2),'d','Color','white');
                    qq_two=qq;
                    if (true)
                        %ld_vertices = ld_vertices(qq_one:end,:);
                        ld_vertices = ld_vertices(1:qq_two,:);
                        
                        if (0) % setup a circular shape
                            phi = (0:pi/180/1:2*pi)';
                            ld_vertices = [100+5*fileinfo.ppmmx*cos(phi),100+5*fileinfo.ppmmy*sin(phi)];
                            hold on;
                            plot(ld_vertices(:,1),ld_vertices(:,2),'g');
                        end
                        
                        if (0) % for hypo-echoic object alike
                            ld_vertices = ld_vertices_original;
                            hold on;
                            plot(ld_vertices(:,1),ld_vertices(:,2),'g');
                        end
                        
                        % Calc polygon roughness
                        if 0
                            x=ld_vertices(:,1);
                            y=ld_vertices(:,2);
                            t=0:length(x)-1;
                            ti=0:20:t(end);
                            xi=interp1(t,x,ti,'spline')';
                            yi=interp1(t,y,ti,'spline')';
                            hold on;plot(xi,yi,'b');
                            ld_vertices = [xi yi];                            
                        end
                        
                        ld_vertices_mm = [ld_vertices(:,1)/fileinfo.ppmmx ld_vertices(:,2)/fileinfo.ppmmy];
                        devThetaIMG = zeros(size(whole_a));
                        curvatureK_IMG = zeros(size(whole_a));

                        deviationThetas = [];
                        curvaturesK = [];
                        curvaturesR = [];
                        grad_curvaturesK = [];
                        arc_len_increments = [];
                        
                        num_points = size(ld_vertices,1);
                        
                        arrows_num = 0;
                        arrows_sum = 0;

                        for pt=2:num_points-1
                            pt1 = [ld_vertices(pt-1,1) ld_vertices(pt-1,2) 0];
                            pt2 = [ld_vertices(pt,1) ld_vertices(pt,2) 0];
                            pt3 = [ld_vertices(pt+1,1) ld_vertices(pt+1,2) 0];
                            
                            pt1mm = [ld_vertices_mm(pt-1,1) ld_vertices_mm(pt-1,2) 0];
                            pt2mm = [ld_vertices_mm(pt,1) ld_vertices_mm(pt,2) 0];
                            pt3mm = [ld_vertices_mm(pt+1,1) ld_vertices_mm(pt+1,2) 0];

                            u = pt1mm-pt2mm;
                            v = pt3mm-pt2mm;
                            CosTheta = dot(u,v)/(norm(u)*norm(v));
                            ThetaInDegrees = acos(CosTheta)*180/pi;
                            deviationTheta = 180-ThetaInDegrees;
                            deviationThetas = [deviationThetas deviationTheta];
                            
                            arclen_element = (norm(u)+norm(v))/2;
                            
                            curvatureK = (deviationTheta/180*pi)/arclen_element;
                            curvaturesK = [curvaturesK curvatureK];
                            curvaturesR = [curvaturesR 1/curvatureK];
                            arc_len_increments = [arc_len_increments arclen_element];
                
                            if pt>2
                                grad_curvatureK = (curvaturesK(end)-curvaturesK(end-1))/arclen_element;
                                grad_curvatureK = abs(grad_curvatureK);
                                grad_curvaturesK = [grad_curvaturesK grad_curvatureK];
                            end
                            
                            devThetaIMG(round(pt2(2)),round(pt2(1))) = devThetaIMG(round(pt2(2)),round(pt2(1)))+deviationTheta;
                            curvatureK_IMG(round(pt2(2)),round(pt2(1))) = curvatureK_IMG(round(pt2(2)),round(pt2(1)))+curvatureK;
                            
                            %if (deviationTheta>=30&&deviationTheta<=999)                            
                            if (curvatureK>10)                            
                                Start = [pt2(1) pt2(2)-25];
                                Stop = [pt2(1) pt2(2)];
%                                 Start = [pt2(1) pt2(2)+25];
%                                 Stop = [pt2(1) pt2(2)];
                                clr = hsv2rgb([deviationTheta/180 1.0 1.0]);
                                %arrow(Start,Stop,'Length',5,'TipAngle',30,'EdgeColor',clr,'FaceColor',clr);
                                arrows_num=arrows_num+1;
                                arrows_sum=arrows_sum+deviationTheta;
                            end
                            
                            if false && frm==1 % make surface curvature animation
                                h = gcf;
                                
                                if pt==2
                                    h_curvanim = figure('Position',[100 100 800 600]);
                                    set(gcf,'Color','white')
                                else
                                    figure(h_curvanim);
                                end
                                
                                clf;
                                subplot(2,2,1);
                                plot(ld_vertices(:,1),ld_vertices(:,2));
                                title('Plaque surface');
                                xlabel('x position (pixels)');ylabel('y position (pixels)');
                                axis equal;
                                hold on;
                                %DrawCircle(pt2(1),pt2(2),1/curvatureK,360,'-r');
                                
                                myu=pt2-pt1;
                                clr=[0 0 0.5];
                                Start=[pt2(1) pt2(2)];
                                arrow(Start,Start+myu(1:2),'Length',8,'TipAngle',30,'EdgeColor',clr,'FaceColor',clr)
                                
                                subplot(2,2,2);
                                plot(deviationThetas);title('deviationThetas');xlabel('position index');ylabel('degrees');
                                axis tight;
                                subplot(2,2,3);
                                plot(arc_len_increments*1E3);title('arc_len_increments','Interpreter', 'none');xlabel('position index');ylabel('�m');
                                axis tight;
                                subplot(2,2,4);
                                plot(curvaturesK);title('curvaturesK');xlabel('position index');ylabel('radians/mm');
                                axis tight;

                                f = getframe(h_curvanim);
                                f.cdata = imresize(f.cdata,1.5,'bilinear');
                                M_curvanim(pt-1) = f;

                                figure(h);

                                if pt==num_points-1
                                     movie_file_name = strcat(fullname,'.mv_curvanim.th',num2str(g_threshold(1)*100),'.prefilt',...
                                       num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.avi');

                                    if (exist(movie_file_name))
                                        delete(movie_file_name);
                                    end

                                    movie2avi(M_curvanim,movie_file_name,'compression','Cinepak','quality',100,'fps',15);

                                    winopen(movie_file_name);
                                end
                            end
                        end
                        
                        disp(sprintf('arrows_num=%d',arrows_num));
                        
                        disp(sprintf('arrows_sum=%ld',arrows_sum));
                        
                        deviationThetas_localstds = zeros(numel(deviationThetas),1);
                        local_std_arm_len = 5;
                        for theta_index=1:numel(deviationThetas)
                            local_start = max(1,theta_index-local_std_arm_len);
                            local_end = min(theta_index+local_std_arm_len,numel(deviationThetas));
                            deviationThetas_localstds(theta_index)=std(deviationThetas(local_start:local_end));
                        end
                        
                        devTheta_localstdsIMG = zeros(size(whole_a));
                        
                        num_points = size(ld_vertices,1);
                        for pt=2:num_points-1
                            pt1 = [ld_vertices(pt-1,1) ld_vertices(pt-1,2) 0];
                            pt2 = [ld_vertices(pt,1) ld_vertices(pt,2) 0];
                            pt3 = [ld_vertices(pt+1,1) ld_vertices(pt+1,2) 0];
                            
                            devTheta_localstdsIMG(round(pt2(2)),round(pt2(1))) = ...
                                devTheta_localstdsIMG(round(pt2(2)),round(pt2(1)))+deviationThetas_localstds(pt-1);
                        end
                        
                        if (frm==1)
                            sumtheta=sum(deviationThetas);
                            sumarclen=sum(arc_len_increments);

                            theta_factors = mean(deviationThetas);
                            theta_stds = std(deviationThetas);
                            curvaturesK_median = median(curvaturesK);
                            curvaturesK_std = std(curvaturesK);
                            curvaturesK_max = max(curvaturesK);
                            curvaturesR_median = median(curvaturesR);
                            curvaturesR_std = std(curvaturesR);
                            grad_curvaturesK_median = median(grad_curvaturesK);
                            grad_curvaturesK_std = std(grad_curvaturesK);
                            grad_curvaturesK_max = max(grad_curvaturesK);
                            mean_curvaturesR = sumarclen/(sumtheta/180*pi);
                            mean_curvaturesK = (sumtheta/180*pi)/sumarclen;
                            h=gcf;
                            devThetaIMG_h=figure;imagesc(devThetaIMG);title('devThetaIMG');
                            devTheta_localstdsIMG_h=figure;imagesc(devTheta_localstdsIMG);title('devTheta_localstdsIMG');
                            curvatureK_IMG_h=figure;imagesc(curvatureK_IMG);title('curvatureK_IMG');

                            str = ['r=' num2str(sumarclen/(sumtheta/180*pi))];
                            figure;plot(deviationThetas);title(['deviationThetas,frm1,' str]);
                            figure;plot(curvaturesK);title('curvaturesK,frm1');
                            if false
                                figure;plot(curvaturesR);title('curvaturesR,frm1');
                            end
                            figure;plot(grad_curvaturesK);title('grad_curvaturesK,frm1');
                            figure;plot(arc_len_increments);title('arc_len_increments,frm1');
                            figure(h);
                        else
                            sumtheta=sum(deviationThetas);
                            sumarclen=sum(arc_len_increments);

                            theta_factors = [theta_factors mean(deviationThetas)];
                            theta_stds = [theta_stds std(deviationThetas)];
                            curvaturesK_median = [curvaturesK_median median(curvaturesK)];
                            curvaturesK_std = [curvaturesK_std std(curvaturesK)];
                            curvaturesK_max = [curvaturesK_max max(curvaturesK)];
                            curvaturesR_median = [curvaturesR_median median(curvaturesR)];
                            curvaturesR_std = [curvaturesR_std std(curvaturesR)];
                            grad_curvaturesK_median = [grad_curvaturesK_median median(grad_curvaturesK)];
                            grad_curvaturesK_std = [grad_curvaturesK_std std(grad_curvaturesK)];
                            grad_curvaturesK_max = [grad_curvaturesK_max max(grad_curvaturesK)];
                            mean_curvaturesR = [mean_curvaturesR sumarclen/(sumtheta/180*pi)];
                            mean_curvaturesK = [mean_curvaturesK (sumtheta/180*pi)/sumarclen];
                            %h=gcf;
                            %figure(devThetaIMG_h);imagesc(devThetaIMG);title('devThetaIMG');
                            %figure(devTheta_localstdsIMG_h);imagesc(devTheta_localstdsIMG);title('devTheta_localstdsIMG');
                            %figure(curvatureK_IMG_h);imagesc(curvatureK_IMG);title('curvatureK_IMG');
                            %figure(h);

%                             h=gcf;
%                             str = ['r=' num2str(sumarclen/(sumtheta/180*pi))];
%                             str2 = ['frm' num2str(frm)];
%                             figure;plot(deviationThetas);title(['deviationThetas,' str2 ',' str]);
%                             figure;plot(curvaturesK);title(['curvaturesK,' str2]);
%                             figure;plot(curvaturesR);title(['curvaturesR,' str2]);
%                             figure;plot(grad_curvaturesK);title(['grad_curvaturesK,' str2]);
%                             figure;plot(arc_len_increments);title(['arc_len_increments,' str2]);
%                             figure(h);
                        end
                        
                        hold on;
                        %%%plot(ld_vertices(:,1),ld_vertices(:,2),'c','LineWidth',1);
                        
                        st_vertices = [ld_vertices;flipud(st_vertices);ld_vertices(1,:)];
                    else
                        qq_min=min(qq_one,qq_two);
                        qq_max=max(qq_one,qq_two);
                        ld_vertices = ld_vertices(qq_min:qq_max,:);
                        st_vertices = [ld_vertices;st_vertices;ld_vertices(1,:)];
                    end

                    if (cond_bad)
                        disp('skip frame?');
                    else
                        ld = linedata{frm};
                        ld=[{ld_vertices(:,1)' ld_vertices(:,2)'}; {st_vertices(:,1)' st_vertices(:,2)'}];
                        linedata{frm} = ld;
                    end
                end
                
                if frm==1
                    mean_displacements_surf_x = [];
                    mean_displacements_surf_y = [];
                    ld_vertices_prevfrm = ld_vertices;
                end
                
                mean_x = mean(ld_vertices(:,1));
                mean_x_prevfrm = mean(ld_vertices_prevfrm(:,1));
                mean_y = mean(ld_vertices(:,2));
                mean_y_prevfrm = mean(ld_vertices_prevfrm(:,2));
                mean_displacements_surf_x = [mean_displacements_surf_x (mean_x-mean_x_prevfrm)/ppmmx];
                mean_displacements_surf_y = [mean_displacements_surf_y (mean_y-mean_y_prevfrm)/ppmmy];
                ld_vertices_prevfrm = ld_vertices;
                
                if (~cond_bad)
                    inside_points_x = [];
                    inside_points_y = [];
                    whole_a_plaque = [];
                    whole_a_plaque_normalized = [];
                    area = 0;
                    whole_a_colour_data = whole_a*0;
                    whole_a_plaque_colour_data = [];
                    for y=1:size(whole_a,1)
                        xx=1:size(whole_a,2);
                        yy=ones(1,size(whole_a,2))*y;
                        in = inpolygon(xx,yy,st_vertices(:,1),st_vertices(:,2));
                        if ~isempty(xx(in))
                            inside_points_x=[inside_points_x xx(in)];
                            inside_points_y=[inside_points_y yy(in)];
                        end
                        if (1==1)
                            in_xx = xx(in);
                            in_yy = yy(in);
                            for k=1:length(in_xx)
                                tx = in_xx(k);
                                ty = in_yy(k);
                                glevel = whole_a(ty,tx);
                                
                                sum_num = 0;
                                sum_denom = 0;
                                for pt=2:size(ld_vertices,1)-1
                                    %[qq,dd] = dsearchn(ld_vertices(pt,:),[tx ty]);
                                    dd = sqrt((tx-ld_vertices(pt,1))^2+(ty-ld_vertices(pt,2))^2);
                                    dd = max(dd,1E-3);
                                    %text(ld_vertices(qq,1),ld_vertices(qq,2),'x','Color','green');
                                    sum_num = sum_num+deviationThetas_localstds(pt-1)/dd;
                                    sum_denom = sum_denom+1/dd;
                                end
                                
                                if (sum_denom==0) 
                                    sr = 0;
                                else
                                    sr = sum_num/sum_denom;
                                end
                                
                                TEMP = sr*(255-glevel);
                                whole_a_colour_data(ty,tx) = TEMP;
                                whole_a_plaque_colour_data = [whole_a_plaque_colour_data TEMP];
                                
                                if whole_a_normalized(ty,tx)<40
                                    if (get(handles.checkbox18,'Value')==1)
                                        plot(tx,ty,'b.','MarkerSize', 1, 'EraseMode','xor');
                                    end
                                    area = area+1/(ppmmx*ppmmy);
                                end
                            end
                        end
                        whole_a_plaque = [whole_a_plaque whole_a(y,xx(in))];
                        whole_a_plaque_normalized = [whole_a_plaque_normalized whole_a_normalized(y,xx(in))];
                    end
                    poly_areas_low_gs = [poly_areas_low_gs area];
                    
                    if (frm==1)
                        mean_colour_data = [];
                    end
                    
                    mean_colour_data = [mean_colour_data mean(whole_a_plaque_colour_data)];

                    hdisk = fspecial('disk',double(2));
                    whole_a_colour_data = imfilter(whole_a_colour_data,hdisk);
                    h=gcf;
                    if (frm==1) 
                        hcolorfig = figure;imagesc(whole_a_colour_data);title('whole_a_colour_data');
                    else
                        figure(hcolorfig);imagesc(whole_a_colour_data);title('whole_a_colour_data');
                    end
                    figure(h);

                    if (frm==1) 
                        mean_gs_plaque = [];
                        median_gs_plaque = [];

                        mean_gs_plaque_normalized = [];
                        median_gs_plaque_normalized = [];
                    end
                    
                    h=gcf;
                    if (frm==1) 
                        h_histfig = figure;
                    else
                        %figure(h_histfig);
                    end
                    if (frm==1)
                        hist(whole_a_plaque,256);title(['frm ' num2str(frm)]);
                    end
                    figure(h);

                    mean_gs_plaque = [mean_gs_plaque mean(whole_a_plaque)];

                    median_gs_plaque = [median_gs_plaque median(whole_a_plaque)];

                    mean_gs_plaque_normalized = [mean_gs_plaque_normalized mean(whole_a_plaque_normalized)];

                    median_gs_plaque_normalized = [median_gs_plaque_normalized median(whole_a_plaque_normalized)];

                    if (max(median_gs_plaque)-min(median_gs_plaque)>=20)
                        disp('x');
                    end

                    if (strcmp(ext,'.xif') && frm==xifinfo.num_frames) || ...
                       (~strcmp(ext,'.xif') && frm==fileinfo.NumberOfFrames)                   
                        h=gcf;
                        x=1:frm;
                        if (strcmp(ext,'.xif')) 
                            x=x/xifinfo.frameRate;
                        else
                            x=x/fileinfo.FrameRate;
                        end

                        if false
                            h2=figure;plot(x,mean_gs_plaque);
                            set(gca,'FontName','Segoe UI Light');
                            set(gca,'FontSize',8);
                            title('Plaque mean greyscale');
                            xlabel('time(s)');
                            ylabel('Greyscale intensity');                    
                                saveas(h2,strcat(fullname,'.me.th',num2str(g_threshold(1)*100),'.prefilt',...
                num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

                            h2=figure;plot(x,mean_gs_plaque_normalized);
                            set(gca,'FontName','Segoe UI Light');
                            set(gca,'FontSize',8);
                            title('Plaque mean greyscale (normalized)');
                            xlabel('time(s)');
                            ylabel('Greyscale intensity');                    
                                saveas(h2,strcat(fullname,'.meN.th',num2str(g_threshold(1)*100),'.prefilt',...
                num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
                        end


                        h2=figure;plot(x,median_gs_plaque);
                        set(gca,'FontName','Segoe UI Light');
                        set(gca,'FontSize',8);
                        title('Plaque median greyscale');
                        xlabel('time(s)');
                        ylabel('Greyscale intensity');                    
                            saveas(h2,strcat(fullname,'.md.th',num2str(g_threshold(1)*100),'.prefilt',...
            num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

                        h2=figure;plot(x,median_gs_plaque_normalized);
                        set(gca,'FontName','Segoe UI Light');
                        set(gca,'FontSize',8);
                        title('Plaque median greyscale (normalized)');
                        xlabel('time(s)');
                        ylabel('Greyscale intensity');                    
                            saveas(h2,strcat(fullname,'.mdN.th',num2str(g_threshold(1)*100),'.prefilt',...
            num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

                        h2=figure;plot(x,mean_colour_data);
                        set(gca,'FontName','Segoe UI Light');
                        set(gca,'FontSize',8);
                        title('Plaque mean colour data');
                        xlabel('time(s)');
                        ylabel('arbitrary units');                    
                        saveas(h2,strcat(fullname,'.meCD.th',num2str(g_threshold(1)*100),'.prefilt',...
            num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

    %                     X = mean_gs_plaque;
    %                     N = length(X);
    %                     Y = abs(fft(X))/(N/2);
    %                     f = fileinfo.FrameRate/2*linspace(0,1,N/2)
    %                     power_spectrum = Y.^2;
    %                     figure;
    %                     plot(f,power_spectrum(1:N/2)*2); title('power_spectrum(1:N/2)*2');
                        figure(h); 
                    end
                end
            
                if (~flag_showdisplacements)
                    x=st_vertices(:,1);
                    y=st_vertices(:,2);
                    line(x,y,'LineStyle',':','Color','green','EraseMode','normal');
                    k=convhull(x,y);
                    %line(x(k),y(k),'LineStyle',':','Color','white','EraseMode','normal');

                    hull_X = x(k)/ppmmx;
                    hull_Y = y(k)/ppmmy;
                    hull_area = polyarea(hull_X,hull_Y);
                    
                    if (frm==1)
                        hull_areas = [hull_area];
                    else
                        hull_areas = [hull_areas hull_area];
                    end
                end
            else
                if (~flag_showdisplacements)
                    line(st_vertices(:,1),st_vertices(:,2),'LineStyle',':','Color','green','EraseMode','normal');
                end
                if (1==0)
                    ld = linedata{frm,1};
                    ld{1,1} = st_vertices(:,1)';
                    ld{1,2} = st_vertices(:,2)';
                end
            end
       end

            if (cond_bad)
                if (~exist('mean_gs_plaque','var'))
                    mean_gs_plaque = [];
                end
                if (~exist('median_gs_plaque','var'))
                    median_gs_plaque = [];
                end
                if (~exist('mean_gs_plaque_normalized','var'))
                    mean_gs_plaque_normalized = [];
                end
                if (~exist('median_gs_plaque_normalized','var'))
                    median_gs_plaque_normalized = [];
                end
                mean_gs_plaque = [mean_gs_plaque nan];
                median_gs_plaque = [median_gs_plaque nan];
                mean_gs_plaque_normalized = [mean_gs_plaque_normalized nan];
                median_gs_plaque_normalized = [median_gs_plaque_normalized nan];
                poly_areas = [poly_areas nan];
            else
                poly_X = st_vertices(:,1)/ppmmx;
                poly_Y = st_vertices(:,2)/ppmmy;
                poly_area = polyarea(poly_X,poly_Y);
                poly_areas = [poly_areas poly_area];
            end

            if (frm>0 || true)
                poly_X_original_loc = pos_st_interest(:,1)/ppmmx;
                poly_Y_original_loc = pos_st_interest(:,2)/ppmmy;
                mean_displacement_x = mean(poly_X_just_spectrack-poly_X_original_loc);
                mean_displacement_y = mean(poly_Y_just_spectrack-poly_Y_original_loc);
                mean_displacements_x = [mean_displacements_x mean_displacement_x];
                mean_displacements_y = [mean_displacements_y mean_displacement_y];
                if (size(st_vertices,1)>1)
                    %st_vertices = [st_vertices;st_vertices(1,:)];
                end
            end
        end
    end

    if (create_movie)
        str = sprintf('Capturing frame %d',frm);
        set(handles.infoedit,'String',str);
        drawnow;
        hold on;
        text(50, 50, ['frame ' num2str(frm)], 'Color', 'magenta','FontName','Segoe UI Light','FontSize',8);
        f = getframe;
        
        if (frm==1||frm==151)
            disp('here');
        end
        
        
        if (1==0)
        if (not(isempty(pos_interest))) % Clip to ROI
            sy = size(whole_a,1)/size(f.cdata,1);
            sx = size(whole_a,2)/size(f.cdata,2);
            
            f.cdata = f.cdata(ceil(pos_interest(2)/sy):min(size(f.cdata,1),floor((pos_interest(2)+dim_interest(2))/sy)),...
                              ceil(pos_interest(1)/sx):min(size(f.cdata,2),floor((pos_interest(1)+dim_interest(1))/sx)),:);
        end
        end
        % f.cdata = imresize(f.cdata,[263 256 3],'bilinear');
        % f.cdata(:,:,2) = f.cdata(:,:,2)*0+1;
% % %         f.cdata = rgb2hsv(f.cdata);
% % %         f.cdata(:,:,3) = min(1.0,f.cdata(:,:,3)*1.50);
% % %         f.cdata = hsv2rgb(f.cdata);
        f.cdata = imresize(f.cdata,2.5,'bilinear');
        
        slow_down_factor = 1;
        for template_i=1:slow_down_factor
            if (exist('prev_f','var'))
                if (size(f.cdata)~=size(prev_f.cdata))
                        f.cdata = imresize(f.cdata, size(prev_f.cdata));
                end
            end
            try
                M((frm-1)*slow_down_factor+template_i) = f;
            catch err
                disp(err.identifier);
            end
            prev_f = f;
        end
% %         M(frm)=f;
    end
    
    frm = frm + 1;
    
    if (strcmp(ext,'.mtf'))
        [str cnt] = fscanf(fid, '%s\n', 1);
    else
        if (strcmp(ext,'.xif'))
            cnt = 1;

            if (frm>xifinfo.num_frames)
                break;
            end

            str = sprintf('Extracting frame %d of %d',frm,xifinfo.num_frames);
            set(handles.infoedit,'String',str);
            drawnow;

            [xifinfo,tdata.la] = readxif(fullname,frm,xifinfo);

            if (not(xifinfo.result))
                disp(xifinfo.errmsg);
                disp(xifinfo.syserrmsg);
            end        
        else
        if (strcmp(ext,'.avi')||strcmp(ext,'.mpg')||strcmp(ext,'.wmv'))
            cnt = 1;

            if (frm>fileinfo.NumberOfFrames)
                break;
            end

            str = sprintf('Extracting frame %d of %d',frm,fileinfo.NumberOfFrames);
            set(handles.infoedit,'String',str);
            drawnow;

            tdata.la = readavi(fullname,frm);
        else
        if (strcmp(ext,'.cml'))
            cnt = 1;

            if (frm>cmlinfo.Nt)
                break;
            end

            str = sprintf('Extracting frame %d of %d',frm,cmlinfo.Nt);
            set(handles.infoedit,'String',str);
            drawnow;

            [cmlinfo,tdata.la] = readcml(handles,cmlinfo.MyFileName,frm);
        else
            cnt = 1;

            if (frm>fileinfo.NumberOfFrames)
                break;
            end

            str = sprintf('Extracting frame %d of %d',frm,fileinfo.NumberOfFrames);
            set(handles.infoedit,'String',str);
            drawnow;

            tdata.la = readdicom(fileinfo.MyFileName,frm);
        end
        end
    end
end
end

if (strcmp(ext,'.mtf'))
    fclose(fid);
end

if (exist('fileinfo','var') && isfield(fileinfo,'FrameRate'))
    frameRate=fileinfo.FrameRate;
else
    frameRate=xifinfo.frameRate;
end
    
if (length(poly_areas)>0)
    cmp = (max(poly_areas)-min(poly_areas))/max(poly_areas);
    text(50,90,['compression = ' num2str(cmp,'%0.2f')],'Color', 'magenta','FontName','Segoe UI Light','FontSize',8);
    
    x=0:length(poly_areas)-1;
    x=x/frameRate;
    h=figure;subplot(1,2,1);plot(x,poly_areas);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    xlabel('time(s)');
    ylabel('area (mm^2)');
    title(['Plaque area, max compression=' num2str(cmp,'%0.2f')]);

    x=0:length(poly_areas_low_gs)-1;
    x=x/frameRate;
    subplot(1,2,2);plot(x,poly_areas_low_gs);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    xlabel('time(s)');
    ylabel('area (mm^2)');
    title(['poly_areas_low_gs']);
    saveas(h,strcat(fullname,'.pa.th',num2str(g_threshold(1)*100),'.prefilt',...
        num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));

    if exist('hull_areas','var') && false
        h=figure;plot(x,hull_areas);
        set(gca,'FontName','Segoe UI Light');
        set(gca,'FontSize',8);
        xlabel('time(s)');
        ylabel('area (mm^2)');
        title('Hull area');
        saveas(h,strcat(fullname,'.ha.th',num2str(g_threshold(1)*100),'.prefilt',...
            num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
    end
end

x=0:length(mean_displacements_x)-1;
x=x/frameRate;
figure;subplot(2,2,1);plot(x,mean_displacements_x);
set(gca,'FontName','Segoe UI Light');
set(gca,'FontSize',8);
title('mean displacements_x');
axis tight;
xlabel('time(s)');
ylabel('displacement (mm)');

x=0:length(mean_displacements_y)-1;
x=x/frameRate;
subplot(2,2,2);plot(x,mean_displacements_y);
set(gca,'FontName','Segoe UI Light');
set(gca,'FontSize',8);
title('mean displacements_y');
axis tight;
xlabel('time(s)');
ylabel('displacement (mm)');

% save('mean_displacements','mean_displacements_x','mean_displacements_y','frameRate');

mean_displacements_ABS = sqrt(mean_displacements_x.^2+mean_displacements_y.^2);
subplot(2,2,3);
plot(x,mean_displacements_ABS);
set(gca,'FontName','Segoe UI Light');
set(gca,'FontSize',8);
title('mean displacements');
axis tight;
xlabel('time(s)');
ylabel('displacement (mm)');

mean_displacements_VEL = gradient(mean_displacements_ABS,1/frameRate);
subplot(2,2,4);
plot(x,mean_displacements_VEL);
set(gca,'FontName','Segoe UI Light');
set(gca,'FontSize',8);
title('mean displacements (differential)');
axis tight;
xlabel('time(s)');
ylabel('differential (mm/s)');

if exist('mean_displacements_surf_x','var') && exist('mean_displacements_surf_y','var')
    x=0:length(mean_displacements_surf_x)-1;
    x=x/frameRate;
    figure;subplot(1,2,1);plot(x,mean_displacements_surf_x);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('mean displacements_surf_x');

    x=0:length(mean_displacements_surf_y)-1;
    x=x/frameRate;
    subplot(1,2,2);plot(x,mean_displacements_surf_y);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('mean displacements_surf_y');
end

if false && exist('theta_factors','var') && exist('theta_stds','var')
    h3=figure;subplot(1,2,1);plot(theta_factors);
    %errorbar(theta_factors,theta_stds/100);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('theta__factors');
    axis tight;

    subplot(1,2,2);plot(theta_stds);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('theta__stds');
    axis tight;

    saveas(h3,strcat(fullname,'.theta_factors.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end

if false && exist('mean_curvaturesR','var')
    h3=figure;subplot(1,1,1);plot(mean_curvaturesR);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('meanCurvaturesR');
    axis tight;

    saveas(h3,strcat(fullname,'.mean_curvaturesR.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end

if exist('mean_curvaturesK','var')
    h3=figure;subplot(1,1,1);plot(mean_curvaturesK);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('meanCurvaturesK');
    axis tight;

    saveas(h3,strcat(fullname,'.mean_curvaturesK.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end

if exist('curvaturesK_median','var') && exist('curvaturesK_std','var') ...
        && exist('curvaturesK_max','var')
    h3=figure;subplot(1,3,1);plot(curvaturesK_median);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('curvaturesK__median');
    axis tight;

    subplot(1,3,2);plot(curvaturesK_std);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('curvaturesK__std');
    axis tight;

    subplot(1,3,3);plot(curvaturesK_max);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('curvaturesK__max');
    axis tight;

    saveas(h3,strcat(fullname,'.curvaturesK.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end

if false && exist('curvaturesR_median','var') && exist('curvaturesR_std','var')
    h3=figure;subplot(1,2,1);plot(curvaturesR_median);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('curvaturesR__median');
    axis tight;

    subplot(1,2,2);plot(curvaturesR_std);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('curvaturesR__std');
    axis tight;

    saveas(h3,strcat(fullname,'.curvaturesR.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end
    
if exist('grad_curvaturesK_median','var') && exist('grad_curvaturesK_std','var') ...
        && exist('grad_curvaturesK_max','var')
    h3=figure;subplot(1,3,1);plot(grad_curvaturesK_median);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('grad_curvaturesK__median');
    axis tight;

    subplot(1,3,2);plot(grad_curvaturesK_std);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('grad_curvaturesK__std');
    axis tight;

    subplot(1,3,3);plot(grad_curvaturesK_max);
    set(gca,'FontName','Segoe UI Light');
    set(gca,'FontSize',8);
    title('grad_curvaturesK__max');
    axis tight;

    saveas(h3,strcat(fullname,'.grad_curvaturesK.th',num2str(g_threshold(1)*100),'.prefilt',...
    num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.fig'));
end

% save
if (not(in_training==-1) & create_movie)
    % implay(M);

    movie_file_name = strcat(fullname,'.mv.th',num2str(g_threshold(1)*100),'.prefilt',...
       num2str(prefilter),'.',datestr(now,'yyyymmdd_HHMMSS'),'.avi');

    set(handles.infoedit,'String',strcat('Writing to movie file ',movie_file_name));
    drawnow;
           
    if (exist(movie_file_name))
        delete(movie_file_name);
    end

     compressionStr = 'None';
     if ispc
         compressionStr = 'None';
     end
    
     if (strcmp(ext,'.cml'))
        movie2avi(M,movie_file_name,'compression',compressionStr,'quality',100,'fps',cmlinfo.PRF);
     else
         if (strcmp(ext,'.xif'))
            movie2avi(M,movie_file_name,'compression',compressionStr,'quality',100,'fps',xifinfo.frameRate);
         else
            fR = fileinfo.FrameRate;
            fR = 10;
            movie2avi(M,movie_file_name,'compression',compressionStr,'quality',100,'fps',fR);
         end
     end
     
    winopen(movie_file_name);
end

in_training = 0;

set(handles.infoedit,'String','');
drawnow;

MeasurementFunSetup(handles,linedata);
    

% --- Executes during object creation, after setting all properties.
function ThresholdSlider_CreateFcn(hObject, eventdata, handles)
global g_threshold
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background, change
%       'usewhitebg' to 0 to use default.  See ISPC and COMPUTER.
usewhitebg = 1;
if usewhitebg
    set(hObject,'BackgroundColor',[.9 .9 .9]);
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% --- Executes on slider movement.
function ThresholdSlider_Callback(hObject, eventdata, handles)
global g_threshold
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
g_threshold = get(hObject,'Value');

str = sprintf('Threshold (%.2f%%)',g_threshold*100);

set(handles.ThresholdText,'String',str);


% --- Executes on button press in ThresholdRestoreDefault.
function ThresholdRestoreDefault_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.01);
set(handles.RangeSlider,'Value',1);
set(handles.PThresholdslider,'Value',0.10);
set(handles.pfilterslider,'Value',0);
set(handles.prefilterslider,'Value',5);

ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);
RangeSlider_Callback(handles.RangeSlider, [], handles);
PThresholdslider_Callback(handles.PThresholdslider, [], handles);
pfilterslider_Callback(handles.pfilterslider, [], handles);
prefilterslider_Callback(handles.prefilterslider, [], handles);


% --- Executes during object creation, after setting all properties.
function application_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to application_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on selection change in application_menu.
function application_menu_Callback(hObject, eventdata, handles)
% hObject    handle to application_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns application_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from application_menu
ThresholdRestoreDefault_Callback([], [], handles);



% --------------------------------------------------------------------
function option_preload_cineloops_Callback(hObject, eventdata, handles)
global preload_cineloops
% hObject    handle to option_preload_cineloops (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preload_cineloops = not(preload_cineloops)

if (preload_cineloops)
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end    



% --------------------------------------------------------------------
function option_maximize_contrast_Callback(hObject, eventdata, handles)
global maximize_contrast
% hObject    handle to option_maximize_contrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
maximize_contrast = not(maximize_contrast)

if (maximize_contrast)
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end    


% --------------------------------------------------------------------
function option_create_movie_Callback(hObject, eventdata, handles)
global create_movie
% hObject    handle to option_create_movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
create_movie = not(create_movie)

if (create_movie)
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end    


% --------------------------------------------------------------------
function option_median_filter_results_Callback(hObject, eventdata, handles)
global median_filter
% hObject    handle to option_median_filter_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
median_filter = not(median_filter)

if (median_filter)
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end    

%STRENDSWITH Determines whether a string ends with a specified pattern
%
%   b = strstartswith(s, pat);
%       returns whether the string s ends with a sub-string pat.
%

%   History
%   -------
%       - Created by Dahua Lin, on Oct 9, 2008
%

function b = strendswith(s, pat)

sl = length(s);
pl = length(pat);

b = (sl >= pl && strcmp(s(sl-pl+1:sl), pat)) || isempty(pat);


function pathStrOut = appendFileSep(pathStrIn)
if ~strendswith(pathStrIn,filesep)
    pathStrOut=strcat(pathStrIn,filesep);
else
    pathStrOut=pathStrIn;
end


function loadCurrentDirectoryContents(handles)
global nm_path driveLetters

if isempty(nm_path) || not(ischar(nm_path))
    nm_path = appendFileSep(pwd);
end
    
set(handles.editPath,'String',nm_path);

set(handles.infoedit,'String','Cataloging directory contents, please be patient...');
drawnow;

filenames = [];
filesizes = [];
notes = [];
total_size = 0;
total_folders = 0;

for i=1:numel(driveLetters)
    filenames = [filenames;{driveLetters{i}}];
    filesizes = [filesizes;{''}];
    notes = [notes;{['Drive ' driveLetters{i}]}];
end

dirdata = dir(nm_path);
for i = 1:size(dirdata,1)
    set(handles.infoedit,'String',['Cataloging directory contents, please be patient...' num2str(i)]);
    drawnow;
    
    if (dirdata(i).isdir==0) 
        continue
    end
    
    dirname = dirdata(i).name;
    
    if strcmp(dirname,'.') 
        continue
    end
    
    if strcmp(dirname,'..')
        filenames = [filenames;{dirname}];
        filesizes = [filesizes;{''}];
        notes = [notes;{'Folder up'}];
        continue
    end
    
    dirname=[dirname filesep];
        
    total_folders = total_folders+1;
    
    filenames = [filenames;{dirname}];
    filesizes = [filesizes;{''}];
    notes = [notes;{''}];
end

extensions = {'*.xif', '*.cml','*.mtf','US00*.','*.dcm','*.avi','*.jpg','*.tif','*.mpg','*.wmv','*.gif'};

for exti=1:size(extensions,2)
    
    dirdata = dir([nm_path extensions{exti}]);
    
    for i=1:size(dirdata,1)
        
        set(handles.infoedit,'String',['Cataloging directory contents, please be patient...\r\n' num2str(exti) '.' num2str(i)]);
        drawnow;
        
        file_notes = '';

        fileID = fopen(strcat(nm_path,dirdata(i).name,'.inf'),'r');
        
        if fileID>=0
            
            StudyDate=fgets(fileID);
            if (StudyDate==-1) 
                StudyDate='?';
            end
            
            ContentTime=fgets(fileID);
            if (ContentTime==-1) 
                ContentTime='?';
            end
            
            UltrasoundColorDataPresent=fgets(fileID);
            if (UltrasoundColorDataPresent==-1) 
                UltrasoundColorDataPresent='?';
            end
            
            FamilyName=fgets(fileID);
            if (FamilyName==-1) 
                FamilyName='?';
            end
            
            fclose(fileID);
            
            file_notes = strcat(StudyDate,'.',FamilyName,'.',ContentTime,'.',UltrasoundColorDataPresent);
        else
            if strendswith(dirdata(i).name,'.dcm')
                
                filename = strcat(nm_path,dirdata(i).name);

                try
                    qq = dicominfo(filename);

                    fileID = fopen(strcat(filename,'.inf'),'w');

                    StudyDate = '?';
                    ContentTime = '?';
                    UltrasoundColorDataPresent = '?';
                    FamilyName = '?';

                    if isfield(qq,'StudyDate')
                        StudyDate = qq.StudyDate;
                    end

                    if isfield(qq,'ContentTime')
                        ContentTime = qq.ContentTime;
                    else
                        if isfield(qq,'StudyTime')
                            ContentTime = qq.StudyTime;
                        end
                    end

                    if isfield(qq,'UltrasoundColorDataPresent')
                        UltrasoundColorDataPresent = num2str(qq.UltrasoundColorDataPresent);
                    end

                    if isfield(qq,'PatientName') && isfield(qq.PatientName,'FamilyName')
                        FamilyName = qq.PatientName.FamilyName;
                    end

                    fprintf(fileID,'%s\r\n',StudyDate);
                    fprintf(fileID,'%s\r\n',ContentTime);
                    fprintf(fileID,'%s\r\n',UltrasoundColorDataPresent);
                    fprintf(fileID,'%s\r\n',FamilyName);

                    fclose(fileID);

                    file_notes = strcat(StudyDate,'.',FamilyName,'.',ContentTime,'.',UltrasoundColorDataPresent);
                catch err
                    file_notes = err.message;
                end
            end
        end
        
        total_size = total_size+dirdata(i).bytes;
        
        if isempty(filenames)
            filenames = {dirdata(i).name};
            filesizes = {dirdata(i).bytes/1024/1024};
            notes = {file_notes};
        else
            filenames = [filenames;{dirdata(i).name}];
            filesizes = [filesizes;{dirdata(i).bytes/1024/1024}];
            notes = [notes;{file_notes}];
        end
    end
end

set(handles.uitable1,'Data',[filenames filesizes notes]);

set(handles.uitable1,'ColumnWidth',{120 90 240})

if total_size>0
    set(handles.infoedit,'String',strcat(num2str(total_size/1024/1024),' MB in this directory'));
else
    set(handles.infoedit,'String',strcat(num2str(total_folders),' folders in this directory'));
end
drawnow;

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Browse to given target
function browseTo(target)
global handles nm_file nm_path fullname Ufname

nm_file = target;

if strendswith(nm_file,':')
    x = pwd;
    cd(target);
    nm_path = appendFileSep(pwd);
    cd(x);
    loadCurrentDirectoryContents(handles);
    return;
end

if (strcmp(nm_file,'..'))
    x = pwd;
    cd(nm_path);
    cd('..');
    nm_path = appendFileSep(pwd);
    cd(x);
    loadCurrentDirectoryContents(handles);
    return;
end

if (not(isempty(strfind(nm_file,filesep))))
    x = pwd;
    cd(nm_path);
    cd(nm_file);
    nm_path = appendFileSep(pwd);
    cd(x);
    loadCurrentDirectoryContents(handles);
    return;
end

fullname = strcat(nm_path, nm_file);
Ufname = upper(nm_file);

if (not(isempty(strfind(fullname,'.mtf'))))
    prev_dir = pwd;
    cd(nm_path);
end

selectFile(handles,1);    

if (not(isempty(strfind(fullname,'.mtf'))))
    cd(prev_dir);
end


function uitable1_CellSelectionCallback(hObject, eventdata, handles)

set(hObject, 'UserData', eventdata.Indices);

if (length(eventdata.Indices)<1) 
    return; 
end

sel = eventdata.Indices(1);

data = get(hObject,'Data');

if size(data,1)<sel
    return;
end

target = data{sel,1}

browseTo(target);


function editPath_Callback(hObject, eventdata, handles)
% hObject    handle to editPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editPath as text
%        str2double(get(hObject,'String')) returns contents of editPath as a double


% --- Executes during object creation, after setting all properties.
function editPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox19.
function checkbox19_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox19
show_displacement_field = get(hObject,'Value');
if show_displacement_field
    set(handles.checkbox26,'Enable','on'); % incremental checkbox
else
    set(handles.checkbox26,'Enable','off');
end

function displayInfo(str,draw_now)
% Display str in infoedit control
global handles
set(handles.infoedit,'String',str);

if nargin<2
    draw_now=0;
end

if draw_now
    drawnow;
end

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global nm_path
nm_path = get(handles.editPath,'String');

try
    x = pwd;
    cd(nm_path);
    nm_path = [pwd filesep];
    cd(x);
catch err
    displayInfo(err.message);
end

loadCurrentDirectoryContents(handles);


% --- Executes on slider movement.
function RangeSlider_Callback(hObject, eventdata, handles)
global block_size

block_size = double(int16(get(hObject,'Value')));

str = sprintf('Range (%d pix)',block_size);

set(handles.RangeText,'String',str);

block_size=block_size*2+1

% --- Executes during object creation, after setting all properties.
function RangeSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RangeSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function PThresholdslider_Callback(hObject, eventdata, handles)
global PThreshold

PThreshold = get(hObject,'Value');

str = sprintf('P threshold (%.1f%%)',PThreshold*100);

set(handles.PThresholdtext,'String',str);

% --- Executes during object creation, after setting all properties.
function PThresholdslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PThresholdslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function pfilterslider_Callback(hObject, eventdata, handles)
global pfilter

pfilter = get(hObject,'Value');

str = sprintf('P h/p filter (%.1f)',pfilter);

set(handles.pfiltertext,'String',str);

% --- Executes during object creation, after setting all properties.
function pfilterslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pfilterslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function prefilterslider_Callback(hObject, eventdata, handles)
global prefilter

prefilter = int16(get(hObject,'Value'));

str = sprintf('Prefilt ref size (%d px)',prefilter);

set(handles.prefiltertext,'String',str);

% --- Executes during object creation, after setting all properties.
function prefilterslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to prefilterslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
track_GO(handles);


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.01);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.02);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.03);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.04);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.05);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.06);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
set(handles.ThresholdSlider,'Value',0.07);
ThresholdSlider_Callback(handles.ThresholdSlider, [], handles);


% Catalog Files
function pushbutton17_Callback(hObject, eventdata, handles)
global nm_path

if isempty(nm_path) || not(ischar(nm_path))
    nm_path = [pwd filesep];
end
   
set(handles.editPath,'String',nm_path);

choice = questdlg(['Catalog files in ',nm_path,'?'], 'Cataloger', 'Continue', 'Cancel', 'Cancel');

if ~strcmp(choice,'Continue')
    return
end

dirdata = dir([nm_path '*.inf']);

for i=1:size(dirdata,1)
    infname = dirdata(i).name;
    inflessname = strrep(infname,'.inf','');
    
    set(handles.infoedit,'String',['Cataloging ',inflessname,'...']);
    drawnow;

    fileID = fopen(strcat(nm_path,infname),'r');
    if (fileID>=0)
        StudyDate=fgets(fileID);
        fgets(fileID); % ContentTime
        fgets(fileID); % UltrasoundColorDataPresent
        FamilyName=fgets(fileID);
        if (FamilyName==-1) 
            FamilyName='?';
        end
        fclose(fileID);
        dest_path = strcat(nm_path,'__catalog',filesep,StudyDate,'.',FamilyName);
        mkdir(dest_path);
        copyfile(strcat(nm_path,inflessname),dest_path);
        copyfile(strcat(nm_path,infname),dest_path);
    end
end

loadCurrentDirectoryContents(handles);

set(handles.infoedit,'String','Completed.');
drawnow;

% --- Executes on button press in checkbox21.
function checkbox21_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox21



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
path = get(handles.editPath,'String');

dos(['explorer ' '"' path '" &']);

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double
global frame_limiter_num
frame_limiter_num = str2double(get(hObject,'String'));

global DICOMfilename
clear DICOMfilename

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
global frame_limiter_offset
frame_limiter_offset = str2double(get(hObject,'String'));

global DICOMfilename
clear DICOMfilename

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fullname

dos(['"' fullname '" &']);


function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton20_Callback(hObject, eventdata, handles)
% Let user pick Cropper region
global whole_matrix_dimensions

if ~whole_matrix_dimensions

    str1 = sprintf('Please choose an ultrasound image file first\n\n');

    set(handles.infoedit,'String',str1);
    
    return
end

str1 = sprintf('Selecting a Cropper region\n\n');

str2 = sprintf('Choose your region by drawing a rectangular area over the ultrasound image\n\n');

set(handles.infoedit,'String',[str1 str2]);

VD_DisplayOriginal;

% get the new Region of Interest
waitforbuttonpress;
pt_first = double(int16(get(gca,'CurrentPoint')));
rbbox;
pt_other = double(int16(get(gca,'CurrentPoint')));

% convert the points to their 2D representations
pt_first = pt_first(1,1:2);
pt_other = pt_other(1,1:2);

pos_interest = min(pt_first,pt_other);
dim_interest = abs(pt_first-pt_other);

% eliminate selections outside the range
unity_vector_a = ones(1,2);
limit_vector_a = [whole_matrix_dimensions(2) whole_matrix_dimensions(1)];

pos_interest = max(pos_interest, unity_vector_a);
pos_interest = min(pos_interest, limit_vector_a);

range_vector_a = limit_vector_a - pos_interest;
dim_interest = min(dim_interest, range_vector_a);
dim_interest = max(dim_interest, unity_vector_a);

if (min(dim_interest(1),dim_interest(2)) < 30)
    
    str1 = sprintf('The selected region of interest is too small.\n\n');
    
    set(handles.infoedit,'String',str1);
    
    return
end

set(handles.edit11,'String',num2str(pos_interest(1)));

set(handles.edit12,'String',num2str(pos_interest(2)));

set(handles.edit13,'String',num2str(pos_interest(1)+dim_interest(1)-1));

set(handles.edit14,'String',num2str(pos_interest(2)+dim_interest(2)-1));

str1 = sprintf('Cropper region has been set\n\n');

str2 = sprintf('Reload image to realize the changes\n\n');

set(handles.infoedit,'String',[str1 str2]);

function pushbutton21_Callback(hObject, eventdata, handles)
% reset Cropper region
set(handles.edit11,'String','0');

set(handles.edit12,'String','0');

set(handles.edit13,'String','0');

set(handles.edit14,'String','0');


% --- Executes on slider movement.
function slider11_Callback(hObject, eventdata, handles)
% hObject    handle to slider11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
frm = round(get(hObject,'Value'));

selectFile(handles,frm);


% --- Executes during object creation, after setting all properties.
function slider11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in checkbox23.
function checkbox23_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox23


% --- Executes on button press in checkbox24.
function checkbox24_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox24


% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global frame_limiter_num
global frame_limiter_offset
frame_limiter_num=0;
frame_limiter_offset=0;
set(handles.edit9,'String','0');
set(handles.edit10,'String','0');


% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)

currentIndices = get(handles.uitable1, 'UserData');

if (numel(currentIndices)<1) 
    return; 
end

sel = currentIndices(1);

data = get(handles.uitable1,'Data');

if (size(data,1)<sel) 
    return; 
end

target = data{sel,1}

browseTo(target);


% --- Executes on button press in checkbox25.
function checkbox25_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox25


% Convert to DICOM any .jpg or .tif or .avi files in path
function pushbutton24_Callback(hObject, eventdata, handles)
global nm_path

if isempty(nm_path) || not(ischar(nm_path))
    nm_path = [pwd filesep];
end
   
set(handles.editPath,'String',nm_path);

choice = questdlg(['Convert to DICOM all .jpg, .tif, and .avi files in ',nm_path,'?'], 'Converter', 'Continue', 'Cancel', 'Cancel');

if ~strcmp(choice,'Continue')
    return
end

dirdata = dir([nm_path '*.*']);

for i=1:size(dirdata,1)
    if dirdata(i).isdir==1 
        continue
    end
    
    filename = dirdata(i).name;
    
    if ~strendswith(filename,'.tif') && ~strendswith(filename,'.jpg') && ~strendswith(filename,'.avi')
        continue
    end
    
    set(handles.infoedit,'String',['Converting ',filename,'...']);
    drawnow;

    mkdir(strcat(nm_path,'DICOMconv'));

    if strendswith(filename,'.avi')
        mov=aviread(strcat(nm_path,filename)); %#ok<FREMO>
        num_fi = numel(mov);
        height = size(mov(1).cdata,1);
        width = size(mov(1).cdata,2);
        cropp = 0.10; % 10%
        y1 = round(height*cropp);
        y2 = round(height*(1-cropp));
        x1 = round(width*cropp);
        x2 = round(width*(1-cropp));
        height = y2-y1+1;
        width = x2-x1+1;

        for fi=1:num_fi
            set(handles.infoedit,'String',num2str(fi));
            drawnow;
            im_frame=mov(fi).cdata(y1:y2,x1:x2);
            save([nm_path 'DICOMconv' filesep 'im_frame' num2str(fi)],'im_frame');
        end
        clear mov
        
        img=repmat(uint8(0),[height width 1 num_fi]);
        for fi=1:num_fi
            set(handles.infoedit,'String',num2str(fi));
            drawnow;
            load([nm_path 'DICOMconv' filesep 'im_frame' num2str(fi)]);
            delete([nm_path 'DICOMconv' filesep 'im_frame' num2str(fi) '.mat']);
            img(:,:,1,fi)=im_frame;
        end
        
        dicomwrite(img,[nm_path 'DICOMconv' filesep filename '.dcm'],'CompressionMode','JPEG lossy',...
            'StudyDate','19800101','UltrasoundColorDataPresent','0');
    else
        img=imread(strcat(nm_path,filename));
        
        dicomwrite(img,[nm_path 'DICOMconv' filesep filename '.dcm'],'CompressionMode','None',...
            'StudyDate','19800101','UltrasoundColorDataPresent','0');
    end
end

loadCurrentDirectoryContents(handles);

set(handles.infoedit,'String','Completed.');
drawnow;


% --- Executes on button press in checkbox26.
function checkbox26_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox26



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.edit15,'String','80');
set(handles.edit16,'String','8');


% --------------------------------------------------------------------
function Options_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Options_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function option_menu_DICOM_option_Callback(hObject, eventdata, handles)
% hObject    handle to option_menu_DICOM_option (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Options_Callback(hObject, eventdata, handles)
% hObject    handle to Options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8
