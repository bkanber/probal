function frame_data = readavi(filename, frame_no)
global handles
global video
global fileinfo
global frame_limiter_num
global frame_limiter_offset

clear frame_data;

if isempty(frame_limiter_num)
    frame_limiter_num = 0;
end

if isempty(frame_limiter_offset)
    frame_limiter_offset = 0;
end

if frame_no==1
    %clear fileinfo;
    video = VideoReader(filename);
end

fileinfo.FrameRate = video.FrameRate;
fileinfo.NumberOfFrames = video.NumberOfFrames;

if frame_limiter_num~=0
    fileinfo.NumberOfFrames = min(fileinfo.NumberOfFrames,frame_limiter_num);
end

if frame_limiter_num==0 && frame_limiter_offset~=0
    fileinfo.NumberOfFrames = fileinfo.NumberOfFrames-frame_limiter_offset;
end

video.VideoFormat % The following is for RGB24

frame_data = read(video,frame_no+frame_limiter_offset);
frame_data = rgb2hsv(frame_data);
frame_data = min(255,frame_data(:,:,3)*255);

frame_data = frame_data-min(min(frame_data));

frame_data = frame_data/max(max(frame_data));

frame_data = frame_data*255;

x1 = str2double(get(handles.edit11,'String'));
y1 = str2double(get(handles.edit12,'String'));
x2 = str2double(get(handles.edit13,'String'));
y2 = str2double(get(handles.edit14,'String'));

if x1>=1 && x1<=size(frame_data,2) && x2>=1 && x2<=size(frame_data,2) && x2>=x1 && ...
   y1>=1 && y1<=size(frame_data,1) && y2>=1 && y2<=size(frame_data,1) && y2>=y1
    
    frame_data = frame_data(y1:y2,x1:x2);
end

if true % no good for GSM
    frame_data = frame_data-min(min(frame_data));

    frame_data = frame_data/max(max(frame_data));

    frame_data = frame_data*255;
end

if false % no good for GSM
    frame_data = double(histeq(uint8(frame_data)));
end

if ~isfield(fileinfo,'ppmmx') || ~isfield(fileinfo,'ppmmy')
    prompt = {'Enter x resolution in pixels per mm','Enter y resolution in pixels per mm'};
    name = 'Enter resolution information';
    numlines=1;
    
    global def_ppmmx;
    global def_ppmmy;
    
    if isempty(def_ppmmx) 
        def_ppmmx = '';
    end
    
    if isempty(def_ppmmy) 
        def_ppmmy = '';
    end
    
    if isfield(fileinfo,'ppmmx')
        def_ppmmx = num2str(fileinfo.ppmmx);
    end
    
    if isfield(fileinfo,'ppmmy')
        def_ppmmy = num2str(fileinfo.ppmmy);
    end
    
    defaultanswer={def_ppmmx,def_ppmmy};

    options.Resize='on';
    options.WindowStyle='modal';
    options.Interpreter='tex';
    
    answer=inputdlg(prompt,name,numlines,defaultanswer,options);
    
    if ~isempty(answer)
        fileinfo.ppmmx = str2num(answer{1});
        fileinfo.ppmmy = str2num(answer{2});
        def_ppmmx = answer{1};
        def_ppmmy = answer{2};
    end
end

%frame_data=flipud(frame_data);

%frame_data=frame_data(50:400,100:550,:,:);

%frame_data=frame_data(50:400,150:500,:,:);
%frame_data=frame_data(150:350,200:550,:,:);
%frame_data(80:200,1:100)=0;

% obj=mmreader(filename);
% 
% get(obj, 'Height')
% get(obj, 'BitsPerPixel')
% get(obj, 'Duration')
% get(obj, 'VideoFormat')
% get(obj, 'Width')
% 
% fileinfo.FrameRate=get(obj, 'FrameRate');
% fileinfo.NumberOfFrames=get(obj, 'NumberOfFrames');
% 
% frame_data=read(obj,frame_no);
% frame_data=rgb2hsv(frame_data);
% %frame_data=min(255,100+frame_data(:,:,3)*300);
% frame_data=min(255,frame_data(:,:,3)*255);
% 
% %frame_data=frame_data(110:580,140:690);
% %frame_data=255-frame_data;
% 
% disp(['NumberOfFrames=' num2str(fileinfo.NumberOfFrames)]);
% disp(['FrameRate=' num2str(fileinfo.FrameRate)]);
