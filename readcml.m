function [cmlinfo, frame_data] = readcml(handles, filename, frame_no)
clear cmlinfo;
clear frame_data;

filename=filename(1:(find(filename=='.')-1));
structxml = xml_load([filename, '.cml']);

cmlinfo.Ultrafast = str2num(structxml.TWODECHO.SUB_MODE)==0;
cmlinfo.BufferRF = strcmp(structxml.DATA_TYPE,'Raw')&&(~cmlinfo.Ultrafast);
cmlinfo.LinePitch = str2num(structxml.TWODECHO.POPE.RECON_INFO.LINE_PITCH);
cmlinfo.NbLines = str2num(structxml.TWODECHO.POPE.RECON_INFO.NB_LINES);
cmlinfo.PitchPix = str2num(structxml.TWODECHO.POPE.RECON_INFO.PITCH_PIX);
cmlinfo.TxFreq = str2num(structxml.TWODECHO.TX_INFO.TX_FREQ);
cmlinfo.NbPiezos = str2num(structxml.PROBE.IMAGING_ARRAY.NB_PIEZO);
cmlinfo.PiezoPitch = str2num(structxml.PROBE.IMAGING_ARRAY.PIEZO_PITCH);
cmlinfo.c0 = str2num(structxml.USER.GLOBAL.SOUND_SPEED); %m/sMemoryLimitBytes

if (cmlinfo.Ultrafast)  
    cmlinfo.Nt =  str2num(structxml.TWODECHO.FIRINGS);    
    cmlinfo.Nj = cmlinfo.NbPiezos/cmlinfo.LinePitch;  
    cmlinfo.Ni = str2num(structxml.TWODECHO.POPE.RECON_INFO.NB_PIX_LINE);
    cmlinfo.TypeData = 'float32';
else
    if cmlinfo.BufferRF
        cmlinfo.Ni = str2num(structxml.TWODECHO.POPE.ACQ_INFO.RF_SAMPLE_NUMBER);
        cmlinfo.Nj = 2*str2num(structxml.TWODECHO.DEFAULT_RF_CHANNEL_NUMBER);
        cmlinfo.Nt = str2num(structxml.TWODECHO.FIRINGS);
        cmlinfo.TypeData = 'int16';
    end;
    if ~cmlinfo.BufferRF
        cmlinfo.Nt = 1;
        cmlinfo.Nj = cmlinfo.NbPiezos/cmlinfo.LinePitch;
        cmlinfo.Ni = str2num(structxml.TWODECHO.POPE.RECON_INFO.NB_PIX_LINE);
        cmlinfo.TypeData = 'float32';
    end;
end;

% cmlinfo.Nt = min(60,cmlinfo.Nt);

cmlinfo.f0 =  cmlinfo.TxFreq; %MHz
cmlinfo.lambda = cmlinfo.c0/1000/cmlinfo.f0; %mm
cmlinfo.Deltax = cmlinfo.PiezoPitch * cmlinfo.LinePitch %*2; %mm %0.2 LF
cmlinfo.Deltaz = cmlinfo.lambda *  cmlinfo.PitchPix; %mm  %0.1369 LF

cmlinfo.PRF = str2num(structxml.TWODECHO.PRF);

cmlinfo.MyFileName = [filename, '.cml'];

cmlinfo.FileOpened = 1;
cmlinfo.MyStart = 1;
cmlinfo.MySize = 3000;

if (cmlinfo.Ultrafast)
    cmlinfo.nn1 = 8;
else
    if(cmlinfo.BufferRF)
       cmlinfo.nn1=2;
    else
        cmlinfo.nn1=8;
    end;
end;

fid = fopen([filename, '.data']);
fseek(fid, cmlinfo.Ni * cmlinfo.Nj * (frame_no-1) * cmlinfo.nn1, 'bof');           
data = fread(fid,cast(cmlinfo.Ni * cmlinfo.Nj * cmlinfo.nn1,'double'),cmlinfo.TypeData);
fclose(fid);

I = reshape(data(1:2:(cmlinfo.Ni*cmlinfo.Nj*2)),[cmlinfo.Ni,cmlinfo.Nj]);
Q = reshape(data(2:2:(cmlinfo.Ni*cmlinfo.Nj*2)),[cmlinfo.Ni,cmlinfo.Nj]);
IQ = I + sqrt(-1) * Q;
cmlinfo.IQ = IQ;

cmlinfo.G0 = 85; %dB Noise floor for B mode
cmlinfo.Compression = 44; %dB

%env_matrix = (20*log10(abs(IQ))-cmlinfo.G0)/cmlinfo.Compression;
%env_matrix = log10(abs(IQ));
%figure;imagesc(env_matrix,[min(min(env_matrix)),max(max(env_matrix))]);colormap(gray);

if 1==0
    %X = zeros(1,size(IQ,1)*2);
    %X(1:2:end-1)=I(:,100);
    %X(2:2:end)=Q(:,100);
    %Aline_no = 100;
    %X = IQ(:,Aline_no);
    X = reshape(IQ(300,:),size(IQ,2),1);
    N = length(X);
    Y = abs(fft(X))/N;
    cmlinfo.sampling_frequency = cmlinfo.f0*1E6*4;
    f = cmlinfo.sampling_frequency/2*linspace(-1,1,N)
    %total_power = sum(power_spectrum)
    sp_rows = 3;
    sp_columns = 4;

    figure('Name',sprintf('Frame %d',frame_no));
    for y=50:50:350
        X = reshape(IQ(y,:),size(IQ,2),1);
        N = length(X);
        Y = abs(fft(X))/N;
        Y = [(Y(N/2+1:end));Y(1:N/2)];
        power_spectrum = Y.^2;
        subplot(4,4,(y)/50);plot(f,power_spectrum);title(sprintf('power_spectrum y=%f',y));axis tight;
    end

    figure('Name',sprintf('Frame %d',frame_no));
    subplot(sp_rows,sp_columns,1);plot(real(X));title('real(X)');axis tight;
    subplot(sp_rows,sp_columns,2);plot(imag(X));title('imag(X)');axis tight;
    subplot(sp_rows,sp_columns,5);plot(abs(X));title('abs(X)');axis tight;
    subplot(sp_rows,sp_columns,6);plot(log10(abs(X)));title('log10(abs(X))');axis tight;
    subplot(sp_rows,sp_columns,9);plot(angle(X));title('phase of X');axis tight;
    Y = [(Y(N/2+1:end));Y(1:N/2)];
    power_spectrum = Y.^2;
    subplot(sp_rows,sp_columns,10);plot(f,power_spectrum);title('power_spectrum');axis tight;

    phase_matrix = angle(IQ);
    subplot(sp_rows,sp_columns,11);imagesc(phase_matrix,[-pi,+pi]);colormap(jet);title('angle(IQ)');
    %

    frame_data = max(0.0,min(1.0,(20*log10(abs(IQ)) - cmlinfo.G0)/cmlinfo.Compression));
    frame_data = 255 * frame_data;
    %X = frame_data(:,Aline_no);
    X = frame_data(300,:);
    subplot(sp_rows,sp_columns,12);plot(X);title('abs(X_frame_data)');axis tight;

    %
    figure;
    frame_data = max(0.0,min(1.0,(20*log10(abs(real(IQ))) - cmlinfo.G0)/cmlinfo.Compression));
    frame_data = 255 * frame_data;
    subplot(2,2,1);imagesc(frame_data);colormap(gray);title('real(IQ)');

    frame_data = max(0.0,min(1.0,(20*log10(abs(imag(IQ))) - cmlinfo.G0)/cmlinfo.Compression));
    frame_data = 255 * frame_data;
    subplot(2,2,2);imagesc(frame_data);colormap(gray);title('imag(IQ)');

    frame_data = max(0.0,min(1.0,(20*log10(abs((imag(IQ)+real(IQ))/2)) - cmlinfo.G0)/cmlinfo.Compression));
    frame_data = 255 * frame_data;
    subplot(2,2,3);imagesc(frame_data);colormap(gray);title('(imag(IQ)+real(IQ))/2');
    %

    frame_data = max(0.0,min(1.0,(20*log10(abs(IQ)) - cmlinfo.G0)/cmlinfo.Compression));
    frame_data = 255 * frame_data;

    %frame_data = log10(abs(IQ));
    %XX%frame_data = angle(IQ);
    %XX%frame_data = (frame_data-min(min(frame_data)))/(max(max(frame_data))-min(min(frame_data)));
    %XX%frame_data = frame_data*255;

    %
    subplot(2,2,4);imagesc(frame_data);colormap(gray);title('abs(IQ)');
    line([1 size(frame_data,2)],[300,300],'Color','green');

    %
else
    if (1==0)
        % theta = atan2(real(IQ),imag(IQ));
        % A = real(IQ) ./ sin(theta);
        % A2 = A*exp(-0.18*11.25*1/4*(154000/11.25E6)/8.7);
        % gamma = abs(asin(imag(IQ) ./ A2));
        % gamma = gamma - theta - pi/4;
        % %figure;imagesc(gamma,[min(min(gamma)),max(max(gamma))]);
        % frame_data = gamma; 

        frame_data = log10(abs(IQ));
        %XX%frame_data = angle(IQ);
        frame_data = (frame_data-min(min(frame_data)))/(max(max(frame_data))-min(min(frame_data)));
        frame_data = frame_data*255;
        % ind = find(isnan(frame_data)==true);
        % if (~isempty(ind)) frame_data(ind) = 0; end
    end
end

% cmlinfo.frame_data_I = max(0.0,min(1.0,(20*log10(abs(I)) - cmlinfo.G0)/cmlinfo.Compression));
% cmlinfo.frame_data_I = 1+255 * cmlinfo.frame_data_I;
% cmlinfo.frame_data_I = imresize(cmlinfo.frame_data_I,[cmlinfo.Ni*cmlinfo.Deltaz*10,cmlinfo.Nj*cmlinfo.Deltax*10]);
%figure;imagesc(frame_data);colormap(gray);title('I');

% cmlinfo.frame_data_Q = max(0.0,min(1.0,(20*log10(abs(Q)) - cmlinfo.G0)/cmlinfo.Compression));
% cmlinfo.frame_data_Q = 1+255 * cmlinfo.frame_data_Q;
% cmlinfo.frame_data_Q = imresize(cmlinfo.frame_data_Q,[cmlinfo.Ni*cmlinfo.Deltaz*10,cmlinfo.Nj*cmlinfo.Deltax*10]);
%figure;imagesc(frame_data);colormap(gray);title('Q');

if (get(handles.checkboxPhase,'Value')==1)
    frame_data = max(0.0,min(1.0,(angle(IQ)+pi)/(2*pi)));
else
    if (get(handles.checkboxABSOLUTEMEAN,'Value')==1)
        frame_data = max(0.0,min(1.0,(20*log10(abs((I+Q)/2)) - cmlinfo.G0)/cmlinfo.Compression));
    else
        if (get(handles.checkboxABSI,'Value')==1)
            frame_data = max(0.0,min(1.0,(20*log10(abs(I)) - cmlinfo.G0)/cmlinfo.Compression));
        else
            if (get(handles.checkboxABSQ,'Value')==1)
                frame_data = max(0.0,min(1.0,(20*log10(abs(Q)) - cmlinfo.G0)/cmlinfo.Compression));
            else
                frame_data = max(0.0,min(1.0,(20*log10(abs(IQ)) - cmlinfo.G0)/cmlinfo.Compression));
            end
        end
    end
end

frame_data = uint8(255 * frame_data);
frame_data = imresize(frame_data,[cmlinfo.Ni*cmlinfo.Deltaz*10,cmlinfo.Nj*cmlinfo.Deltax*10]);

%imwrite(frame_data,'export.bmp','bmp');

% frame_data = histeq(frame_data);
frame_data = double(frame_data);

%figure;imagesc(frame_data);colormap(gray);

%cmlinfo.frame_data_I = frame_data;
%cmlinfo.frame_data_Q = frame_data;
%figure;imagesc(frame_data);colormap(gray);title('IQ');

% Qscaler = mean(mean(cmlinfo.frame_data_I))/mean(mean(cmlinfo.frame_data_Q));
% 
% dres_frame_data = zeros(size(frame_data,1)*2,size(frame_data,2));
% dres_frame_data(1:2:end-1,:) = cmlinfo.frame_data_I;
% dres_frame_data(2:2:end,:) = cmlinfo.frame_data_Q*Qscaler;

%frame_data = dres_frame_data;
%cmlinfo.frame_data_I = frame_data;
%cmlinfo.frame_data_Q = frame_data;

cmlinfo.ppmmx = size(frame_data,2)/(cmlinfo.Nj*cmlinfo.Deltax);
cmlinfo.ppmmy = size(frame_data,1)/(cmlinfo.Ni*cmlinfo.Deltaz);

cmlinfo.Ni = size(frame_data,1);
cmlinfo.Nj = size(frame_data,2);

return
