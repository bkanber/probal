function p = phase(C) % Phase p (0..2xpi) of complex number C
    p = angle(C);
    ind = find(p<0);
    if (~isempty(ind))
        p(ind) = 2*pi+p(ind);
    end
end