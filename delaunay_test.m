% Create a set of points representing the point cloud
%x=[1 5 7 9]';
%y=[2 4 9 3]';
figure;plot(x,y);
numpts=length(x);

% % Create a set of points representing the point cloud
% numpts=192;
% t = linspace( -pi, pi, numpts+1 )';
% t(end) = [];
% r = 0.1 + 5*sqrt( cos( 6*t ).^2 + (0.7).^2 );
% x = r.*cos(t);
% y = r.*sin(t);
% ri = randperm(numpts);
% x = x(ri);
% y = y(ri);

% Construct a Delaunay Triangulation of the point set.
dt = DelaunayTri(x,y);
tri = dt(:,:);

% Insert the location of the Voronoi vertices into the existing
% triangulation
V = dt.voronoiDiagram();
% Remove the infinite vertex
V(1,:) = [];
numv = size(V,1);
dt.X(end+(1:numv),:) = V;

% The Delaunay edges that connect pairs of sample points represent the
% boundary.
delEdges = dt.edges();
validx = delEdges(:,1) <= numpts;
validy = delEdges(:,2) <= numpts;
boundaryEdges = delEdges((validx & validy), :)';
xb = x(boundaryEdges);
yb = y(boundaryEdges);
%figure;
%triplot(tri,x,y);
axis equal;
hold on;
%plot(x,y,'*r');
plot(xb,yb, '-r');
xlabel('Curve reconstruction from a point cloud', 'fontweight','b');
hold off;
