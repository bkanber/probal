function m = medianN(X)
m = median(reshape(X,1,numel(X)));
