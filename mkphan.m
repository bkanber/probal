fhd = fopen('phantom.mtf','wt');

d=zeros(1,50);

for frm=0:49
    frm

    a = zeros(400,100);
    
    r = round(20+80*sin((frm/25)*pi)^2);
    
    a(200-r-10:200-r-1,:)=255;
    a(200+r:200+r+10,:)=255;
    
    d(frm+1) = 2*r;
    
    imshow(a,[0 255]);
    drawnow;
    
    imwrite(a,sprintf('frame%d.tif',frm),'tif');
    fprintf(fhd,'frame%d.tif\n',frm);
    

end

fclose(fhd);

figure;
plot(d);
