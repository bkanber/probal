% =============================================================
% readxif.m - reads an HDI lab .xif (cineloop) file
%
% Note
% This is a preliminary version
%
% Usage
% Information is returned in structure 'info'
% The specified frame is returned in 'this_frame'
% 'info.result' indicates whether function was successfull
%
% Example
% [info,this_frame] = readxif('myfile.xif',1,[]);
% if (not(info.result))
%   disp('error opening myfile.xif');
% else
%   for i=1:info.num_frames
%     [info,this_frame] = readxif('myfile.xif',i,info);
%     display(this_frame);
%   end
% end
%
% Copyright (c) 2003 Department of Medical Physics
%
% July 03 (Bar?? Kanber) - Preliminary version
% April 11 (Baris Kanber) - Performance upgrade
% =============================================================

function [info,this_frame] = readxif(filename,frame_no,info)
global fileinfo
global frame_limiter_num
global frame_limiter_offset

if isempty(frame_limiter_num)
    frame_limiter_num = 0;
end

if isempty(frame_limiter_offset)
    frame_limiter_offset = 0;
end

this_frame = [];

if (isempty(info))
    % retrieve the header and fill the 'info' structure
    info.result = 0;
    str0 = '';
    
    [fhd,info.syserrmsg] = fopen(filename,'rb');

    if (fhd==-1)
        info.errmsg = 'Error opening the .xif file';
        return;
    end

    while(1)
        str1 = fgets(fhd);

        if (str1==-1)
            info.syserrmsg = ferror(fhd);
            fclose(fhd);
            info.errmsg = 'The format of this .xif file has not been recognised';
            return;
        end

        if not(isempty(strfind(str1,'EOH')))
            break;
        end
        
        str0 = strcat(str0,str1);
    end

    info.fpos = ftell(fhd);

    fclose(fhd);
   
	info.width = GetToken(str0, 'echoNumLines', 'int');
	info.height = GetToken(str0, 'echoNumDisplaySamples', 'int');
	info.num_frames = GetToken(str0, 'numFrames', 'int');
	info.echoFrameSize = GetToken(str0, 'echoFrameSize', 'int');
	info.colorFrameSize = GetToken(str0, 'colorFrameSize', 'int');
	info.frameRate = GetToken(str0, 'frameRate', 'int');
	
	info.echoBounds_depthFirstSample = GetToken(str0, 'echoBounds_depthFirstSample', 'double');
	info.echoDepth_startDepth = GetToken(str0, 'echoDepth_startDepth', 'double');
	info.echoDepth_stopDepth = GetToken(str0, 'echoDepth_stopDepth', 'double');
	info.echoDepth_displayStartDepth = GetToken(str0, 'echoDepth_displayStartDepth', 'double');
	info.echoDepth_displayStopDepth = GetToken(str0, 'echoDepth_displayStopDepth', 'double');
	info.echoScan_linearWidth = GetToken(str0, 'echoScan_linearWidth', 'double')
	
	info.institution = GetToken(str0, 'institution', 'string');
	info.date = GetToken(str0, 'date', 'string');
	info.time = GetToken(str0, 'time', 'string');
	
	info.lastname = GetBracketToken(str0, 'last', 'string');
	info.firstname = GetBracketToken(str0, 'first', 'string');
	
	info.patient_id = GetBracketToken(str0, 'codeMeaning="Id"', 'string');
	
	if (not(isempty(info.colorFrameSize)))
        disp('Note: this .xif file has colour information');
	end
	
	info.fullFrameSize = info.echoFrameSize;
	
	if (not(isempty(info.colorFrameSize)))
        info.fullFrameSize = info.fullFrameSize + info.colorFrameSize;
	end

	info.heightmm = info.echoDepth_displayStopDepth-info.echoDepth_displayStartDepth;
  
    info.widthmm = info.echoScan_linearWidth;
  
    if (info.heightmm)
        info.mmaspectratio = info.widthmm/info.heightmm;
    else
        info.mmaspectratio = NaN;
    end
    
    if frame_limiter_num~=0
        info.num_frames = min(info.num_frames,frame_limiter_num);
    end

    if frame_limiter_num==0 && frame_limiter_offset~=0
        info.num_frames = info.num_frames-frame_limiter_offset;
    end
   
end

info.result = 0;

b = isempty(info.width);
b = b | isempty(info.height);
b = b | isempty(info.num_frames);
b = b | isempty(info.echoFrameSize);

if (b)
    info.errmsg = 'The format of this .xif file has not been recognised';
    return;
end

b = (info.num_frames < 1);

if (b)
    info.errmsg = 'The format of this .xif file is not supported';
    return;
end

[fhd,info.syserrmsg] = fopen(filename,'rb');

if (fhd==-1)
    info.errmsg = 'Error opening the .xif file';
    return;
end

if (fseek(fhd,info.fpos+(frame_no+frame_limiter_offset-1)*info.fullFrameSize,'bof')==-1)
    info.syserrmsg = ferror(fhd);
    fclose(fhd);
    info.errmsg = 'Error reading the .xif file';
    return;
end

[this_frame,count] = fread(fhd,[info.height,info.width],'uint8');

if (not(count==info.echoFrameSize))
    info.syserrmsg = ferror(fhd);
    fclose(fhd);
    info.errmsg = 'Error reading the .xif file';
    return;
end

info.consimwidth = 512;    
info.inconsimheight = round(info.consimwidth/info.mmaspectratio);
this_frame = imresize(this_frame,[info.inconsimheight,info.consimwidth],'bilinear');

this_frame = double(imnoise(uint8(this_frame),'gaussian',0,0.15));

%this_frame=min(255,this_frame/150*255); % exeggarate contrast

% this_frame = 255-this_frame;
% this_frame = this_frame-min(min(this_frame));
% this_frame = this_frame*255/max(max(this_frame));
% %this_frame = this_frame*1.5;

% custom scaling
if 0
    scale_f = 0.5;

    this_frame = imresize(this_frame,scale_f,'nearest');

    %fileinfo.ppmmx = fileinfo.ppmmx*scale_f;
    %fileinfo.ppmmy = fileinfo.ppmmy*scale_f;
end

% info.num_frames=2;

fileinfo.NumberOfFrames=info.num_frames;
fileinfo.FrameRate=info.frameRate;
fileinfo.ppmmx=info.consimwidth/info.widthmm;
fileinfo.ppmmy=info.inconsimheight/info.heightmm;

info.result = 1;

fclose(fhd);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function val = GetToken(str,token,type)

token = sprintf('"%s"', token);

k = strfind(str, token);

if (isempty(k))
    val = [];
    return;
end

str = str(k:end);

k = strfind(str, 'value=');

if (isempty(k))
    val = [];
    return;
end

switch (type)
    case 'int'
        val = sscanf(str(k:end),'value="%d"');
    case 'double'
        val = sscanf(str(k:end),'value="%f"');
    case 'string'
        str = str(k+length('value=')+1:end);
        
        k = strfind(str, '"');
        
		if (isempty(k))
            val = [];
            return;
		end
        
        val = str(1:k-1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Retrieves values for tokens of the type <token>value</token>
function val = GetBracketToken(str,token,type)

token = strcat(token, '>');

k = strfind(str, token);

if (isempty(k))
    val = [];
    return;
end

str = str(k+length(token):end);

switch (type)
    case 'int'
        val = sscanf(str,'%d<');
    case 'double'
        val = sscanf(str,'%f<');
    case 'string'
        k = strfind(str, '<');
        
		if (isempty(k))
            val = [];
            return;
		end
        
        val = str(1:k-1);
end
