 /* =============================================================
 * scanpath.c - used by vesselmo.m
 *
 * Computational function
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <stdlib.h>

void scanpath(double *usregion, int randomize, int limx, int limy, long numpoints, long* thispoint, double* path, int x, int y)
{
  double xl = usregion[0];
  double yu = usregion[1];
  double xr = usregion[2];
  double yd = usregion[3];

  if (randomize && rand()<0.40*RAND_MAX) return;
  
  if (x<xl || x>xr || y<yu || y>yd) return;

  if (x>=1 && x<=limx && y>=1 && y<=limy)
  {
      path[*thispoint] = x;
      path[numpoints+*thispoint] = y;
      
      (*thispoint)++;
  }
}

/*
Usage format is [path,npoints] = scanpath(x,y,limx,limy,limdist,randomize,usregion)
*/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  int dist;
  long numpoints, thispoint;  
  int x, y, limx, limy, limdist;
  int randomize;
  int total_num_of_cells;
  double* path;
  double* usregion;

  /* Check for proper number of arguments. */
  if (nrhs!=7 || nlhs!=2)
  {
    mexErrMsgTxt("Usage format is [path,npoints] = scanpath(x,y,limx,limy,limdist,randomize,usregion)");
    return;
  }
  
  if (mxGetN(prhs[6])!=4)
  {
    mexErrMsgTxt("usregion not supplied, scanpath can not continue...");
    return;
  }

  x = (int)(*mxGetPr(prhs[0]));
  y = (int)(*mxGetPr(prhs[1]));
  limx = (int)(*mxGetPr(prhs[2]));
  limy = (int)(*mxGetPr(prhs[3]));
  limdist = (int)(*mxGetPr(prhs[4]));

  randomize = (int)(*mxGetPr(prhs[5]));

  usregion = mxGetPr(prhs[6]);

  /* Create matrix for the return argument. */
  numpoints = 0L + (long)limdist * (8L + ((long)limdist-1)*4);
  thispoint = 0;
  
  plhs[0] = mxCreateDoubleMatrix(numpoints,2,mxREAL);

  path = mxGetPr(plhs[0]);

  for (dist=1;dist<=limdist;dist++)
  {
    int i;
    
    scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x+dist,y);
    scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x-dist,y);
    
    for (i=1;i<=dist;i++)
    {
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x+dist,y-i);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x+dist,y+i);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x-dist,y-i);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x-dist,y+i);
    }
    
    scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x,y+dist);
    scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x,y-dist);
    
    for (i=1;i<=dist-1;i++)
    {
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x-i,y-dist);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x+i,y-dist);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x-i,y+dist);
        scanpath(usregion,randomize,limx,limy,numpoints,&thispoint,path,x+i,y+dist);
    }
  }
  
  plhs[1] = mxCreateDoubleScalar((double)thispoint);

}
