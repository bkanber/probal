asymp_gsm = [0.0321
0.0315
0.0444
0.0634
0.0238
0.0283
0.0352
0.0314
];

symp_gsm = [0.0881
0.0665
0.0936
0.0810
0.0910
0.0936
0.1068
0.0576
0.1063
0.0841
0.0415
0.0685
]

X1 = cat(1,asymp_gsm,symp_gsm);
X2 = cat(1,repmat('asymp',numel(asymp_gsm),1),repmat('symp ',numel(symp_gsm),1));

figure;boxplot(X1,X2,'whisker',999);

set(gca,'FontName','Delicious')
set(gca,'FontSize',18)

title('symptomatic vs asymptomatic');

grid on;

[p,h] = ranksum(asymp_gsm,symp_gsm)

