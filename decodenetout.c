 /* =============================================================
 * decodenetout.c - used by vesselmo.m
 *
 * Computational function
 * Decodes network output
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>

/*
Usage format is decodenetout(trainpath,pathsize,tblock_size,whole_block,limx,limy,netout)
*/

void decodenetout(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  double *trainpath = mxGetPr(prhs[0]);
  double *pathsize = mxGetPr(prhs[1]);

  long tblock_size = (long)(*mxGetPr(prhs[2]));
  
  double *whole_block = mxGetPr(prhs[3]);

  long limx = (long)(*mxGetPr(prhs[4]));
  long limy = (long)(*mxGetPr(prhs[5]));

  double *netout = mxGetPr(prhs[6]);

  long npoints = (long)pathsize[0];
  long i, X, Y;
  
  for (i = 0; i < npoints; i++)
  {
/*
%    if (netout[i]>0.5)
%            whole_block(trainpath(i,2),trainpath(i,1)) = 1;
%          else
%            whole_block(trainpath(i,2),trainpath(i,1)) = 0;
%          end
% */
    X = trainpath[i];
    Y = trainpath[i+npoints];
    
    whole_block[Y-1+(X-1)*limy] = netout[i];
  }

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs!=7 || nlhs!=0)
  {
    mexErrMsgTxt("Usage format is decodenetout(trainpath,pathsize,tblock_size,whole_block,limx,limy,netout)");
    return;
  }

  decodenetout(nlhs, plhs, nrhs, prhs);

}
