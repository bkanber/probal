alphas=[];
AUCs=[];

for alpha=0:0.01:1.5
    close all;

    vals = 256*dos./(1+gsm).*(sii*1E3+alpha);

    class = sympstat;
    % class = uint8(class);

    % vals = vals*100; % for DOS
    AUC=rocsimu(vals,class);

    disp(['alpha=' num2str(alpha) ', AUC=' num2str(AUC)]);
    alphas=[alphas alpha];
    AUCs=[AUCs AUC];
end

figure;plot(alphas,AUCs);
