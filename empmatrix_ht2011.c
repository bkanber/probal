 /* =============================================================
 * empmatrix_ht2011.c
 *
 * Calculates theoretical probability matrix (HT)
 *
 * This is a MEX-file for MATLAB.
 * Copyright (c) 1984-2000 The MathWorks, Inc.
 * =============================================================
 */
 
/* $Revision: 1.9 $ */

#include "mex.h"
#include <math.h>
#include <stdlib.h>
#include "varfun.h"

void empmatrix(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  long centre_x = (long)(*mxGetPr(prhs[0]));
  long centre_y = (long)(*mxGetPr(prhs[1]));

  double *la = mxGetPr(prhs[2]);
  double *trainpath = mxGetPr(prhs[3]);
  
  long npoints = mxGetM(prhs[3]);

  long ldimx = (long)(*mxGetPr(prhs[4]));
  long ldimy = (long)(*mxGetPr(prhs[5]));
  long block_size = (long)(*mxGetPr(prhs[6]));
  
  double g_threshold = *mxGetPr(prhs[8]);
  double pfilter = *mxGetPr(prhs[9]);
  
  long i;

  long larm_len = ceil(((double)block_size-1)/2);
  long rarm_len = floor(((double)block_size-1)/2);

  double *whole_block = mxGetPr(prhs[7]);

  long bx, by;
  
  double Gth = g_threshold*256;

  long optI = (centre_y-1)+(centre_x-1)*ldimy;
                  
  double G_center = la[optI];

  for (i = 1; i <= npoints; i++)
  {
		long this_x = trainpath[i-1];
		long this_y = trainpath[i-1+npoints];
		long G = (this_y-1)+(this_x-1)*ldimy;
		double thisGS = la[G];

		if (abs(thisGS-G_center)<Gth) whole_block[G] = 1;
		else
			whole_block[G] = 0;
   } /* for npoints */
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  /* Check for proper number of arguments. */
  if (nrhs==0 || 1==1)
  {
    mexPrintf("P matrix computation module(HT) - Build(8) 19 Nov 2011 - (C) 2003-2011\n\n");
    /*return;*/
  }
  
  if (nrhs!=10 || nlhs!=0)
  {
    mexErrMsgTxt("Usage format is empmatrix(x, y, la, trainpath, ldimx, ldimy, block_size, whole_block, g_threshold, pfilter)");
    return;
  }
  
  empmatrix(nlhs, plhs, nrhs, prhs);

}
